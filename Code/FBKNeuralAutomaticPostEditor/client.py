import socket
import sys
import ConfigParser
from itertools import izip
from xml.etree.ElementTree import Element, SubElement, tostring
import xml.etree.ElementTree as ET
import codecs

class APEclient(object):
    
    def __init__(self, confFile):
        """
         Initialize the client
         Args:
            confFile: the configuration file.

        """
        
        self.Config = ConfigParser.ConfigParser()
    
        self.Config.read(confFile)
    
        print dict(self.Config.items('client-config'))
        print dict(self.Config.items('file-config'))

        # Set the client parameters
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        host = self.ConfigSectionMap("client-config")['host'] #"localhost" #raw_input("Server hostname? ")
        port = int(self.ConfigSectionMap("client-config")['port']) #input("Server port? ")
        serverAct  = int(self.ConfigSectionMap("client-config")['action'])
        print host
        print port 
        self.sock.connect((host,port))
    
        print >> sys.stderr, "Action " +str(serverAct)
        
        # check each action
        if serverAct == 0:
            # post-edit
            srcFile = self.ConfigSectionMap("file-config")['srcfile']
            mtFile = self.ConfigSectionMap("file-config")['mtfile']
            outFile = self.ConfigSectionMap("file-config")['outputfile']
    
            self.oFile = codecs.open(outFile, 'w',  encoding='utf-8')
            self.c = 1
            if (srcFile != None) & (mtFile != None) & (srcFile != "") &  (mtFile != "") :
                #Multi-source
                with codecs.open(srcFile, encoding='utf-8') as srcF, codecs.open(mtFile, encoding='utf-8') as mtF: 
                    for srcS, mtS in izip(srcF, mtF):
                        self.srcS = srcS.strip()
                        self.mtS = mtS.strip()
                        self.postedit()
                        self.c +=1
            elif (srcFile != None) & (srcFile != ""):
                #Single source
                print >> sys.stderr, "Single source "
                with codecs.open(srcFile, encoding='utf-8') as srcF: 
                    for srcS in izip(srcF):
                        print >> sys.stderr, "srcS ", srcS[0]
                        sys.stderr.flush()
                        self.srcS = srcS[0].strip()
                        self.mtS = None
                        self.postedit()
                        self.c +=1
            self.oFile.close()
            
        elif serverAct == 1:
            # update the model
            srcFile = self.ConfigSectionMap("file-config")['srcfile']
            mtFile = self.ConfigSectionMap("file-config")['mtfile']
            peFile = self.ConfigSectionMap("file-config")['pefile']
    
     
            self.c = 1
            if (srcFile != None)  & (mtFile != None) & (peFile != None) & (srcFile != "") & (mtFile != "") & (peFile != ""):
                # multi-source
                with codecs.open(srcFile, encoding='utf-8') as srcF, codecs.open(mtFile, encoding='utf-8') as mtF, codecs.open(peFile, encoding='utf-8') as peF: 
                    for srcS, mtS, peS in izip(srcF, mtF, peF):
                        self.srcS = srcS.strip()
                        self.mtS = mtS.strip()
                        self.peS = peS.strip()
                        self.update()
            elif(srcFile != None) &  (peFile != None) &  (srcFile != "") &  (peFile != ""):
                # single-source
                with codecs.open(srcFile, encoding='utf-8') as srcF,  codecs.open(peFile, encoding='utf-8') as peF: 
                    for srcS,  peS in izip(srcF,  peF):
                        self.srcS = srcS.strip()
                        self.mtS = None
                        self.peS = peS.strip()
                        self.update()                        
        elif serverAct == 2:
            # Online: post-edit + update
            
            srcFile = self.ConfigSectionMap("file-config")['srcfile']
            mtFile = self.ConfigSectionMap("file-config")['mtfile']
            peFile = self.ConfigSectionMap("file-config")['pefile']
    
            outFile = self.ConfigSectionMap("file-config")['outputfile']
    
            self.oFile = codecs.open(outFile, 'w',  encoding='utf-8')
            self.c = 1
            if(srcFile != "") & (srcFile != None) & (mtFile != "") & (mtFile != None) & (peFile != "") & (peFile != None):
                # Multi-source
                with codecs.open(srcFile, encoding='utf-8') as srcF, codecs.open(mtFile, encoding='utf-8') as mtF, codecs.open(peFile, encoding='utf-8') as peF: 
                    for srcS, mtS, peS in izip(srcF, mtF, peF):
                        self.srcS = srcS.strip()
                        self.mtS = mtS.strip()
                        self.peS = peS.strip()

                        ret = self.postedit()
                        if(ret == 0):
                            self.update()
                        else:
                            print sys.stderr, "Post-edit step did not work, I cannot update the model!!"
                        self.c +=1
            elif(srcFile != "") & (srcFile != None) &  (peFile != "") & (peFile != None):
                # Single source
                with codecs.open(srcFile, encoding='utf-8') as srcF, codecs.open(peFile, encoding='utf-8') as peF: 
                    for srcS,  peS in izip(srcF,  peF):
                        self.srcS = srcS.strip()
                        self.mtS = None
                        self.peS = peS.strip()

                        ret = self.postedit()
                        if(ret == 0):
                            self.update()
                        else:
                            print sys.stderr, "Post-edit step did not work, I cannot update the model!!"
                        self.c +=1

            self.oFile.close()
        elif serverAct == 3:
            # Dump the model
            print >> sys.stderr, "Dump the model!"
            self.dump()
        else:
            # Unknown action, the client switches off
            print >> sys.stderr, "Action Not Found - Client Switched off!" 
            sys.exit(-1)

        
    
    
    def ConfigSectionMap(self, section):
        """
         Map the sections in the configuration file into a map
         Args:
            section: the section of the configuration file to map.
        """
        
        dict1 = {}
        options = self.Config.options(section)
        for option in options:
            try:
                dict1[option] = self.Config.get(section, option)
                if dict1[option] == -1:
                    DebugPrint("skip: %s" % option)
            except:
                print("exception on %s!" % option)
                dict1[option] = None
        return dict1


    def postedit(self):
        """ 
            Post-editing the source segment (one if single source, two if multi-source)
        """
        # prepare the xml output to send
        print >> sys.stderr, "I'm inside postedit"
        root = Element('root')
        id = SubElement(root, "id")
        id.text = str(self.c)
    
        action = SubElement(root, "action")
        action.text = "postedit"
        source = SubElement(root, "source")
        
        # add the source
        if self.srcS:

            source.text = self.srcS #"The files are stored in the Adobe / Fireworks CS4 folder in your user-specific Application Data folder ( Windows ) or Application Support folder ( Mac OS ) ."
        else:

            source.text = " "
 
       
        # add the mt. The mt is mandatory.       
        if(self.mtS != None):
            
            mt = SubElement(root, "mt")
            if(self.mtS ==""):
                print >> sys.stderr, "mt is empty - Client Switched off!" 
                sys.exit(-1)
            mt.text = self.mtS
        else:
            print >> sys.stderr, "mt is empty - Client Switched off!" 
            sys.exit(-1)
    
        data =  tostring(root)
        
        print >> sys.stderr,data
        sys.stderr.flush()
    
        
        self.sock.send(data.encode("UTF-8")) #.encode('utf-8')
        
        print >> sys.stderr, "Client sent data "
        
        # check the response from the server
        response = self.sock.recv(10000)
        if (response != "") & (response != None):
            print "Response: "+response
            rootR = ET.fromstring(response)
 
            for childR in rootR:

                if childR.tag == 'ape':
                    ape = childR.text
                    print ape
                    
                    # save the ape segment in a file
                    self.oFile.write(ape+"\n")
                    self.oFile.flush()
                elif childR.tag == 'error':
                    pstErr = childR.text
                    print pstErr 

            return 0
        else:
            print "The response from the server is empty!!"  
            sys.exit(0)
            return -1

    def update(self):
        """ 
            Update the model
        """
        
        print >> sys.stderr, "I'm inside update"
        root = Element('root')
        id = SubElement(root, "id")
        id.text = str(self.c)
    
        action = SubElement(root, "action")
        action.text = "update"
        source = SubElement(root, "source")
        
        # add the source
        if self.srcS:
            print >> sys.stderr,"Inside source exist"
            source.text = self.srcS #"The files are stored in the Adobe / Fireworks CS4 folder in your user-specific Application Data folder ( Windows ) or Application Support folder ( Mac OS ) ."
        else:
            print >> sys.stderr,"Inside source empty"
            source.text = " "
 
       
        # add the mt. The mt is mandatory.  
        if(self.mtS != None):
            
            mt = SubElement(root, "mt")
            mt.text = self.mtS #"
        else:
            print >> sys.stderr, "mt is empty - Client Switched off!" 
            sys.exit(-1)
        
        # add the pe. The pe is mandatory. 
        if(self.peS != None): 
            pe = SubElement(root, "pe")
            pe.text = self.peS 
        else:
            print >> sys.stderr, "pe is empty - Client Switched off!" 
            sys.exit(-1)
 
     
        data =  tostring(root)
        
        print >> sys.stderr,data
        sys.stderr.flush()
    
        
        self.sock.send(data.encode("UTF-8")) 

        print >> sys.stderr, "Client sent data "
        # check the response from the server 
        response = self.sock.recv(10000)
        if (response != "") & (response != None):
            print "Response: "+response
            rootR = ET.fromstring(response)

            for childR in rootR:

                if childR.tag == 'update':
                    upd = childR.text
                    print upd
                elif childR.tag == 'error':
                    updErr = childR.text
                    print updErr 
                   
                    
            
        else:
            print "The response from the server is empty!!"  
            sys.exit(0)

 
    def dump(self):
        """ 
            Dump the model to file.
        """

        
        print >> sys.stderr, "I'm inside dump"
        root = Element('root')
       
        action = SubElement(root, "action")
        action.text = "modelDump"

        data =  tostring(root)
        
        print >> sys.stderr,data
        sys.stderr.flush()
    
       
        self.sock.send(data.encode("UTF-8")) 
       
        print >> sys.stderr, "Client sent data "
        
        response = self.sock.recv(10000)
        if (response != "") & (response != None):
            print "Response: "+response
            rootR = ET.fromstring(response)

            for childR in rootR:

                if childR.tag == 'dump':
                    dmp = childR.text
                    print dmp
                elif childR.tag == 'error':
                    dmpErr = childR.text
                    print dmpErr   
                    
            
        else:
            print "The response from the server is empty!!"  
            sys.exit(0)

        

if __name__ == "__main__":
    
    argv=None
    if argv is None:
        argv = sys.argv
        if len(argv) != 2:
                print "Wrong Number of Parameters "
                print "Usage: "
                print "python ....py configurationFile"

                sys.exit()

    
    confFile  = argv[1] 
    
    APEclient(confFile)
    
 