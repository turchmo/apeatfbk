import sys
import sys, os, lucene, threading, time
from datetime import datetime

from java.nio.file import Paths
from java.io import StringReader

# from org.apache.lucene.analysis.miscellaneous import LimitTokenCountAnalyzer
# from org.apache.lucene.analysis.standard import StandardAnalyzer
from org.apache.lucene.analysis.shingle import ShingleAnalyzerWrapper
from org.apache.lucene.analysis.core import WhitespaceAnalyzer
from org.apache.lucene.document import Document, Field, FieldType, StringField, TextField
from org.apache.lucene.index import \
    FieldInfo, IndexWriter, IndexWriterConfig, IndexOptions
from org.apache.lucene.store import SimpleFSDirectory
from org.apache.lucene.queryparser.classic import QueryParser
from org.apache.lucene.store import SimpleFSDirectory
from org.apache.lucene.search import IndexSearcher
from org.apache.lucene.index import DirectoryReader
from org.apache.lucene.index import Term
from org.apache.lucene.search import BooleanClause, BooleanQuery, TermQuery
from org.apache.lucene.index import IndexReader

from org.apache.lucene.analysis.tokenattributes import CharTermAttribute;
from org.apache.lucene.analysis import TokenStream;


class Searcher(object):
    """Usage: python IndexFiles <doc_directory>"""

    def __init__(self, base_dir):
        #lucene.initVM(vmargs=['-Djava.awt.headless=true'])
        self.directory = SimpleFSDirectory(Paths.get(base_dir))
        self.dirReader = DirectoryReader.open(self.directory)
        self.searcher = IndexSearcher(self.dirReader)
        self.analyzer = WhitespaceAnalyzer()
        # self.analyzer = ShingleAnalyzerWrapper(WhitespaceAnalyzer(), 2, 5, " ", True, False, None)

    # self.analyzer = StandardAnalyzer()

    def maxDoc(self):
        reader = self.searcher.getIndexReader();
        num = reader.numDocs();
        return num

    def getBooleanAndQueryMust(self, query):

        rquery = BooleanQuery.Builder()
        ts = self.analyzer.tokenStream("source0", StringReader(query[0]))

        termAtt = ts.addAttribute(CharTermAttribute.class_)
        ts.reset()

        while (ts.incrementToken()):
            termText = termAtt.toString()
            rquery.add(TermQuery(Term("source0", termText)), BooleanClause.Occur.MUST)
        ts.close()

        ts = self.analyzer.tokenStream("source1", StringReader(query[1]))

        termAtt = ts.addAttribute(CharTermAttribute.class_)
        ts.reset()

        while (ts.incrementToken()):
            termText = termAtt.toString()
            rquery.add(TermQuery(Term("source1", termText)), BooleanClause.Occur.MUST)
        ts.close()

        return rquery

    def getBooleanAndQuery(self, query):
        rquery = BooleanQuery.Builder()

        for i in xrange(len(query)):
            field = 'source' + str(i)
            q = BooleanQuery.Builder()
            ts = self.analyzer.tokenStream(field, StringReader(query[i]))
            termAtt = ts.addAttribute(CharTermAttribute.class_)
            ts.reset()

            while (ts.incrementToken()):
                termText = termAtt.toString()
                q.add(TermQuery(Term(field, termText)), BooleanClause.Occur.SHOULD)
            ts.close()
            rquery.add(q.build(), BooleanClause.Occur.MUST)
        return rquery

    def run_screenSourceRef(self):
        while True:
            print
            print "Hit enter with no input to quit."
            command = raw_input("Query:")
            if command == '':
                return

            print
            print "Searching for:", command

            #query = self.getBooleanAndQueryMust(command.split("##"))
            query = self.getBooleanAndQuery(command.split("##"))

            scoreDocs = self.searcher.search(query.build(), 1).scoreDocs

            print "%s total matching documents." % len(scoreDocs)

            for scoreDoc in scoreDocs:
                doc = self.searcher.doc(scoreDoc.doc)
                print 'id:', doc.get("id"), 'score:', scoreDoc.score, 'source:', doc.get(
                    "source0"), 'source1:', doc.get("source1")

    def getBooleanQuery(self, query):
        rquery = BooleanQuery.Builder()


        field = 'source0'
        ts = self.analyzer.tokenStream(field, StringReader(query))
        termAtt = ts.addAttribute(CharTermAttribute.class_)
        ts.reset()

        while (ts.incrementToken()):
            termText = termAtt.toString()
            rquery.add(TermQuery(Term(field, termText)), BooleanClause.Occur.SHOULD)
        ts.close()
        return rquery


    def run_screenSource(self):
        while True:
            print
            print "Hit enter with no input to quit."
            command = raw_input("Query:")
            if command == '':
                return

            print
            print "Searching for:", command

            query = self.getBooleanQuery(command)
            #query = QueryParser("source0", self.analyzer).parse(command)

            scoreDocs = self.searcher.search(query.build(), 1).scoreDocs
            #scoreDocs = self.searcher.search(query, 1).scoreDocs

            print "%s total matching documents." % len(scoreDocs)

            for scoreDoc in scoreDocs:
                doc = self.searcher.doc(scoreDoc.doc)
                print 'id:', doc.get("id"), '\nscore:', scoreDoc.score, '\nsource:', doc.get(
                    "source0"), '\nsource1:', doc.get("source1")


class Ticker(object):

    def __init__(self):
        self.tick = True

    def run(self):
        while self.tick:
            sys.stdout.write('.')
            sys.stdout.flush()
            time.sleep(1.0)

class Indexer(object):
    """Usage: python IndexFiles <doc_directory>"""

    def __init__(self, storeDir):
        lucene.initVM(vmargs=['-Djava.awt.headless=true'])
        print 'lucene', lucene.VERSION
        if not os.path.exists(storeDir):
            os.mkdir(storeDir)

        directory = SimpleFSDirectory(Paths.get(storeDir))
        analyzer = WhitespaceAnalyzer()
        config = IndexWriterConfig(analyzer)
        config.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND)
        self.writer = IndexWriter(directory, config)

    def commit(self):
        ticker = Ticker()
        print 'commit index',
        threading.Thread(target=ticker.run).start()
        self.writer.commit()
        ticker.tick = False
        print 'done'

    def close(self):
        self.writer.close()

    def add(self, count, source, reference):
        try:
            doc = Document()
            doc.add(TextField("id", str(count), Field.Store.YES))
            doc.add(TextField("source0", source, Field.Store.YES))
            doc.add(TextField("source1", reference, Field.Store.YES))
            self.writer.addDocument(doc)

        except Exception, e:
            print "Failed in adding source reference sentences:", e


def index(mt, src, index_dir):
    indexer = Indexer(index_dir)

    from itertools import izip
    c = 0
    with open(mt) as textfile1, open(src) as textfile2:
        for x, y in izip(textfile1, textfile2):
            x = x.strip()
            y = y.strip()
            indexer.add(c, x, y)

            c = c + 1
    indexer.commit()

    indexer.close()


if __name__ == '__main__':
    work_dir = sys.argv[1]
    mt = sys.argv[2]
    src = sys.argv[3]

    index(mt, src, work_dir + '/Index.index123')

    search = Searcher(work_dir + '/Index.index123')

    #search.run_screenSourceRef()
    search.run_screenSource()

