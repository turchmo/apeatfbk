#!/usr/bin/env python
import sys
from util import *
'''
Load the dictionaries and set the method to convert string into numbers
'''
from util import load_dict, load_config

class dictionaryManager(object):

    def __init__(self, num_words, dictionary, model_options, debug):
        
        self.debug = debug
        
        self.model_options = model_options
        
        self.num_words = num_words
        
        print >> sys.stderr, "Dict File: " + dictionary 
        
        self.word_dict = load_dict(dictionary)

        if num_words:
            for key, idx in self.word_dict.items():
                if idx >= num_words:
                    del self.word_dict[key]
        else:
            self.num_words = len(self.word_dict)
        
        print >> sys.stderr, "Set Num_words: "+ str(self.num_words)
        
        self.word_idict = dict()
        for kk, vv in self.word_dict.iteritems():
            self.word_idict[vv] = kk
        self.word_idict[0] = '<eos>'
        self.word_idict[1] = 'UNK'
        
        #print "dictionary size:"
        #print len(self.word_idict)
        #print len(self.word_dict)
        #print self.word_idict
        
      
        
        

    def fromSourcetoIds(self, sentence):

        xs = []
        words = sentence.strip().split()

        x = []
        for wi in words:
            #print "Before: "
            #print w
            #print w.split('|')
            #w = [f if f in self.word_dict else 1 for f in w.split('|')[0]]
            w = wi.encode("UTF-8")
            
            if self.debug:
                if (w in self.word_dict) == False:
                    print >> sys.stderr, "Word not found in dictionary!"
                
                    whatisthis(w)
                
                    print >> sys.stderr, w.decode("UTF-8")
                
                #whatisthis(self.word_idict[57])
               
                #print >> sys.stderr, str(self.word_idict[57]).decode("UTF-8")
            #    print >> sys.stderr, self.word_dict
            #    sys.exit()
                
            
            #w = [self.word_dict[w.encode("UTF-8")] if w.encode("UTF-8") in self.word_dict else 1] #for f in w.split('|')[0]]
            w = [self.word_dict[w] if w in self.word_dict else 1] #for f in w.split('|')[0]]
            
            if self.debug:
                print >> sys.stderr, "After Selecting the id ",w
            
            #print "After: "
            #print w
            x.append(w)

        #x += [[0]*self.model_options['factors']]
        xs.append(x)
        return xs
    

    
    
    def fromTargettoIds(self, sentence):
        
        xs = []
        words = sentence.strip().split()

        #tt = [self.word_idict[w.encode("UTF-8")] if w.encode("UTF-8") in self.word_idict else 1
         #             for w in words]
        
        tt = [self.word_dict[w.encode("UTF-8")] if w.encode("UTF-8") in self.word_dict else 1
                      for w in words]
        xs.append(tt)
        
        return xs
    
    def getNumWords(self):
        return self.num_words
    
    def fromIdstoSource(self, src):
        
        sentenceSource = ""
        for pos in range(src.shape[1]):
                    if src[0, pos, 0] == 0:
                        break
                    vv = src[0, pos, 0]
                    
                    
                    if vv in self.word_idict:
                        sentenceSource += self.word_idict[vv]
                    else:
                        sentenceSource += 'UNK'
                    sentenceSource += ' '

        return sentenceSource.strip().decode("UTF-8")


    def printIDs(self, target):
        ww = []
        #print "here"
        for w in target:
            if w == 0:
                break
            ww.append(str(w))
        return ' '.join(ww)


    def fromIdstoTarget(self, target):
        
            
        ww = []
        #print "here"
        for w in target:
            if (w == 0) :
                break
            if w in self.word_idict:
                ww.append(self.word_idict[w])
            else:
                if self.debug:
                    print >> sys.stderr, 'Your decoder has generated an id for which there is not an associated word: ',w
                    print >> sys.stderr, 'This id will be replaced by XXXXX'
                ww.append("XXXXX")
        return ' '.join(ww).decode("UTF-8")
    