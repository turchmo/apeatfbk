#!/usr/bin/env python

INDEX_DIR = "IndexFiles.index"

import sys, os, lucene, threading, time
from datetime import datetime


from java.nio.file import Paths
from java.io import StringReader


#from org.apache.lucene.analysis.miscellaneous import LimitTokenCountAnalyzer
#from org.apache.lucene.analysis.standard import StandardAnalyzer
from org.apache.lucene.analysis.core import WhitespaceAnalyzer
from org.apache.lucene.document import Document, Field, FieldType, StringField, TextField
from org.apache.lucene.index import \
    FieldInfo, IndexWriter, IndexWriterConfig, IndexOptions
from org.apache.lucene.store import SimpleFSDirectory
from org.apache.lucene.queryparser.classic import QueryParser
from org.apache.lucene.store import SimpleFSDirectory
from org.apache.lucene.search import IndexSearcher
from org.apache.lucene.index import DirectoryReader
from org.apache.lucene.index import Term
from org.apache.lucene.search import BooleanClause, BooleanQuery, TermQuery
from org.apache.lucene.index import IndexReader

from org.apache.lucene.analysis.tokenattributes import CharTermAttribute;
from org.apache.lucene.analysis import TokenStream;

"""
This class is loosely based on the Lucene (java implementation) demo class
org.apache.lucene.demo.IndexFiles.  It will take a directory as an argument
and will index all of the files in that directory and downward recursively.
It will index on the file path, the file name and the file contents.  The
resulting Lucene index will be placed in the current directory and called
'index'.
"""

class Ticker(object):

    def __init__(self):
        self.tick = True

    def run(self):
        while self.tick:
            sys.stdout.write('.')
            sys.stdout.flush()
            time.sleep(1.0)

class Indexer(object):
    """Usage: python IndexFiles <doc_directory>"""

    def __init__(self, storeDir, initVM=True):
        if initVM:
            lucene.initVM(vmargs=['-Djava.awt.headless=true'])
            print >> sys.stderr, 'lucene', lucene.VERSION
        if not os.path.exists(storeDir):
            os.mkdir(storeDir)

        directory = SimpleFSDirectory(Paths.get(storeDir))
        # analyzer = StandardAnalyzer()
        analyzer = WhitespaceAnalyzer()
        # analyzer = LimitTokenCountAnalyzer(analyzer, 10000)
        config = IndexWriterConfig(analyzer)
        #config.setOpenMode(IndexWriterConfig.OpenMode.CREATE)
        config.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND)
        self.writer = IndexWriter(directory, config)
        

    def commit(self):
        ticker = Ticker()
        print >> sys.stderr, 'commit index',
        threading.Thread(target=ticker.run).start()
        self.writer.commit()
        #self.writer.close()
        ticker.tick = False
        print >> sys.stderr, 'done'

    def close(self):
        self.writer.close()

    def add(self, count, source, reference, source_words, reference_words, alignment=None):
        try:
            doc = Document()
            doc.add(TextField("id", str(count), Field.Store.YES))

            for i in xrange(len(source)):
                doc.add(TextField("source"+str(i), source[i], Field.Store.YES))

            for i in xrange(len(source_words)):
                doc.add(TextField("source_words" + str(i), source_words[i], Field.Store.YES))

            doc.add(TextField("reference", reference, Field.Store.YES))
            doc.add(TextField("reference_words", reference_words, Field.Store.YES))
            if alignment:
                doc.add(TextField("alignment", alignment, Field.Store.YES))
            self.writer.addDocument(doc)
        except Exception, e:
            print >> sys.stderr, "Failed in adding source reference sentences:", e


    def add_with_alignment(self, count, source, reference, alignment):
        try:
            doc = Document()
            doc.add(TextField("id", str(count), Field.Store.YES))
            doc.add(TextField("source", source, Field.Store.YES))
            doc.add(TextField("reference", reference, Field.Store.YES))
            doc.add(TextField("alignment", alignment, Field.Store.YES))
            self.writer.addDocument(doc)
        except Exception, e:
            print "Failed in adding source reference sentences:", e



    def add2(self, count, source, reference):
        try:
            doc = Document()
            doc.add(TextField("id", str(count), Field.Store.YES))
            doc.add(TextField("source", source, Field.Store.YES))
            doc.add(TextField("reference", reference, Field.Store.YES))
            self.writer.addDocument(doc)

        except Exception, e:
            print "Failed in adding source reference sentences:", e


class Searcher(object):
    """Usage: python IndexFiles <doc_directory>"""

    def __init__(self, base_dir, initVM=True, verbose=False):
        print "Inside __init__ Searcher"
        print initVM
        if verbose:
            print >> sys.stderr, 'lucene', lucene.VERSION
            print >> sys.stderr, base_dir
            print >> sys.stderr, "INIT_VS: ", initVM

        if initVM:
            lucene.initVM(vmargs=['-Djava.awt.headless=true'])

        directory = SimpleFSDirectory(Paths.get(base_dir))
        self.reader = DirectoryReader.open(directory)
        self.searcher = IndexSearcher(self.reader)
        self.analyzer = WhitespaceAnalyzer()
        #self.analyzer = StandardAnalyzer()



    def delete(self):
        self.reader.close()
        del self.searcher

    def maxDoc(self):
        reader = self.searcher.getIndexReader();
        num = reader.numDocs();
        return num


    def run_screen(self):
        while True:
            print
            print "Hit enter with no input to quit."
            command = raw_input("Query:")
            if command == '':
                return

            print
            print "Searching for:", command
            query = QueryParser("source", self.analyzer).parse(command)
            scoreDocs = self.searcher.search(query, 50).scoreDocs
            print "%s total matching documents." % len(scoreDocs)

            for scoreDoc in scoreDocs:
                doc = self.searcher.doc(scoreDoc.doc)
                print 'id:', doc.get("id"), 'score:', scoreDoc.score, 'source:', doc.get("source"), 'reference:', doc.get("reference")

    def run_screenSourceRef(self):
        while True:
            print
            print "Hit enter with no input to quit."
            command = raw_input("Query:")
            if command == '':
                return

            print
            print "Searching for:", command

            query = BooleanQuery.Builder()
            query.add(TermQuery(Term("source", command.split("##")[0])), BooleanClause.Occur.MUST)
            query.add(TermQuery(Term("reference", command.split("##")[1])), BooleanClause.Occur.MUST)
            bq1 = query.build()


            #query = QueryParser("source", self.analyzer).parse(command)
            scoreDocs = self.searcher.search(bq1, 50).scoreDocs
            print "%s total matching documents." % len(scoreDocs)

            for scoreDoc in scoreDocs:
                doc = self.searcher.doc(scoreDoc.doc)
                print 'id:', doc.get("id"), 'score:', scoreDoc.score, 'source:', doc.get("source"), 'reference:', doc.get("reference")

    def query_source(self, queryString, maxDoc):

        query = QueryParser("source", self.analyzer).parse(queryString)
        scoreDocs = self.searcher.search(query, int(maxDoc)).scoreDocs
        vec = {}
        c = 0
        for scoreDoc in scoreDocs:
            doc = self.searcher.doc(scoreDoc.doc)
            vec[c] = [doc.get("id"), scoreDoc.score, doc.get("source"), doc.get("reference")]
            c += 1
        return vec

    def query_multi_source(self, dquery, maxDoc, num_encoder):
        print "inside PyLucene - before building query!"
        
        print dquery
        query = self.getBooleanAndQuery(dquery)

        print "inside PyLucene - before retrieving doc!"

        scoreDocs = self.searcher.search(query.build(), int(maxDoc)).scoreDocs

        print "inside PyLucene - after search!"

        vec = {}
        c = 0
        for scoreDoc in scoreDocs:
            doc = self.searcher.doc(scoreDoc.doc)
            source = [doc.get("source"+str(i)) for i in xrange(num_encoder)]
            source_words = [doc.get("source_words" + str(i)) for i in xrange(num_encoder)]
            vec[c] = [doc.get("id"), scoreDoc.score, source, doc.get("reference"), source_words, doc.get("reference_words")]
            c += 1
        return vec

    def query_single_source(self, dquery, maxDoc, field, num_encoder):

        #query = self.getBooleanQuery(dquery, field)
        query = QueryParser(field, self.analyzer).parse(dquery[field])

        #scoreDocs = self.searcher.search(query.build(), int(maxDoc)).scoreDocs
        scoreDocs = self.searcher.search(query, int(maxDoc)).scoreDocs

        vec = {}
        c = 0
        for scoreDoc in scoreDocs:
            doc = self.searcher.doc(scoreDoc.doc)
            source = [doc.get("source"+str(i)) for i in xrange(num_encoder)]
            vec[c] = [doc.get("id"), scoreDoc.score, source, doc.get("reference")]
            c += 1
        return vec


    def query_source_with_alignment(self, queryString, maxDoc):
        query = QueryParser("source", self.analyzer).parse(queryString)
        scoreDocs = self.searcher.search(query, int(maxDoc)).scoreDocs

        vec = {}
        c = 0
        for scoreDoc in scoreDocs:
            doc = self.searcher.doc(scoreDoc.doc)
            vec[c] = [doc.get("id"), scoreDoc.score, doc.get("source"), doc.get("reference"), doc.get("alignment")]
            c += 1
        return vec


    def query_source_reference(self, dquery, maxDoc):
        sourceString = dquery["source0"]
        referenceString = dquery["source1"]

        query = BooleanQuery.Builder()
        ts = self.analyzer.tokenStream("source", StringReader(sourceString))

        termAtt = ts.addAttribute(CharTermAttribute.class_)
        ts.reset()

        while (ts.incrementToken()):
            termText = termAtt.toString()
            print "term text: " + termText
            query.add(TermQuery(Term("source", termText)), BooleanClause.Occur.MUST)
        ts.close()

        # add reference
        ts = self.analyzer.tokenStream("reference", StringReader(referenceString))

        termAtt = ts.addAttribute(CharTermAttribute.class_)
        ts.reset()

        while (ts.incrementToken()):
            termText = termAtt.toString()
            print "term text: " + termText
            query.add(TermQuery(Term("reference", termText)), BooleanClause.Occur.MUST)
        ts.close()

        # query.build()

        return query



    def getBooleanAndQuery(self, query):
        
        print query
        
        rquery = BooleanQuery.Builder()
        print query
        for field in query:
            q = BooleanQuery.Builder()
            ts = self.analyzer.tokenStream(field, StringReader(query[field]))
            termAtt = ts.addAttribute(CharTermAttribute.class_)
            ts.reset()

            while (ts.incrementToken()):
                termText = termAtt.toString()
                q.add(TermQuery(Term(field, termText)), BooleanClause.Occur.SHOULD)
            ts.close()
            rquery.add(q.build(), BooleanClause.Occur.MUST)

        return rquery

    def getBooleanQuery(self, query, field):
        rquery = BooleanQuery.Builder()

        ts = self.analyzer.tokenStream(field, StringReader(query[field]))
        termAtt = ts.addAttribute(CharTermAttribute.class_)
        ts.reset()

        while (ts.incrementToken()):
            termText = termAtt.toString()
            rquery.add(TermQuery(Term(field, termText)), BooleanClause.Occur.SHOULD)
        ts.close()

        return rquery

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print IndexFiles.__doc__
        sys.exit(1)
    #lucene.initVM(vmargs=['-Djava.awt.headless=true'])
    print 'lucene', lucene.VERSION
    start = datetime.now()
    try:
        base_dir = os.path.dirname(os.path.abspath(sys.argv[1]))
        print base_dir
        print os.path.join(base_dir, INDEX_DIR)

        indexer = Indexer(os.path.join(base_dir, INDEX_DIR))

        from itertools import izip
        c = 0
        with open(sys.argv[2]) as textfile1, open(sys.argv[3]) as textfile2:
            for x, y in izip(textfile1, textfile2):
                x = x.strip()
                y = y.strip()
                indexer.add(c, x, y)

                c = c +1
                indexer.commit()

        indexer.close()


        #IndexFiles(sys.argv[1], os.path.join(base_dir, INDEX_DIR),
        #           StandardAnalyzer())
        end = datetime.now()
        print end - start


        search = Searcher(os.path.join(base_dir, INDEX_DIR))

        #search.run()
        search.run_screenSourceRef()

    except Exception, e:
        print "Failed: ", e
        raise e


    
