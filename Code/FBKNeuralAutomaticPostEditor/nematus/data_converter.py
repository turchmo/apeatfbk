#!/usr/bin/env python
'''
Load the dictionaries and set the method to convert string into numbers
'''
from util import load_dict, load_config

class dataConverter(object):

    def __init__(self, num_words, dictionary, model_options):
        
        self.model_options = model_options
        
        self.num_words = num_words
        
        print "Dict File: " + dictionary 
        
        self.word_dict = load_dict(dictionary)
        if num_words:
            for key, idx in self.word_dict.items():
                if idx >= num_words:
                    del self.word_dict[key]
        self.word_idict = dict()
        for kk, vv in self.word_dict.iteritems():
            self.word_idict[vv] = kk
        self.word_idict[0] = '<eos>'
        self.word_idict[1] = 'UNK'
        
        print "dictionary size:"
        print len(self.word_idict)
        #print self.word_idict

    def processSource(self, sentence):
        
        xs = []
        words = sentence.strip().split()
        print "start processing"
        x = []
        for w in words:
            #print "Before: "
            #print w
            #print w.split('|')
            #w = [f if f in self.word_dict else 1 for f in w.split('|')[0]]
            w = [self.word_dict[w] if w in self.word_dict else 1] #for f in w.split('|')[0]]
            #print "After: "
            #print w
            x.append(w)
        print "end processing"
        x += [[0]*self.model_options['factors']]
        xs.append(x)
        return xs
    
    
    def processTarget(self, sentence):
        
        xs = []
        words = sentence.strip().split()

        tt = [self.word_idict[w] if w in self.word_idict else 1
                      for w in words]
        xs.append(tt)
        
        return xs
    
    
    
    