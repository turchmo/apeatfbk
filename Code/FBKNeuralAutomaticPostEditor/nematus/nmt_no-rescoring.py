'''
Build a neural machine translation model with soft attention
'''
import theano
import theano.tensor as tensor
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

import cPickle as pkl
import json
import ipdb
import numpy
import copy
import argparse

import os
import warnings
import sys
import time
import pprint
from subprocess import Popen

from collections import OrderedDict
import nltk
from PyLucene import Indexer, Searcher
from itertools import izip
import itertools
from math import floor
from collections import defaultdict

profile = False
numpy.random.seed(1234)

from data_iterator import TextIterator
from util import *
from theano_util import *
from alignment_util import *
import pyter

from layers import *
from initializers import *
from optimizers import *

from domain_interpolation_data_iterator import DomainInterpolatorTextIterator

# batch preparation
def prepare_data(seqs_x, seqs_y, maxlen=None, n_words_src=30000,
                 n_words=30000):
    # x: a list of sentences
    lengths_x = [[len(x__) for x__ in x_] for x_ in seqs_x ]
    lengths_y = [len(s) for s in seqs_y]

    ''' 
    # Length constraint is already checked in data_iterator.py
    if maxlen is not None:
        new_seqs_x = []
        new_seqs_y = []
        new_lengths_x = []
        new_lengths_y = []
        for l_x, s_x, l_y, s_y in zip(lengths_x, seqs_x, lengths_y, seqs_y):
            if l_x < maxlen and l_y < maxlen:
                new_seqs_x.append(s_x)
                new_lengths_x.append(l_x)
                new_seqs_y.append(s_y)
                new_lengths_y.append(l_y)
        lengths_x = new_lengths_x
        seqs_x = new_seqs_x
        lengths_y = new_lengths_y
        seqs_y = new_seqs_y

        if len(lengths_x) < 1 or len(lengths_y) < 1:
            return None, None, None, None
    '''

    n_samples = len(seqs_x[0])
    n_factors = len(seqs_x[0][0][0])
    maxlen_x = [numpy.max(len_x) + 1 for len_x in lengths_x]
    maxlen_y = numpy.max(lengths_y) + 1

    x = [numpy.zeros((n_factors, maxlen_x_, n_samples)).astype('int64') for maxlen_x_ in maxlen_x]
    y = numpy.zeros((maxlen_y, n_samples)).astype('int64')

    x_mask = [numpy.zeros((maxlen_x_, n_samples)).astype('float32') for maxlen_x_ in maxlen_x]
    y_mask = numpy.zeros((maxlen_y, n_samples)).astype('float32')

    for idx, seq_ in enumerate(zip(*seqs_x + [seqs_y])):
        s_x_ = seq_[:-1]
        s_y = seq_[-1]

        for ii, s_x in enumerate(s_x_):
            x[ii][:, :lengths_x[ii][idx], idx] = zip(*s_x)
            x_mask[ii][:lengths_x[ii][idx]+1, idx] = 1.

        y[:lengths_y[idx], idx] = s_y
        y_mask[:lengths_y[idx]+1, idx] = 1.

    return x, x_mask, y, y_mask


# initialize all parameters
def init_params(options):
    num_encoder = len(options['datasets']) - 1

    params = OrderedDict()

    for i in xrange(num_encoder):
        # embedding
        for factor in range(options['factors']):
            params[embedding_name(factor)+'_'+str(i)] = norm_weight(options['n_words_src'], options['dim_per_factor'][factor])

        # encoder: bidirectional RNN
        params = get_layer_param(options['encoder'])(options, params,
                                                  prefix='encoder'+'_'+str(i),
                                                  nin=options['dim_word'],
                                                  dim=options['dim'])
        params = get_layer_param(options['encoder'])(options, params,
                                                  prefix='encoder_r'+'_'+str(i),
                                                  nin=options['dim_word'],
                                                  dim=options['dim'])

    ctxdim = 2 * options['dim']
    params = get_layer_param('ff_init_merger')(params, prefix='ff_init_merger',
                                nin=ctxdim*num_encoder, nout=ctxdim)

    params['Wemb_dec'] = norm_weight(options['n_words'], options['dim_word'])


    # init_state, init_cell
    params = get_layer_param('ff')(options, params, prefix='ff_state',
                                nin=ctxdim, nout=options['dim'])
    # decoder
    params = get_layer_param(options['decoder'])(options, params,
                                              prefix='decoder',
                                              nin=options['dim_word'],
                                              dim=options['dim'],
                                              dimctx=ctxdim, attend_merge_op='mean-concat')
    # readout
    params = get_layer_param('ff')(options, params, prefix='ff_logit_lstm',
                                nin=options['dim'], nout=options['dim_word'],
                                ortho=False)
    params = get_layer_param('ff')(options, params, prefix='ff_logit_prev',
                                nin=options['dim_word'],
                                nout=options['dim_word'], ortho=False)
    params = get_layer_param('ff')(options, params, prefix='ff_logit_ctx',
                                nin=ctxdim, nout=options['dim_word'],
                                ortho=False)
    params = get_layer_param('ff')(options, params, prefix='ff_logit',
                                nin=options['dim_word'],
                                nout=options['n_words'])

    return params


# bidirectional RNN encoder: take input x (optionally with mask), and produce sequence of context vectors (ctx)
def build_encoder(tparams, options, trng, use_noise, x=None, x_mask=None, enc_id=None, sampling=False):
    #x = tensor.tensor3('x', dtype='int64')
    #x.tag.test_value = (numpy.random.rand(1, 5, 10) * 100).astype('int64')

    # for the backward rnn, we just need to invert x
    xr = x[:, ::-1]
    if x_mask is None:
        xr_mask = None
    else:
        xr_mask = x_mask[::-1]

    n_timesteps = x.shape[1]
    n_samples = x.shape[2]

    if options['use_dropout']:
        retain_probability_emb = 1 - options['dropout_embedding']
        retain_probability_hidden = 1 - options['dropout_hidden']
        retain_probability_source = 1 - options['dropout_source']
        if sampling:
            if options['model_version'] < 0.1:
                rec_dropout = theano.shared(numpy.array([retain_probability_hidden] * 2, dtype='float32'))
                rec_dropout_r = theano.shared(numpy.array([retain_probability_hidden] * 2, dtype='float32'))
                emb_dropout = theano.shared(numpy.array([retain_probability_emb] * 2, dtype='float32'))
                emb_dropout_r = theano.shared(numpy.array([retain_probability_emb] * 2, dtype='float32'))
                source_dropout = theano.shared(numpy.float32(retain_probability_source))
            else:
                rec_dropout = theano.shared(numpy.array([1.] * 2, dtype='float32'))
                rec_dropout_r = theano.shared(numpy.array([1.] * 2, dtype='float32'))
                emb_dropout = theano.shared(numpy.array([1.] * 2, dtype='float32'))
                emb_dropout_r = theano.shared(numpy.array([1.] * 2, dtype='float32'))
                source_dropout = theano.shared(numpy.float32(1.))
        else:
            if options['model_version'] < 0.1:
                scaled = False
            else:
                scaled = True
            rec_dropout = shared_dropout_layer((2, n_samples, options['dim']), use_noise, trng,
                                               retain_probability_hidden, scaled)
            rec_dropout_r = shared_dropout_layer((2, n_samples, options['dim']), use_noise, trng,
                                                 retain_probability_hidden, scaled)
            emb_dropout = shared_dropout_layer((2, n_samples, options['dim_word']), use_noise, trng,
                                               retain_probability_emb, scaled)
            emb_dropout_r = shared_dropout_layer((2, n_samples, options['dim_word']), use_noise, trng,
                                                 retain_probability_emb, scaled)
            source_dropout = shared_dropout_layer((n_timesteps, n_samples, 1), use_noise, trng,
                                                  retain_probability_source, scaled)
            source_dropout = tensor.tile(source_dropout, (1, 1, options['dim_word']))
    else:
        rec_dropout = theano.shared(numpy.array([1.] * 2, dtype='float32'))
        rec_dropout_r = theano.shared(numpy.array([1.] * 2, dtype='float32'))
        emb_dropout = theano.shared(numpy.array([1.] * 2, dtype='float32'))
        emb_dropout_r = theano.shared(numpy.array([1.] * 2, dtype='float32'))

    # word embedding for forward rnn (source)
    emb = []
    for factor in range(options['factors']):
        emb.append(tparams[embedding_name(factor)+'_'+str(enc_id)][x[factor].flatten()])

    emb = concatenate(emb, axis=1)
    emb = emb.reshape([n_timesteps, n_samples, options['dim_word']])

    if options['use_dropout']:
        emb *= source_dropout

    proj = get_layer_constr(options['encoder'])(tparams, emb, options,
                                                prefix='encoder'+'_'+str(enc_id),
                                                mask=x_mask,
                                                emb_dropout=emb_dropout,
                                                rec_dropout=rec_dropout,
                                                profile=profile)

    # word embedding for backward rnn (source)
    embr = []
    for factor in range(options['factors']):
        embr.append(tparams[embedding_name(factor)+'_'+str(enc_id)][xr[factor].flatten()])
    embr = concatenate(embr, axis=1)
    embr = embr.reshape([n_timesteps, n_samples, options['dim_word']])
    if options['use_dropout']:
        if sampling:
            embr *= source_dropout
        else:
            # we drop out the same words in both directions
            embr *= source_dropout[::-1]

    projr = get_layer_constr(options['encoder'])(tparams, embr, options,
                                                 prefix='encoder_r'+'_'+str(enc_id),
                                                 mask=xr_mask,
                                                 emb_dropout=emb_dropout_r,
                                                 rec_dropout=rec_dropout_r,
                                                 profile=profile)

    # context will be the concatenation of forward and backward rnns
    ctx = concatenate([proj[0], projr[0][::-1]], axis=proj[0].ndim - 1)

    return ctx


# build a training model
def build_model(tparams, options):
    opt_ret = dict()
    num_encoder = len(options['datasets']) - 1

    trng = RandomStreams(1234)
    use_noise = theano.shared(numpy.float32(0.))

    xs = [tensor.ltensor3('source%d' % i) for i in range(num_encoder)]
    for i in xrange(num_encoder):
        x_test = (numpy.random.rand(1, 5, 10) * 100).astype('int64')
        xs[i].tag.test_value = x_test

    x_masks = [tensor.matrix('source%d_mask' % i) for i in range(num_encoder)]

    y = tensor.matrix('y', dtype='int64')
    y.tag.test_value = (numpy.random.rand(8, 10) * 100).astype('int64')
    y_mask = tensor.matrix('y_mask', dtype='float32')
    y_mask.tag.test_value = numpy.ones(shape=(8, 10)).astype('float32')
    n_samples = y.shape[1]
    n_timesteps_trg = y.shape[0]

    if options['use_dropout']:
        retain_probability_emb = 1-options['dropout_embedding']
        retain_probability_hidden = 1-options['dropout_hidden']
        retain_probability_target = 1-options['dropout_target']
        if options['model_version'] < 0.1:
            scaled = False
        else:
            scaled = True
        rec_dropout_d = shared_dropout_layer((5, n_samples, options['dim']), use_noise, trng, retain_probability_hidden, scaled)
        emb_dropout_d = shared_dropout_layer((2, n_samples, options['dim_word']), use_noise, trng, retain_probability_emb, scaled)
        ctx_dropout_d = shared_dropout_layer((4, n_samples, 2*options['dim']), use_noise, trng, retain_probability_hidden, scaled)
        target_dropout = shared_dropout_layer((n_timesteps_trg, n_samples, 1), use_noise, trng, retain_probability_target, scaled)
        target_dropout = tensor.tile(target_dropout, (1,1,options['dim_word']))
    else:
        rec_dropout_d = theano.shared(numpy.array([1.]*5, dtype='float32'))
        emb_dropout_d = theano.shared(numpy.array([1.]*2, dtype='float32'))
        ctx_dropout_d = theano.shared(numpy.array([1.]*4, dtype='float32'))

    for i in range(num_encoder):
        xs[i].tag.test_value = (numpy.random.rand(1, 5, 10)*100).astype('int64')
        x_masks[i].tag.test_value = numpy.ones(shape=(5, 10)).astype('float32')

    ctxs = OrderedDict()
    for enc_id in xrange(num_encoder):
        ctxs[enc_id] = build_encoder(tparams, options, trng, use_noise, xs[enc_id], x_masks[enc_id], enc_id, sampling=False)

    states_below = OrderedDict()
    for enc_id in xrange(num_encoder):
        states_below[enc_id] = (ctxs[enc_id] * x_masks[enc_id][:, :, None]).sum(0) / x_masks[enc_id].sum(0)[:, None]

    ctx_mean = get_layer_constr('ff_init_merger')(
        tparams, states_below, prefix='ff_init_merger',
        init_merge_op='mean-concat',
        init_merge_act='linear')

    # mean of the context (across time) will be used to initialize decoder rnn
    #ctx_mean = (ctx * x_mask[:, :, None]).sum(0) / x_mask.sum(0)[:, None]

    # or you can use the last state of forward + backward encoder rnns
    # ctx_mean = concatenate([proj[0][-1], projr[0][-1]], axis=proj[0].ndim-2)

    if options['use_dropout']:
        ctx_mean *= shared_dropout_layer((n_samples, 2*options['dim']), use_noise, trng, retain_probability_hidden, scaled)

    # initial decoder state
    init_state = get_layer_constr('ff')(tparams, ctx_mean, options,
                                    prefix='ff_state', activ='tanh')



    # word embedding (target), we will shift the target sequence one time step
    # to the right. This is done because of the bi-gram connections in the
    # readout and decoder rnn. The first target will be all zeros and we will
    # not condition on the last output.
    emb = tparams['Wemb_dec'][y.flatten()]
    emb = emb.reshape([n_timesteps_trg, n_samples, options['dim_word']])

    emb_shifted = tensor.zeros_like(emb)
    emb_shifted = tensor.set_subtensor(emb_shifted[1:], emb[:-1])
    emb = emb_shifted

    if options['use_dropout']:
        emb *= target_dropout

    # decoder - pass through the decoder conditional gru with attention
    proj = get_layer_constr(options['decoder'])(tparams, emb, options,
                                            prefix='decoder',
                                            mask=y_mask, context=ctxs,
                                            context_mask=x_masks,
                                            one_step=False,
                                            init_state=init_state,
                                            emb_dropout=emb_dropout_d,
                                            ctx_dropout=ctx_dropout_d,
                                            rec_dropout=rec_dropout_d,
                                            profile=profile, attend_merge_op='mean-concat')
    # hidden states of the decoder gru
    proj_h = proj[0]

    # weighted averages of context, generated by attention module
    ctxs = proj[1]

    if options['use_dropout']:
        proj_h *= shared_dropout_layer((n_samples, options['dim']), use_noise, trng, retain_probability_hidden, scaled)
        emb *= shared_dropout_layer((n_samples, options['dim_word']), use_noise, trng, retain_probability_emb, scaled)
        ctxs *= shared_dropout_layer((n_samples, 2*options['dim']), use_noise, trng, retain_probability_hidden, scaled)

    # weights (alignment matrix) #####LIUCAN: this is where the attention vector is.
    opt_ret['dec_alphas'] = proj[2:2+num_encoder]

    # compute word probabilities
    logit_lstm = get_layer_constr('ff')(tparams, proj_h, options,
                                    prefix='ff_logit_lstm', activ='linear')
    logit_prev = get_layer_constr('ff')(tparams, emb, options,
                                    prefix='ff_logit_prev', activ='linear')
    logit_ctx = get_layer_constr('ff')(tparams, ctxs, options,
                                   prefix='ff_logit_ctx', activ='linear')
    logit = tensor.tanh(logit_lstm+logit_prev+logit_ctx)

    if options['use_dropout']:
        logit *= shared_dropout_layer((n_samples, options['dim_word']), use_noise, trng, retain_probability_hidden, scaled)

    logit = get_layer_constr('ff')(tparams, logit, options,
                                   prefix='ff_logit', activ='linear')
    logit_shp = logit.shape
    probs = tensor.nnet.softmax(logit.reshape([logit_shp[0]*logit_shp[1],
                                               logit_shp[2]]))

    # cost
    y_flat = y.flatten()
    y_flat_idx = tensor.arange(y_flat.shape[0]) * options['n_words'] + y_flat
    cost = -tensor.log(probs.flatten()[y_flat_idx])
    cost = cost.reshape([y.shape[0], y.shape[1]])
    cost = (cost * y_mask).sum(0)

    #print "Print out in build_model()"
    #print opt_ret
    return trng, use_noise, xs, x_masks, y, y_mask, opt_ret, cost


# build a sampler
def build_sampler(tparams, options, use_noise, trng, return_alignment=False):
    opt_ret = dict()
    num_encoder = len(options['datasets']) - 1

    trng = RandomStreams(1234)
    use_noise = theano.shared(numpy.float32(0.))

    xs = [tensor.ltensor3('source%d' % i) for i in range(num_encoder)]
    #x_masks = [tensor.matrix('source%d_mask' % i) for i in range(num_encoder)]

    ctx = OrderedDict()
    for enc_id in xrange(num_encoder):
        ctx[enc_id] = build_encoder(tparams, options, trng, use_noise, xs[enc_id], None, enc_id, sampling=True)

    states_below = OrderedDict()
    for enc_id in xrange(num_encoder):
        states_below[enc_id] = ctx[enc_id].mean(0) #(ctxs[enc_id] * x_masks[enc_id][:, :, None]).sum(0) / x_masks[enc_id].sum(0)[:, None]

    ctx_mean = get_layer_constr('ff_init_merger')(
        tparams, states_below, prefix='ff_init_merger',
        init_merge_op='mean-concat',
        init_merge_act='linear')

    if options['use_dropout'] and options['model_version'] < 0.1:
        retain_probability_emb = 1-options['dropout_embedding']
        retain_probability_hidden = 1-options['dropout_hidden']
        retain_probability_source = 1-options['dropout_source']
        retain_probability_target = 1-options['dropout_target']
        rec_dropout_d = theano.shared(numpy.array([retain_probability_hidden]*5, dtype='float32'))
        emb_dropout_d = theano.shared(numpy.array([retain_probability_emb]*2, dtype='float32'))
        ctx_dropout_d = theano.shared(numpy.array([retain_probability_hidden]*4, dtype='float32'))
        target_dropout = theano.shared(numpy.float32(retain_probability_target))
    else:
        rec_dropout_d = theano.shared(numpy.array([1.]*5, dtype='float32'))
        emb_dropout_d = theano.shared(numpy.array([1.]*2, dtype='float32'))
        ctx_dropout_d = theano.shared(numpy.array([1.]*4, dtype='float32'))

    #x, ctx = build_encoder(tparams, options, trng, use_noise, x_mask=None, sampling=True)

    n_samples = xs[0].shape[2]

    # get the input for decoder rnn initializer mlp
    # ctx_mean = ctx.mean(0)
    # ctx_mean = concatenate([proj[0][-1],projr[0][-1]], axis=proj[0].ndim-2)

    if options['use_dropout'] and options['model_version'] < 0.1:
        ctx_mean *= retain_probability_hidden

    init_state = get_layer_constr('ff')(tparams, ctx_mean, options,
                                    prefix='ff_state', activ='tanh')


    outs = [init_state] + ctx.values() + xs

    print >> sys.stderr, "Building f_init...",
    f_init = theano.function(xs, outs, name='f_init', profile=profile)
    print >>sys.stderr, "Done"

    # x: 1 x 1
    y = tensor.vector('y_sampler', dtype='int64')
    init_state = tensor.matrix('init_state', dtype='float32')

    # if it's the first word, emb should be all zero and it is indicated by -1
    emb = tensor.switch(y[:, None] < 0,
                        tensor.alloc(0., 1, tparams['Wemb_dec'].shape[1]),
                        tparams['Wemb_dec'][y])

    if options['use_dropout'] and options['model_version'] < 0.1:
        emb *= target_dropout

    # apply one step of conditional gru with attention
    proj = get_layer_constr(options['decoder'])(tparams, emb, options,
                                            prefix='decoder',
                                            mask=None, context=ctx,
                                            one_step=True,
                                            init_state=init_state,
                                            emb_dropout=emb_dropout_d,
                                            ctx_dropout=ctx_dropout_d,
                                            rec_dropout=rec_dropout_d,
                                            profile=profile, attend_merge_op='mean-concat')

    # get the next hidden state
    next_state = proj[0]

    # get the weighted averages of context for this target word y
    ctxs = proj[1]

    # alignment matrix (attention model)
    dec_alphas = proj[2:2+num_encoder]

    if options['use_dropout'] and options['model_version'] < 0.1:
        next_state_up = next_state * retain_probability_hidden
        emb *= retain_probability_emb
        ctxs *= retain_probability_hidden
    else:
        next_state_up = next_state

    logit_lstm = get_layer_constr('ff')(tparams, next_state_up, options,
                                    prefix='ff_logit_lstm', activ='linear')
    logit_prev = get_layer_constr('ff')(tparams, emb, options,
                                    prefix='ff_logit_prev', activ='linear')
    logit_ctx = get_layer_constr('ff')(tparams, ctxs, options,
                                   prefix='ff_logit_ctx', activ='linear')
    logit = tensor.tanh(logit_lstm+logit_prev+logit_ctx)

    if options['use_dropout'] and options['model_version'] < 0.1:
        logit *= retain_probability_hidden

    logit = get_layer_constr('ff')(tparams, logit, options,
                              prefix='ff_logit', activ='linear')

    # compute the softmax probability
    next_probs = tensor.nnet.softmax(logit)

    # sample from softmax distribution to get the sample
    next_sample = trng.multinomial(pvals=next_probs).argmax(1)

    # compile a function to do the whole thing above, next word probability,
    # sampled word for the next target, next hidden state to be used
    print >>sys.stderr, "Building f_next..",
    inps = [y] + ctx.values() + [init_state]
    outs = [next_probs, next_sample, next_state]

    if return_alignment:
        outs += dec_alphas
        #outs.append(dec_alphas)

    f_next = theano.function(inps, outs, name='f_next', profile=profile)
    print >>sys.stderr, "Done"

    return f_init, f_next

# generate sample, either with stochastic sampling or beam search. Note that,
# this function iteratively calls f_init and f_next functions.
def gen_sample(f_init, f_next, xs, trng=None, k=1, maxlen=30,
               stochastic=True, argmax=False, return_alignment=False, suppress_unk=False, idx=None):

    # k is the beam size we have
    if k > 1:
        assert not stochastic, \
            'Beam search does not support stochastic sampling'

    sample = []
    sample_score = []
    sample_word_probs = []
    alignment = []
    if stochastic:
        sample_score = 0

    live_k = 1
    dead_k = 0

    hyp_samples = [[]] * live_k
    word_probs = [[]] * live_k
    hyp_scores = numpy.zeros(live_k).astype('float32')
    hyp_states = []
    if return_alignment:
        hyp_alignment = [[] for _ in xrange(live_k)]

    # for ensemble decoding, we keep track of states and probability distribution
    # for each model in the ensemble
    num_models = len(f_init)
    next_state = [None]*num_models
    ctx0 = [None]*num_models
    next_p = [None]*num_models
    dec_alphas = [None]*num_models
    # get initial state of decoder rnn and encoder context
    for i in xrange(num_models):
        ret = f_init[i](xs[0], xs[1])
        next_state[i] = ret[0]
        ctx0[i] = [ret[1 + ii] for ii in xrange(2)]

    next_w = -1 * numpy.ones((1,)).astype('int64')  # bos indicator

    # x is a sequence of word ids followed by 0, eos id
    for ii in xrange(maxlen):
        for i in xrange(num_models):
            ctx = [numpy.tile(ctx0_, [live_k, 1]) for ctx0_ in ctx0[i]]
            inps = [next_w] + ctx + [next_state[i]]
            ret = f_next[i](*inps)
            # dimension of dec_alpha (k-beam-size, number-of-input-hidden-units)
            next_p[i], next_w_tmp, next_state[i] = ret[0], ret[1], ret[2]
            if return_alignment:
                dec_alphas[i] = ret[3:]

            if suppress_unk:
                next_p[i][:,1] = -numpy.inf
        if stochastic:
            if argmax:
                nw = sum(next_p)[0].argmax()
            else:
                nw = next_w_tmp[0]
            sample.append(nw)
            sample_score += numpy.log(next_p[0][0, nw])
            if nw == 0:
                break
        else:
            cand_scores = hyp_scores[:, None] - sum(numpy.log(next_p))
            probs = sum(next_p)/num_models
            cand_flat = cand_scores.flatten()
            probs_flat = probs.flatten()
            ranks_flat = cand_flat.argpartition(k-dead_k-1)[:(k-dead_k)]

            #averaging the attention weights accross models
            if return_alignment:
                num_enc = 2
                mean_alignment = [numpy.asarray([dec_alphas[ii_][enc_id] for ii_ in xrange(len(dec_alphas))]).mean(axis=0) for enc_id in xrange(num_enc)]
                #print 'mean align', mean_alignment

            voc_size = next_p[0].shape[1]
            # index of each k-best hypothesis
            trans_indices = ranks_flat / voc_size
            word_indices = ranks_flat % voc_size
            costs = cand_flat[ranks_flat]

            new_hyp_samples = []
            new_hyp_scores = numpy.zeros(k-dead_k).astype('float32')
            new_word_probs = []
            new_hyp_states = []
            if return_alignment:
                # holds the history of attention weights for each time step for each of the surviving hypothesis
                # dimensions (live_k * target_words * source_hidden_units]
                # at each time step we append the attention weights corresponding to the current target word
                new_hyp_alignment = [[] for _ in xrange(k-dead_k)]

            # ti -> index of k-best hypothesis
            for idx, [ti, wi] in enumerate(zip(trans_indices, word_indices)):
                new_hyp_samples.append(hyp_samples[ti]+[wi])
                new_word_probs.append(word_probs[ti] + [probs_flat[ranks_flat[idx]].tolist()])
                new_hyp_scores[idx] = copy.copy(costs[idx])
                new_hyp_states.append([copy.copy(next_state[i][ti]) for i in xrange(num_models)])
                if return_alignment:
                    # get history of attention weights for the current hypothesis
                    new_hyp_alignment[idx] = copy.copy(hyp_alignment[ti])
                    # extend the history with current attention weights
                    new_hyp_alignment[idx].append(mean_alignment[:][ti])

            # check the finished samples
            new_live_k = 0
            hyp_samples = []
            hyp_scores = []
            hyp_states = []
            word_probs = []
            if return_alignment:
                hyp_alignment = []

            # sample and sample_score hold the k-best translations and their scores
            for idx in xrange(len(new_hyp_samples)):
                if new_hyp_samples[idx][-1] == 0:
                    sample.append(new_hyp_samples[idx])
                    sample_score.append(new_hyp_scores[idx])
                    sample_word_probs.append(new_word_probs[idx])
                    if return_alignment:
                        alignment.append(new_hyp_alignment[idx])
                    dead_k += 1
                else:
                    new_live_k += 1
                    hyp_samples.append(new_hyp_samples[idx])
                    hyp_scores.append(new_hyp_scores[idx])
                    hyp_states.append(new_hyp_states[idx])
                    word_probs.append(new_word_probs[idx])
                    if return_alignment:
                        hyp_alignment.append(new_hyp_alignment[idx])
            hyp_scores = numpy.array(hyp_scores)

            live_k = new_live_k

            if new_live_k < 1:
                break
            if dead_k >= k:
                break

            next_w = numpy.array([w[-1] for w in hyp_samples])
            next_state = [numpy.array(state) for state in zip(*hyp_states)]

    if not stochastic:
        # dump every remaining one
        if live_k > 0:
            for idx in xrange(live_k):
                sample.append(hyp_samples[idx])
                sample_score.append(hyp_scores[idx])
                sample_word_probs.append(word_probs[idx])
                if return_alignment:
                    alignment.append(hyp_alignment[idx])

    if not return_alignment:
        alignment = [None for i in range(len(sample))]

    return sample, sample_score, sample_word_probs, alignment


# calculate the log probablities on a given corpus using translation model
def pred_probs(f_log_probs, prepare_data, options, iterator, verbose=True, normalize=False, alignweights=False):
    probs = []
    n_done = 0

    alignments_json = []

    for x, y in iterator:
        #ensure consistency in number of factors
        #if len(x[0][0]) != options['factors']:
        #    sys.stderr.write('Error: mismatch between number of factors in settings ({0}), and number in validation corpus ({1})\n'.format(options['factors'], len(x[0][0])))
        #    sys.exit(1)

        n_done += len(x[0])

        x, x_mask, y, y_mask = prepare_data(x, y,
                                            n_words_src=options['n_words_src'],
                                            n_words=options['n_words'])

        ### in optional save weights mode.
        if alignweights:
            pprobs, attention = f_log_probs(x[0], x[1], x_mask[0], x_mask[1], y, y_mask)
            for jdata in get_alignments(attention, x_mask, y_mask):
                alignments_json.append(jdata)
        else:
            pprobs = f_log_probs(x[0], x[1], x_mask[0], x_mask[1], y, y_mask)

        # normalize scores according to output length
        if normalize:
            lengths = numpy.array([numpy.count_nonzero(s) for s in y_mask.T])
            pprobs /= lengths

        for pp in pprobs:
            probs.append(pp)

        if numpy.isnan(numpy.mean(probs)):
            ipdb.set_trace()

        if verbose:
            print >>sys.stderr, '%d samples computed' % (n_done)

    return numpy.array(probs), alignments_json


def _seqs2words_trg(cc, word_dict_r):
    ww = []
    for w in cc:
        if w == 0:
            break
        ww.append(word_dict_r[w])
    return ' '.join(ww)


def _seqs2words_src(cc, word_dict_r):
    ww = []
    for w in cc:
        if w == 0:
            break
        ww.append(word_dict_r[w[0]])
    return ' '.join(ww)

def bin_(value, scale):
    value *= scale
    if value >= scale:
        return scale - 1
    return int(floor(value))

def flatten3D(a):
    a_2D=list(itertools.chain.from_iterable(a))
    a_1D=list(itertools.chain.from_iterable(a_2D))
    return a_1D

def flatten2D(a):
    a_1D=list(itertools.chain.from_iterable(a))
    return a_1D

def train(dim_word=100,  # word vector dimensionality
          dim=1000,  # the number of LSTM units
          factors=1, # input factors
          dim_per_factor=None, # list of word vector dimensionalities (one per factor): [250,200,50] for total dimensionality of 500
          encoder='gru',
          decoder='gru_cond',
          patience=10,  # early stopping patience
          max_epochs=5000,
          finish_after=10000000,  # finish after this many updates
          dispFreq=100,
          decay_c=0.,  # L2 regularization penalty
          map_decay_c=0., # L2 regularization penalty towards original weights
          alpha_c=0.,  # alignment regularization
          clip_c=-1.,  # gradient clipping threshold
          lrate=0.01,  # learning rate
          n_words_src=None,  # source vocabulary size
          n_words=None,  # target vocabulary size
          maxlen=100,  # maximum length of the description
          optimizer='rmsprop',
          batch_size=1,
          valid_batch_size=16,
          saveto='model.npz',
          validFreq=1000,
          saveFreq=1000,   # save the parameters after every saveFreq updates
          sampleFreq=100,   # generate some samples after every sampleFreq
          datasets=[
              '/data/lisatmp3/chokyun/europarl/europarl-v7.fr-en.en.tok',
              '/data/lisatmp3/chokyun/europarl/europarl-v7.fr-en.fr.tok'],
          valid_datasets=['../data/dev/newstest2011.en.tok',
                          '../data/dev/newstest2011.fr.tok'],
          test_datasets=['../data/dev/newstest2011.en.tok',
                          '../data/dev/newstest2011.fr.tok'],
          dictionaries=[
              '/data/lisatmp3/chokyun/europarl/europarl-v7.fr-en.en.tok.pkl',
              '/data/lisatmp3/chokyun/europarl/europarl-v7.fr-en.fr.tok.pkl'],
          use_dropout=False,
          dropout_embedding=0.2, # dropout for input embeddings (0: no dropout)
          dropout_hidden=0.5, # dropout for hidden layers (0: no dropout)
          dropout_source=0, # dropout source words (0: no dropout)
          dropout_target=0, # dropout target words (0: no dropout)
          reload_=False,
          overwrite=False,
          external_validation_script=None,
          shuffle_each_epoch=True,
          finetune=False,
          finetune_only_last=False,
          sort_by_length=True,
          use_domain_interpolation=False,
          domain_interpolation_min=0.1,
          domain_interpolation_inc=0.1,
          domain_interpolation_indomain_datasets=['indomain.en', 'indomain.fr'],
          maxibatch_size=20, #How many minibatches to load at one time
          model_version=0.1, #store version used for training for compatibility
          translation_fname='online_training_translation.txt',
          use_instance_selection=True,
          reset_model_always=False,
          max_instances_for_reranker=1000,
          max_instances_for_adapter=1,
          indexer_path="./Index.index",
          rescore_metric=None,
          epoch_list=[1],  # [13, 1, 1, 3, 7, 11, 11, 13, 11, 13],
          lrate_list=[0.1],  # [0.01, 0.1, 0.5, 0.25, 0.5, 0.25, 0.5, 0.5, 0.5, 0.5]
          rescore_metric_ref='query',
          score_type_for_dmodel='best', # meadian, ...
          sim_thresh=0.5,
          metric_source_index=-1, # -1 = average all the scores
          query_repres = 'id',
          use_ensemble=False,
          normalize=False,
          print_word_probabilities=False,
          nbest=None,
          rescore=False
    ):
    time_start = time.time()

    batch_size = 1

    print >> sys.stderr, "NEMATUS PARAMETERS:"
    #print >> sys.stderr, locals()

    # Model options
    #model_options = locals().copy()
    model_options = OrderedDict(sorted(locals().copy().items()))
    sys.displayhook = pprint.pprint(model_options)

    if model_options['dim_per_factor'] == None:
        if factors == 1:
            model_options['dim_per_factor'] = [model_options['dim_word']]
        else:
            sys.stderr.write('Error: if using factored input, you must specify \'dim_per_factor\'\n')
            sys.exit(1)

    num_encoder = len(model_options['datasets']) - 1

    #assert(len(dictionaries) == factors + 1) # one dictionary per source factor + 1 for target factor
    #assert(len(model_options['dim_per_factor']) == factors) # each factor embedding has its own dimensionality
    #assert(sum(model_options['dim_per_factor']) == model_options['dim_word']) # dimensionality of factor embeddings sums up to total dimensionality of input embedding vector

    # load dictionaries and invert them
    worddicts = [None] * len(dictionaries)
    worddicts_r = [None] * len(dictionaries)
    for ii, dd in enumerate(dictionaries):
        worddicts[ii] = load_dict(dd)
        worddicts_r[ii] = dict()
        for kk, vv in worddicts[ii].iteritems():
            worddicts_r[ii][vv] = kk

    if n_words_src is None:
        n_words_src = len(worddicts[0])
        model_options['n_words_src'] = n_words_src
    if n_words is None:
        n_words = len(worddicts[-1])
        model_options['n_words'] = n_words

    print >>sys.stderr, 'Building model'
    params = init_params(model_options)

    best_p = None
    bad_counter = 0
    uidx = 0
    estop = False
    history_errs = []
    grad_history = None
    # reload history
    if reload_ and os.path.exists(saveto):
        print >> sys.stderr, 'save to is ', saveto
        params = load_params(saveto, params)
        print >>sys.stderr, 'Reloading the model'
        rmodel = numpy.load(saveto)
        history_errs = list(rmodel['history_errs'])
        #if 'ghist' in rmodel:
        #    grad_history = rmodel['ghist']
        if 'uidx' in rmodel:
            uidx = rmodel['uidx']

    tparams = init_theano_params(params)
    tparams_background = init_theano_params(params)

    trng, use_noise, \
        xs, x_masks, y, y_mask, \
        opt_ret, \
        cost = \
        build_model(tparams, model_options)

    inps = xs[:num_encoder] + x_masks[:num_encoder] + [y, y_mask]

    print >>sys.stderr, 'Building sampler'
    fs_init = []
    fs_next = []
    parameters = [tparams_background, tparams] if use_ensemble else [tparams]
    for p in parameters:
        f_init, f_next = build_sampler(p, model_options, use_noise, trng)
        fs_init.append(f_init)
        fs_next.append(f_next)

    if rescore:
        fs_log_probs = []

    #f_init, f_next = build_sampler(tparams, model_options, use_noise, trng)

    cost = cost.mean()

    # apply L2 regularization on weights
    if decay_c > 0.:
        decay_c = theano.shared(numpy.float32(decay_c), name='decay_c')
        weight_decay = 0.
        for kk, vv in tparams.iteritems():
            weight_decay += (vv ** 2).sum()
        weight_decay *= decay_c
        cost += weight_decay

    # regularize the alpha weights

    if alpha_c > 0. and not model_options['decoder'].endswith('simple'):
        alpha_c = theano.shared(numpy.float32(alpha_c), name='alpha_c')
        alpha_reg = alpha_c * (
            (tensor.cast(y_mask.sum(0)//x_mask.sum(0), 'float32')[:, None] -
             opt_ret['dec_alphas'].sum(0))**2).sum(1).mean()
        cost += alpha_reg


    # apply L2 regularisation to loaded model (map training)
    if map_decay_c > 0:
        map_decay_c = theano.shared(numpy.float32(map_decay_c), name="map_decay_c")
        weight_map_decay = 0.
        for kk, vv in tparams.iteritems():
            init_value = theano.shared(vv.get_value(), name= kk + "_init")
            weight_map_decay += ((vv -init_value) ** 2).sum()
        weight_map_decay *= map_decay_c
        cost += weight_map_decay


    # allow finetuning with fixed embeddings
    if finetune:
        updated_params = OrderedDict([(key,value) for (key,value) in tparams.iteritems() if not key.startswith('Wemb')])
    else:
        updated_params = tparams

    # allow finetuning of only last layer (becomes a linear model training problem)
    if finetune_only_last:
        updated_params = OrderedDict([(key,value) for (key,value) in tparams.iteritems() if key in ['ff_logit_W', 'ff_logit_b']])
    else:
        updated_params = tparams

    print >>sys.stderr, 'Computing gradient...',
    grads = tensor.grad(cost, wrt=itemlist(updated_params))
    print >>sys.stderr, 'Done'

    # apply gradient clipping here
    if clip_c > 0.:
        g2 = 0.
        for g in grads:
            g2 += (g**2).sum()
        new_grads = []
        for g in grads:
            new_grads.append(tensor.switch(g2 > (clip_c**2),
                                           g / tensor.sqrt(g2) * clip_c,
                                           g))
        grads = new_grads

    # compile the optimizer, the actual computational graph is compiled here
    lr = tensor.scalar(name='lr')

    print >>sys.stderr, 'Building optimizers...',
    f_grad_shared, f_update, grad_history = eval(optimizer)(lr, updated_params, grads, inps, cost, profile=profile, ghist=grad_history)
    print >>sys.stderr, 'Done'

    print >>sys.stderr, 'Optimization'

    if use_instance_selection:
        # General variables for instance selection

        chencherry = nltk.translate.bleu_score.SmoothingFunction()

        initVM = True

        if not os.path.exists(os.path.join(indexer_path)):
            initVM = False
            print >> sys.stderr, "WARNING: I COULD NOT FIND THE INDEX DIR. SO, I AM GOING TO INDEX THE BG CORPUS..."
            print >> sys.stderr, "Start creating Index"
            sys.stdout.flush()
            indexer = Indexer(os.path.join(indexer_path))

            print >> sys.stderr, "Finish creating Index"
            sys.stdout.flush()

            # Feed PyLucene with the generic data

            print >> sys.stderr, "start feeding PyLucene"
            sys.stdout.flush()

            print >> sys.stderr, "Initializing test iterator"
            train = TextIterator(datasets[:-1], datasets[-1],
                                 dictionaries[:-1], dictionaries[-1],
                                 n_words_source=n_words_src, n_words_target=n_words,
                                 batch_size=batch_size,
                                 maxlen=maxlen,
                                 shuffle_each_epoch=shuffle_each_epoch,
                                 sort_by_length=sort_by_length,
                                 maxibatch_size=maxibatch_size)

            id = 0
            start_lucene = time.time()
            for x, y, x_words, y_words in train: # WARNING: Batch should contain only one sentence, Not tested on factored input

                x_doc = [' '.join(map(str, numpy.array(x_).flatten())) for x_ in x]
                y_doc = ' '.join(map(str, numpy.array(y).flatten()))

                bpe = [' '.join(numpy.array(x_words_).flatten()) for x_words_ in x_words]
                x_doc_words = [bpe_.replace('@@ ', '') for bpe_ in bpe]

                bpe = ' '.join(numpy.array(y_words).flatten())
                y_doc_words = bpe.replace('@@ ', '')

                indexer.add(id, x_doc, y_doc, x_doc_words, y_doc_words)

                id += 1

                if (id % 100000) == 0:
                    print >> sys.stderr, "c: " + str(id)
                    sys.stdout.flush()
                    indexer.commit()
            indexer.commit()
            indexer.close()
            print >> sys.stderr, "finish feeding PyLucene adding: " + str(id)
            print >> sys.stderr, "Lucene time: " + str(time.time() - start_lucene)
        else:
            print >> sys.stderr, "WARNING: I FOUND THE LUCENE INDEXER DIRECTORY...!!!"
            print >> sys.stderr, "SO, I AM GOING TO RE-USE IT. IF YOU NEED TO RE-INDEX YOUR CORPUS, REMOVE THE INDEX DIR AND RE-RUN NEMATUS."

        # Initialise the Search
        searcher = Searcher(os.path.join(indexer_path), initVM=initVM)


    maxlen_test = float("inf")
    print_alignment = False
    epoch_list = [int(epc) for epc in epoch_list]
    lrate_list = [float(lr) for lr in lrate_list]
    num_epoch_bin = len(epoch_list)
    num_lrate_bin = len(lrate_list)
    # Translation output file name
    of = open(translation_fname, 'w')

    print >> sys.stderr, "Initializing test iterator"
    if test_datasets:
        test = TextIterator(test_datasets[:-1], test_datasets[-1],
                            dictionaries[:-1], dictionaries[-1],
                            n_words_source=n_words_src, n_words_target=n_words,
                            batch_size=batch_size,
                            maxlen=maxlen,
                            shuffle_each_epoch=False,
                            sort_by_length=False)

    n_samples = 0
    for xs, y, xs_words, y_words in test:
        time_total_start = time.time()
        if use_instance_selection and reset_model_always:
            copy_tparams(tparams_background, tparams)

        n_samples += len(y)
        uidx += 1

        if xs[0] is None:
            print >>sys.stderr, 'Minibatch with zero sample under length ', maxlen
            uidx -= 1
            continue

        time_retrieval = 0
        time_adaptation = 0
        dscore = 0.0
        time_retrieval_start = time.time()

        if use_instance_selection:
            d_query = {}

            if query_repres == 'id':
                for i in xrange(num_encoder):
                    d_query['source'+str(i)] = ' '.join(map(str, numpy.array(xs[i]).flatten()))
            else:
                for i in xrange(num_encoder):
                    bpe = ' '.join(numpy.array(xs_words[i]).flatten())
                    d_query['source_words' + str(i)] = bpe.replace('@@ ', '')

            maxDoc = max_instances_for_adapter if rescore_metric is None else max_instances_for_reranker
            retrieved_docs = searcher.query_multi_source(d_query, maxDoc, num_encoder)

            def rerank(d_query, docs, rescore_metric, rescore_metric_ref='query'):
                # to store all the retrieved training instances key:score, value:(training pair)
                all_retrieved = defaultdict(list) #OrderedDict()

                if query_repres == 'id':
                    x_query = [d_query['source'+str(i)].split() for i in xrange(num_encoder)]
                else:
                    x_query = [d_query['source_words' + str(i)].split() for i in xrange(num_encoder)]

                for ids in range(len(docs)):
                    metric_scores = []
                    if query_repres == 'id':
                        retrieved_source_flat = [source.split() for source in docs[ids][2]]
                    else:
                        retrieved_source_flat = [source.split() for source in docs[ids][4]]

                    if rescore_metric == 'BLEU':
                        if rescore_metric_ref == 'query':
                            for i in xrange(len(x_query)):
                                metric_scores.append(float(nltk.translate.bleu_score.sentence_bleu([x_query[i]], retrieved_source_flat[i],smoothing_function=chencherry.method1)))
                        else:
                            for i in xrange(len(x_query)):
                                metric_scores.append(float(nltk.translate.bleu_score.sentence_bleu([retrieved_source_flat[i]], x_query[i],smoothing_function=chencherry.method1)))

                        retrieved_score =  numpy.array(metric_scores).mean() if metric_source_index == -1 else metric_scores[metric_source_index]

                    elif rescore_metric == 'LD':
                        if rescore_metric_ref == 'query':
                            for i in xrange(len(x_query)):
                                metric_scores.append(pyter.levenshtein(x_query[i], retrieved_source_flat[i], normalize=True))
                        else:
                            for i in xrange(len(x_query)):
                                metric_scores.append(
                                    pyter.levenshtein(retrieved_source_flat[i], x_query[i], normalize=True))

                        retrieved_score = 1-(numpy.array(metric_scores).mean() if metric_source_index == -1 else metric_scores[metric_source_index])
                    else:
                        retrieved_score = docs[ids][1]
                        metric_scores.append(docs[ids][1])

                    all_retrieved[retrieved_score].append([docs[ids], metric_scores])

                return OrderedDict(sorted(all_retrieved.items(), reverse=True))

            retrieved_docs = rerank(d_query, retrieved_docs, rescore_metric, rescore_metric_ref)

            def format_string(string, dimentions=1):
                string = numpy.array(map(int, string.split()))

                if dimentions == 2:
                    return string[:,None].tolist()
                return string.tolist()

            # sort the retrieved documents by key and take the top n
            selected_source = [[]] * num_encoder
            selected_target = []
            selected_scores = []
            j = 0
            for kscore in retrieved_docs:
                if kscore < sim_thresh or j >= max_instances_for_adapter:
                    break

                for rd in retrieved_docs[kscore]:
                    metric_value = rd[1]
                    print >> sys.stderr, 'SampleID:', n_samples, 'Sim_Multisrc:', metric_value
                    for i in xrange(num_encoder):
                        selected_source[i] = selected_source[i] + [format_string(rd[0][2][i], 2)]
                        print >> sys.stderr, "Sample_Source[", i, "]", selected_source[i][-1]

                    selected_target.append(format_string(rd[0][3], 1))
                    print >> sys.stderr, "Sample_Target", selected_target[-1]

                    selected_scores.append(kscore)
                    j += 1
                    if j >= max_instances_for_adapter:
                        break

            print >> sys.stderr, 'Batch length:', len(selected_target), 'SampleID:',n_samples

            time_retrieval = time.time() - time_retrieval_start
            time_adaptation_start = time.time()

            if selected_source[0]:
                if score_type_for_dmodel == 'best':
                    dscore = numpy.max(selected_scores)
                elif score_type_for_dmodel == 'average':
                    dscore = numpy.mean(selected_scores)

                max_epochs = epoch_list[bin_(dscore, num_epoch_bin)]
                lrate = lrate_list[bin_(dscore, num_lrate_bin)]
                use_noise.set_value(1.0)
                similar_x, similar_x_mask, similar_y, similar_y_mask = prepare_data(selected_source,
                                                                                    selected_target, maxlen=maxlen,
                                                                                    n_words_src=n_words_src,
                                                                                    n_words=n_words)

                for epc in xrange(max_epochs):
                    cost = f_grad_shared(similar_x[0], similar_x[1], similar_x_mask[0], similar_x_mask[1], similar_y, similar_y_mask)  ## CHECK this
                    f_update(lrate)
            else:
                print >> sys.stderr, "Training instances not found for sample ID:", n_samples
            time_adaptation = time.time() - time_adaptation_start

        errors = []
        words = []
        xs, x_masks, y, y_mask = prepare_data(xs, y, maxlen=maxlen, n_words_src=n_words_src, n_words=n_words)

        time_translation_start = time.time()

        for jj in xrange(xs[0].shape[2]):
            stochastic = False

            x_current = [[]] * num_encoder

            for i in xrange(num_encoder):
                x_current[i] = xs[i][:, :, jj][:, :, None]
                source_len = int(sum(x_masks[i][:, jj]))
                x_current[i] = x_current[i][:, :source_len, :]

            use_noise.set_value(0.)
            samples, scores, sample_word_probs, alignment = gen_sample(fs_init, fs_next,
                                                                     x_current,
                                                                     trng=trng, k=12,
                                                                     maxlen=maxlen,
                                                                     stochastic=stochastic,
                                                                     argmax=False,
                                                                     return_alignment=print_alignment,
                                                                     suppress_unk=False)
            '''
            sample, score, sample_word_probs, alignment = gen_sample([f_init], [f_next],
                                                                      x_current,
                                                                      trng=trng, k=12,
                                                                      maxlen=maxlen,
                                                                      stochastic=stochastic,
                                                                      argmax=False,
                                                                      return_alignment=print_alignment,
                                                                      suppress_unk=False)
            '''
            use_noise.set_value(1.)

            if normalize:
                lengths = numpy.array([len(s) for s in samples])
                scores = scores / lengths

            if nbest:
                order = numpy.argsort(scores)
                for j in order:
                    if print_word_probabilities:
                        probs = " ||| " + " ".join("{0}".format(prob) for prob in sample_word_probs[j])
                    else:
                        probs = ""
                    of.write('{0} ||| {1} ||| {2}{3}\n'.format(i, _seqs2words_trg(samples[j],worddicts_r[-1]), scores[j], probs))
            else:
                minidx = numpy.argmin(scores)
                of.write(_seqs2words_trg(samples[minidx],worddicts_r[-1]) + "\n")
                if print_word_probabilities:
                    for prob in sample_word_probs[minidx]:
                        of.write("{} ".format(prob[minidx]))
                        of.write('\n')
            of.flush()

            '''
            print >> sys.stderr, "Test documentID:", jj
            for i in xrange(num_encoder):
                sentenceSource = ""
                print >> sys.stderr, 'Source[', i, ']:',
                for pos in range(xs[i].shape[1]):
                    if xs[i][0, pos, jj] == 0:
                        break
                    vv = xs[i][0, pos, jj]
                    if vv in worddicts_r[i]:
                        sentenceSource += worddicts_r[i][vv]
                    else:
                        sentenceSource += 'UNK'
                    sentenceSource += ' '
                print >> sys.stderr, sentenceSource

            sentenceRef = ""
            print >> sys.stderr, 'Truth:',
            for vv in y[:, jj]:
                if vv == 0:
                    break
                if vv in worddicts_r[-1]:
                    sentenceRef += worddicts_r[-1][vv] + ' '
                else:
                    sentenceRef += 'UNK'
            sentenceRef = sentenceRef.strip()
            print >> sys.stderr, sentenceRef

            print >> sys.stderr, 'Sample:',
            sentenceTarget = ""
            if stochastic:
                ss = sample
            else:
                lengths = numpy.array([len(s) for s in sample])
                score = score / lengths
                ss = sample[score.argmin()]
            for vv in ss:
                if vv == 0:
                    break
                if vv in worddicts_r[-1]:
                    sentenceTarget += worddicts_r[-1][vv] + ' '
                else:
                    sentenceTarget += 'UNK '
            print >> sys.stderr, sentenceTarget

            of.write(sentenceTarget.strip() + "\n")
            '''

        time_translation = time.time() - time_translation_start
        time_total = time.time() - time_total_start
        print >> sys.stderr, 'SentenceID:', uidx, 'Sim:', dscore, 'Cost:', cost, 'total_time:', time_total, 'retrieval_time:', time_retrieval, 'adaptation_time:', time_adaptation, 'translation_time:', time_translation
        print >> sys.stderr, "=========="

    return None


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    data = parser.add_argument_group('data sets; model loading and saving')
    data.add_argument('--datasets', type=str, required=True, metavar='PATH', nargs=2,
                         help="parallel training corpus (source and target)")
    data.add_argument('--dictionaries', type=str, required=True, metavar='PATH', nargs="+",
                         help="network vocabularies (one per source factor, plus target vocabulary)")
    data.add_argument('--model', type=str, default='model.npz', metavar='PATH', dest='saveto',
                         help="model file name (default: %(default)s)")
    data.add_argument('--saveFreq', type=int, default=30000, metavar='INT',
                         help="save frequency (default: %(default)s)")
    data.add_argument('--reload_', action='store_true',
                         help="load existing model (if '--model' points to existing model)")
    data.add_argument('--overwrite', action='store_true',
                         help="write all models to same file")

    network = parser.add_argument_group('network parameters')
    network.add_argument('--dim_word', type=int, default=512, metavar='INT',
                         help="embedding layer size (default: %(default)s)")
    network.add_argument('--dim', type=int, default=1000, metavar='INT',
                         help="hidden layer size (default: %(default)s)")
    network.add_argument('--n_words_src', type=int, default=None, metavar='INT',
                         help="source vocabulary size (default: %(default)s)")
    network.add_argument('--n_words', type=int, default=None, metavar='INT',
                         help="target vocabulary size (default: %(default)s)")

    network.add_argument('--factors', type=int, default=1, metavar='INT',
                         help="number of input factors (default: %(default)s)")
    network.add_argument('--dim_per_factor', type=int, default=None, nargs='+', metavar='INT',
                         help="list of word vector dimensionalities (one per factor): '--dim_per_factor 250 200 50' for total dimensionality of 500 (default: %(default)s)")
    network.add_argument('--use_dropout', action="store_true",
                         help="use dropout layer (default: %(default)s)")
    network.add_argument('--dropout_embedding', type=float, default=0.2, metavar="FLOAT",
                         help="dropout for input embeddings (0: no dropout) (default: %(default)s)")
    network.add_argument('--dropout_hidden', type=float, default=0.2, metavar="FLOAT",
                         help="dropout for hidden layer (0: no dropout) (default: %(default)s)")
    network.add_argument('--dropout_source', type=float, default=0, metavar="FLOAT",
                         help="dropout source words (0: no dropout) (default: %(default)s)")
    network.add_argument('--dropout_target', type=float, default=0, metavar="FLOAT",
                         help="dropout target words (0: no dropout) (default: %(default)s)")
    #network.add_argument('--encoder', type=str, default='gru',
                         #choices=['gru'],
                         #help='encoder recurrent layer')
    #network.add_argument('--decoder', type=str, default='gru_cond',
                         #choices=['gru_cond'],
                         #help='decoder recurrent layer')

    training = parser.add_argument_group('training parameters')
    training.add_argument('--maxlen', type=int, default=100, metavar='INT',
                         help="maximum sequence length (default: %(default)s)")
    training.add_argument('--optimizer', type=str, default="adam",
                         choices=['adam', 'adadelta', 'rmsprop', 'sgd'],
                         help="optimizer (default: %(default)s)")
    training.add_argument('--batch_size', type=int, default=80, metavar='INT',
                         help="minibatch size (default: %(default)s)")
    training.add_argument('--max_epochs', type=int, default=5000, metavar='INT',
                         help="maximum number of epochs (default: %(default)s)")
    training.add_argument('--finish_after', type=int, default=10000000, metavar='INT',
                         help="maximum number of updates (minibatches) (default: %(default)s)")
    training.add_argument('--decay_c', type=float, default=0, metavar='FLOAT',
                         help="L2 regularization penalty (default: %(default)s)")
    training.add_argument('--map_decay_c', type=float, default=0, metavar='FLOAT',
                         help="L2 regularization penalty towards original weights (default: %(default)s)")
    training.add_argument('--alpha_c', type=float, default=0, metavar='FLOAT',
                         help="alignment regularization (default: %(default)s)")
    training.add_argument('--clip_c', type=float, default=1, metavar='FLOAT',
                         help="gradient clipping threshold (default: %(default)s)")
    training.add_argument('--lrate', type=float, default=0.0001, metavar='FLOAT',
                         help="learning rate (default: %(default)s)")
    training.add_argument('--no_shuffle', action="store_false", dest="shuffle_each_epoch",
                         help="disable shuffling of training data (for each epoch)")
    training.add_argument('--no_sort_by_length', action="store_false", dest="sort_by_length",
                         help='do not sort sentences in maxibatch by length')
    training.add_argument('--maxibatch_size', type=int, default=20, metavar='INT',
                         help='size of maxibatch (number of minibatches that are sorted by length) (default: %(default)s)')
    finetune = training.add_mutually_exclusive_group()
    finetune.add_argument('--finetune', action="store_true",
                        help="train with fixed embedding layer")
    finetune.add_argument('--finetune_only_last', action="store_true",
                        help="train with all layers except output layer fixed")

    validation = parser.add_argument_group('validation parameters')
    validation.add_argument('--valid_datasets', type=str, default=None, metavar='PATH', nargs=2,
                         help="parallel validation corpus (source and target) (default: %(default)s)")
    validation.add_argument('--valid_batch_size', type=int, default=80, metavar='INT',
                         help="validation minibatch size (default: %(default)s)")
    validation.add_argument('--validFreq', type=int, default=10000, metavar='INT',
                         help="validation frequency (default: %(default)s)")
    validation.add_argument('--patience', type=int, default=10, metavar='INT',
                         help="early stopping patience (default: %(default)s)")
    validation.add_argument('--external_validation_script', type=str, default=None, metavar='PATH',
                         help="location of validation script (to run your favorite metric for validation) (default: %(default)s)")

    display = parser.add_argument_group('display parameters')
    display.add_argument('--dispFreq', type=int, default=1000, metavar='INT',
                         help="display loss after INT updates (default: %(default)s)")
    display.add_argument('--sampleFreq', type=int, default=10000, metavar='INT',
                         help="display some samples after INT updates (default: %(default)s)")


    args = parser.parse_args()

    train(**vars(args))
