import sys
import argparse
import nltk

def levenshtein(s1, s2):
    if len(s1) < len(s2):
        return levenshtein(s2, s1)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[
                             j + 1] + 1  # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1  # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row

    return previous_row[-1]


def longest_common_substring(s1, s2):
   m = [[0] * (1 + len(s2)) for i in xrange(1 + len(s1))]
   longest, x_longest = 0, 0
   for x in xrange(1, 1 + len(s1)):
       for y in xrange(1, 1 + len(s2)):
           if s1[x - 1] == s2[y - 1]:
               m[x][y] = m[x - 1][y - 1] + 1
               if m[x][y] > longest:
                   longest = m[x][y]
                   x_longest = x
           else:
               m[x][y] = 0
   return s1[x_longest - longest: x_longest], len(s1[x_longest - longest: x_longest])

def sentBleu(ref,hyp):
    chencherry = nltk.translate.bleu_score.SmoothingFunction()
    return float(nltk.translate.bleu_score.sentence_bleu([ref], hyp, smoothing_function=chencherry.method1))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--ref', type=argparse.FileType('r'), help="Source File")
    parser.add_argument('--hyp', type=argparse.FileType('r'), help="Target File")
    parser.add_argument('--output', '-o', type=argparse.FileType('w'), default=sys.stdout, metavar='PATH',
                        help="Output file (default: standard output)")



    args = parser.parse_args()


    for r,h in zip(args.ref, args.hyp):
        print round(sentBleu(r.strip().split(), h.strip().split()),4)
        #print levenshtein(s.strip(), t.strip())
        #print levenshtein(s.strip().split(),t.strip().split())
        #print longest_common_substring(s.strip().split(), t.strip().split())
