'''
Translates a source file using a translation model.
'''
import sys
import argparse

import numpy
import json
import cPickle as pkl

from multiprocessing import Process, Queue
from util import load_dict, load_config
from compat import fill_options

import datetime
import logging
import os
import subprocess
import re
logging.basicConfig(filename='translate.log',level=logging.DEBUG)

def translate_model(queue, rqueue, pid, models, options, k, normalize, verbose, nbest, return_alignment, suppress_unk):

    from theano_util import (load_params, init_theano_params)
    from nmt_train import (build_sampler, gen_sample, init_params)

    from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
    from theano import shared
    trng = RandomStreams(1234)
    use_noise = shared(numpy.float32(0.))

    fs_init = []
    fs_next = []

    for model, option in zip(models, options):

        # allocate model parameters
        params = init_params(option)

        # load model parameters and set theano shared variables
        params = load_params(model, params)
        tparams = init_theano_params(params)

        # word index
        f_init, f_next = build_sampler(tparams, option, use_noise, trng, return_alignment=return_alignment)

        fs_init.append(f_init)
        fs_next.append(f_next)

    def _translate(seq):
        # sample given an input sequence and obtain scores
        sample, score, word_probs, alignment = gen_sample(fs_init, fs_next,
                                   numpy.array(seq).T.reshape([len(seq[0]), len(seq), 1]),
                                   trng=trng, k=k, maxlen=200,
                                   stochastic=False, argmax=False, return_alignment=return_alignment, suppress_unk=suppress_unk)

        # normalize scores according to sequence lengths
        if normalize:
            lengths = numpy.array([len(s) for s in sample])
            score = score / lengths
        if nbest:
            return sample, score, word_probs, alignment
        else:
            sidx = numpy.argmin(score)
            return sample[sidx], score[sidx], word_probs[sidx], alignment[sidx]

    while True:
        req = queue.get()
        if req is None:
            break

        idx, x = req[0], req[1]
        if verbose:
            sys.stderr.write('{0} - {1}\n'.format(pid,idx))
        seq = _translate(x)

        rqueue.put((idx, seq))

    return

# prints alignment weights for a hypothesis
# dimension (target_words+1 * source_words+1)
def print_matrix(hyp, file):
  # each target word has corresponding alignment weights
  for target_word_alignment in hyp:
    # each source hidden state has a corresponding weight
    for w in target_word_alignment:
      print >>file, w,
    print >> file, ""
  print >> file, ""

import json, io
def print_matrix_json(hyp, source, target, sid, tid, file):
  source.append("</s>")
  target.append("</s>")
  links = []
  for ti, target_word_alignment in enumerate(hyp):
    for si,w in enumerate(target_word_alignment):
      links.append((target[ti], source[si], str(w), sid, tid))
  json.dump(links,file, ensure_ascii=False)


def print_matrices(mm, file):
  for hyp in mm:
    print_matrix(hyp, file)
    print >>file, "\n"


def translation_server(models, k=5, normalize=False, verbose=False, suppress_unk=False):

    logging.debug( "Starting the translation server")

    command_truecaser = ["perl", "/home/farajian/NLP/tools/NMT/nematus-master.translation-server/nematus/preprocessing-scripts/truecase.perl",
               "--model", "/home/farajian/NLP/tools/NMT/nematus-master.translation-server/nematus/TranServerModels/truecaser/model.truecaser.fr-en.en"]
    process_truecase = subprocess.Popen(command_truecaser, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                               shell=False)
    logging.debug("Truecasing server is up")

    command_tokenizer = ["perl",
                         "/home/farajian/NLP/tools/NMT/nematus-master.translation-server/nematus/preprocessing-scripts/tokenizer/tokenizer.perl",
                         "-l", "en"
                         "-threads", "1",
                         "-q",
                         "-b"]
    process_tokenizer = subprocess.Popen(command_tokenizer, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE,
                                        shell=False)

    logging.debug("Tokenizer server is up")
    command_segmenter = ["python",
                         "/home/farajian/NLP/tools/NMT/nematus-15.11.2016/data/apply_bpe.py",
                         "-c", "/home/farajian/NLP/tools/NMT/nematus-master.translation-server/nematus/TranServerModels/en-it/bpe.joint.90000.model"]
    process_segmenter = subprocess.Popen(command_segmenter, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE,
                                        shell=False)
    logging.debug("Segmenter server is up")

    command_detok = ["perl",
                     "/home/farajian/NLP/tools/NMT/nematus-15.11.2016/data/detokenizer.perl",
                     "-u",
                     "-l",
                     "it"]
    process_detok = subprocess.Popen(command_detok, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                                         stderr=subprocess.PIPE,
                                         shell=False)
    logging.debug("Detokenizer server is up")

    '''
    while True:
        sys.stdout.write("ENTER YOUR TEXT: ")
        line = sys.stdin.readline().strip()
        process_detok.stdin.write(line + "\n")
        process_detok.stdin.flush()
        req_detok = process_detok.stdout.readline().rstrip()
        sys.stdout.write(req_detok + "\n")
        sys.stdout.flush()
    '''
    '''
    while True:
        req = sys.stdin.readline().strip()
        process_tokenizer.stdin.write(req + "\n")
        process_tokenizer.stdin.flush()
        logging.debug("sent |%s|" % req)
        answer = process_tokenizer.stdout.readline().rstrip()
        logging.debug("received |%s|" % answer)
        #response_body = answer
    '''

    startLoadingServer = datetime.datetime.now()

    '''
    # load model model_options
    options = []
    for model in args.models:
        try:
            with open('%s.json' % model, 'rb') as f:
                options.append(json.load(f))
        except:
            with open('%s.pkl' % model, 'rb') as f:
                options.append(pkl.load(f))

        # hacks for using old models with missing options
        if not 'dropout_embedding' in options[-1]:
            options[-1]['dropout_embedding'] = 0
        if not 'dropout_hidden' in options[-1]:
            options[-1]['dropout_hidden'] = 0
        if not 'dropout_source' in options[-1]:
            options[-1]['dropout_source'] = 0
        if not 'dropout_target' in options[-1]:
            options[-1]['dropout_target'] = 0

    dictionary, dictionary_target = options[0]['dictionaries']

    # load source dictionary and invert
    word_dict = load_dict(dictionary)
    word_idict = dict()
    for kk, vv in word_dict.iteritems():
        word_idict[vv] = kk
    word_idict[0] = '<eos>'
    word_idict[1] = 'UNK'

    # load target dictionary and invert
    word_dict_trg = load_dict(dictionary_target)
    word_idict_trg = dict()
    for kk, vv in word_dict_trg.iteritems():
        word_idict_trg[vv] = kk
    word_idict_trg[0] = '<eos>'
    word_idict_trg[1] = 'UNK'
    '''
    '''
    # create input and output queues for processes
    from nmt import (build_sampler, gen_sample, load_params,
                     init_params, init_theano_params)

    from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
    from theano import shared
    trng = RandomStreams(1234)
    use_noise = shared(numpy.float32(0.))

    fs_init = []
    fs_next = []

    for model, option in zip(models, options):
        # allocate model parameters
        params = init_params(option)

        # load model parameters and set theano shared variables
        params = load_params(model, params)
        tparams = init_theano_params(params)

        # word index
        f_init, f_next = build_sampler(tparams, option, use_noise, trng)

        fs_init.append(f_init)
        fs_next.append(f_next)
    '''

    ### This part comes from main function
    # load model model_options
    options = []
    for model in models:
        options.append(load_config(model))
        fill_options(options[-1])
    dictionaries = options[0]['dictionaries']

    dictionaries_source = dictionaries[:-1]
    dictionary_target = dictionaries[-1]

    # load source dictionary and invert
    word_dicts = []
    word_idicts = []
    for dictionary in dictionaries_source:
        word_dict = load_dict(dictionary)
        if options[0]['n_words_src']:
            for key, idx in word_dict.items():
                if idx >= options[0]['n_words_src']:
                    del word_dict[key]
        word_idict = dict()
        for kk, vv in word_dict.iteritems():
            word_idict[vv] = kk
        word_idict[0] = '<eos>'
        word_idict[1] = 'UNK'
        word_dicts.append(word_dict)
        word_idicts.append(word_idict)

    # load target dictionary and invert
    word_dict_trg = load_dict(dictionary_target)
    word_idict_trg = dict()
    for kk, vv in word_dict_trg.iteritems():
        word_idict_trg[vv] = kk
    word_idict_trg[0] = '<eos>'
    word_idict_trg[1] = 'UNK'

    logging.debug("3")
    ### The following part comes from translate_model
    from theano_util import (load_params, init_theano_params)
    logging.debug("3.1")
    from nmt import (build_sampler, gen_sample, init_params)

    logging.debug("4")
    from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
    from theano import shared
    trng = RandomStreams(1234)
    use_noise = shared(numpy.float32(0.))

    fs_init = []
    fs_next = []

    logging.debug("5")
    for model, option in zip(models, options):
        # allocate model parameters
        params = init_params(option)

        # load model parameters and set theano shared variables
        params = load_params(model, params)
        tparams = init_theano_params(params)

        # word index
        f_init, f_next = build_sampler(tparams, option, use_noise, trng, return_alignment=False)
        logging.debug("6")
        fs_init.append(f_init)
        fs_next.append(f_next)

    logging.debug("7")
    endLoadingServer = datetime.datetime.now()
    logging.debug("8")
    sys.stderr.write("Server is up\n")
    sys.stderr.flush()
    logging.debug("Time2LoadServer: " + str((endLoadingServer - startLoadingServer).total_seconds()) + " (seconds)\n")

    def _translate(seq):
        # sample given an input sequence and obtain scores
        sample, score, word_probs, alignment = gen_sample(fs_init, fs_next,
                                                          numpy.array(seq).T.reshape([len(seq[0]), len(seq), 1]),
                                                          trng=trng, k=k, maxlen=200,
                                                          stochastic=False, argmax=False,
                                                          return_alignment=False, suppress_unk=suppress_unk)

        # normalize scores according to sequence lengths
        if normalize:
            lengths = numpy.array([len(s) for s in sample])
            score = score / lengths
        #if nbest:
        #    return sample, score, word_probs, alignment
        #else:
        sidx = numpy.argmin(score)
        return sample[sidx], score[sidx], word_probs[sidx], alignment[sidx]

    '''
    def _translate(seq):
        # sample given an input sequence and obtain scores
        sample, score, word_probs, alignment = gen_sample(fs_init, fs_next,
                                                          numpy.array(seq).reshape([len(seq), 1]),
                                                          trng=trng, k=k, maxlen=200,
                                                          stochastic=False, argmax=False,
                                                          return_alignment=False, suppress_unk=suppress_unk)

        # normalize scores according to sequence lengths
        if normalize:
            lengths = numpy.array([len(s) for s in sample])
            score = score / lengths
        #if nbest:
        #    return sample, score, word_probs, alignment
        #else:
        sidx = numpy.argmin(score)
        return sample[sidx], score[sidx], word_probs[sidx], alignment[sidx]
    '''
    ########## Functions for preparing the input
    def _seqs2words(cc):
        ww = []
        for w in cc:
            if w == 0:
                break
            ww.append(word_idict_trg[w])
        return ' '.join(ww)

    def _send_jobs(line):
        #source_sentences = []
        #for idx, line in enumerate(f):
        words = line.strip().split()

        x = []
        for w in words:
            w = [word_dicts[i][f] if f in word_dicts[i] else 1 for (i, f) in enumerate(w.split('|'))]
            if len(w) != options[0]['factors']:
                sys.stderr.write('Error: expected {0} factors, but input word has {1}\n'.format(options[0]['factors'], len(w)))
                sys.exit(1)
            #for midx in xrange(n_process):
            #processes[midx].terminate()

            x.append(w)

        x += [[0] * options[0]['factors']]
            #queue.put((idx, x))
            #source_sentences.append(words)
        #return idx + 1, source_sentences
        return x


    '''
    def _send_jobs(line):
        #source_sentences = []

        words = line.strip().split()
        x = map(lambda w: word_dict[w] if w in word_dict else 1, words)
        x = map(lambda ii: ii if ii < options[0]['n_words_src'] else 1, x)
        x += [0]
        return x
        #source_sentences.append(words)
        #return idx+1, source_sentences
    '''
    ##########

    while True:
        req = sys.stdin.readline().strip()
        logging.debug("Translation request (original): |%s|" %req)

        # Step1. Tokenize the given string
        process_tokenizer.stdin.write(req + "\n")
        process_tokenizer.stdin.flush()
        req_tok = process_tokenizer.stdout.readline().rstrip()
        logging.debug("Translation request (tokenized): |%s|" % req_tok)

        # Step2. Truecase it
        process_truecase.stdin.write(req_tok + "\n")
        process_truecase.stdin.flush()
        req_true = process_truecase.stdout.readline().rstrip()
        logging.debug("Translation request (truecased): |%s|" % req_true)
        #logging.debug("Original Input |%s|" % req)


        process_segmenter.stdin.write(req_true + "\n")
        process_segmenter.stdin.flush()
        req_seg = process_segmenter.stdout.readline().rstrip()
        logging.debug("Translation request (segmented): |%s|" % req_seg)


        # Step3. Translate it
        startTrans = datetime.datetime.now()

        req = _send_jobs(req_seg)
        #req = _send_jobs(req_true)
        trans_list = _translate(req)

        endTrans = datetime.datetime.now()

        trans_string = _seqs2words(trans_list[0]).replace("UNK", "...")
        if trans_string.endswith("@@"):
            trans_string = re.sub("@@$", "", trans_string)
        trans_string = re.sub("@@ ", "", trans_string)

        process_detok.stdin.write(trans_string + "\n")
        process_detok.stdin.flush()
        trans_string = process_detok.stdout.readline().rstrip()
        logging.debug("Translation request (detokenized): |%s|" % trans_string)

        logging.debug("Translation answer: |%s|" %trans_string)
        sys.stdout.write(trans_string+"\n")
        sys.stdout.flush()

        logging.debug("Time2Translate: %f seconds" %(endTrans-startTrans).total_seconds())



def main(models, source_file, saveto, save_alignment=None, k=5,
         normalize=False, n_process=5, chr_level=False, verbose=False, nbest=False, suppress_unk=False, a_json=False, print_word_probabilities=False):
    # load model model_options
    options = []
    for model in models:
        options.append(load_config(model))

        fill_options(options[-1])

    dictionaries = options[0]['dictionaries']

    dictionaries_source = dictionaries[:-1]
    dictionary_target = dictionaries[-1]

    # load source dictionary and invert
    word_dicts = []
    word_idicts = []
    for dictionary in dictionaries_source:
        word_dict = load_dict(dictionary)
        if options[0]['n_words_src']:
            for key, idx in word_dict.items():
                if idx >= options[0]['n_words_src']:
                    del word_dict[key]
        word_idict = dict()
        for kk, vv in word_dict.iteritems():
            word_idict[vv] = kk
        word_idict[0] = '<eos>'
        word_idict[1] = 'UNK'
        word_dicts.append(word_dict)
        word_idicts.append(word_idict)

    # load target dictionary and invert
    word_dict_trg = load_dict(dictionary_target)
    word_idict_trg = dict()
    for kk, vv in word_dict_trg.iteritems():
        word_idict_trg[vv] = kk
    word_idict_trg[0] = '<eos>'
    word_idict_trg[1] = 'UNK'

    # create input and output queues for processes
    queue = Queue()
    rqueue = Queue()
    processes = [None] * n_process
    for midx in xrange(n_process):
        processes[midx] = Process(
            target=translate_model,
            args=(queue, rqueue, midx, models, options, k, normalize, verbose, nbest, save_alignment is not None, suppress_unk))
        processes[midx].start()

    # utility function
    def _seqs2words(cc):
        ww = []
        for w in cc:
            if w == 0:
                break
            ww.append(word_idict_trg[w])
        return ' '.join(ww)

    def _send_jobs(f):
        source_sentences = []
        for idx, line in enumerate(f):
            if chr_level:
                words = list(line.decode('utf-8').strip())
            else:
                words = line.strip().split()

            x = []
            for w in words:
                w = [word_dicts[i][f] if f in word_dicts[i] else 1 for (i,f) in enumerate(w.split('|'))]
                if len(w) != options[0]['factors']:
                    sys.stderr.write('Error: expected {0} factors, but input word has {1}\n'.format(options[0]['factors'], len(w)))
                    for midx in xrange(n_process):
                        processes[midx].terminate()
                    sys.exit(1)
                x.append(w)

            x += [[0]*options[0]['factors']]
            queue.put((idx, x))
            source_sentences.append(words)
        return idx+1, source_sentences

    def _finish_processes():
        for midx in xrange(n_process):
            queue.put(None)

    def _retrieve_jobs(n_samples):
        trans = [None] * n_samples
        out_idx = 0
        for idx in xrange(n_samples):
            resp = rqueue.get()
            trans[resp[0]] = resp[1]
            if verbose and numpy.mod(idx, 10) == 0:
                sys.stderr.write('Sample {0} / {1} Done\n'.format((idx+1), n_samples))
            while out_idx < n_samples and trans[out_idx] != None:
                yield trans[out_idx]
                out_idx += 1

    sys.stderr.write('Translating {0} ...\n'.format(source_file.name))
    n_samples, source_sentences = _send_jobs(source_file)
    _finish_processes()

    for i, trans in enumerate(_retrieve_jobs(n_samples)):
        if nbest:
            samples, scores, word_probs, alignment = trans
            order = numpy.argsort(scores)
            for j in order:
                if print_word_probabilities:
                    probs = " ||| " + " ".join("{0}".format(prob) for prob in word_probs[j])
                else:
                    probs = ""
                saveto.write('{0} ||| {1} ||| {2}{3}\n'.format(i, _seqs2words(samples[j]), scores[j], probs))
                # print alignment matrix for each hypothesis
                # header: sentence id ||| translation ||| score ||| source ||| source_token_count+eos translation_token_count+eos
                if save_alignment is not None:
                  if a_json:
                    print_matrix_json(alignment[j], source_sentences[i], _seqs2words(samples[j]).split(), i, i+j,save_alignment)
                  else:
                    save_alignment.write('{0} ||| {1} ||| {2} ||| {3} ||| {4} {5}\n'.format(
                                        i, _seqs2words(samples[j]), scores[j], ' '.join(source_sentences[i]) , len(source_sentences[i])+1, len(samples[j])))
                    print_matrix(alignment[j], save_alignment)
        else:
            samples, scores, word_probs, alignment = trans
    
            saveto.write(_seqs2words(samples) + "\n")
            if print_word_probabilities:
                for prob in word_probs:
                    saveto.write("{} ".format(prob))
                saveto.write('\n')
            if save_alignment is not None:
              if a_json:
                print_matrix_json(trans[1], source_sentences[i], _seqs2words(trans[0]).split(), i, i,save_alignment)
              else:
                save_alignment.write('{0} ||| {1} ||| {2} ||| {3} ||| {4} {5}\n'.format(
                                      i, _seqs2words(trans[0]), 0, ' '.join(source_sentences[i]) , len(source_sentences[i])+1, len(trans[0])))
                print_matrix(trans[3], save_alignment)

    sys.stderr.write('Done\n')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-k', type=int, default=5,
                        help="Beam size (default: %(default)s))")
    parser.add_argument('-p', type=int, default=5,
                        help="Number of processes (default: %(default)s))")
    parser.add_argument('-n', action="store_true",
                        help="Normalize scores by sentence length")
    parser.add_argument('-c', action="store_true", help="Character-level")
    parser.add_argument('-v', action="store_true", help="verbose mode.")
    parser.add_argument('--models', '-m', type=str, nargs = '+', required=True,
                        help="model to use. Provide multiple models (with same vocabulary) for ensemble decoding")
    parser.add_argument('--input', '-i', type=argparse.FileType('r'),
                        default=sys.stdin, metavar='PATH',
                        help="Input file (default: standard input)")
    parser.add_argument('--output', '-o', type=argparse.FileType('w'),
                        default=sys.stdout, metavar='PATH',
                        help="Output file (default: standard output)")
    parser.add_argument('--output_alignment', '-a', type=argparse.FileType('w'),
                        default=None, metavar='PATH',
                        help="Output file for alignment weights (default: standard output)")
    parser.add_argument('--json_alignment', action="store_true",
                        help="Output alignment in json format")
    parser.add_argument('--n-best', action="store_true",
                        help="Write n-best list (of size k)")
    parser.add_argument('--suppress-unk', action="store_true", help="Suppress hypotheses containing UNK.")
    parser.add_argument('--print-word-probabilities', '-wp',action="store_true", help="Print probabilities of each word")
    parser.add_argument('--server', '-s', action="store_true",
                        help="Start the translation session in SERVER mode.")

    args = parser.parse_args()

    logging.debug("Starting TRANSLATE.PY")
    logging.debug("SERVER MODE: %r", args.server)
    if args.server:
        logging.debug("IF_SERVERMODE")
        '''
        if not args.p == 1:
            sys.stderr.write("WARNING: Number of precesses in the server mode has to be {0}. We are going to start the server with {0} active process.\n".format((1)))
        if not args.input== sys.stdin:
            sys.stderr.write("WARNING: The input stream in the server mode has to be STDIN. We are going to open STDIN as the input stream.")
        if not args.output == sys.stdout:
            sys.stderr.write("WARNING: The output stream in the server mode has to be STDOUT. You will recieve translations in STDOUT.")
        '''
        logging.debug("INIT, STARTING THE TRANSLATION SERVER...")
        translation_server(args.models, k=args.k, normalize=args.n, suppress_unk=args.suppress_unk)
    else:
        logging.debug("ELSE_SERVER")
        main(args.models, args.input,
             args.output, k=args.k, normalize=args.n, n_process=args.p,
             chr_level=args.c, verbose=args.v, nbest=args.n_best, suppress_unk=args.suppress_unk,
             print_word_probabilities=args.print_word_probabilities, save_alignment=args.output_alignment,
             a_json=args.json_alignment)
