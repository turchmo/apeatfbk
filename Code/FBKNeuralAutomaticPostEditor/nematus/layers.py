'''
Layer definitions
'''

import json
import cPickle as pkl
import numpy
from collections import OrderedDict

import sys

import theano
import theano.tensor as tensor
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

from initializers import *
from util import *
from theano_util import *
from alignment_util import *

# layers: 'name': ('parameter initializer', 'feedforward')
layers = {'ff': ('param_init_fflayer', 'fflayer'),
          'gru': ('param_init_gru', 'gru_layer'),
          'gru_cond': ('param_init_gru_cond', 'gru_cond_layer'),
          'ff_init_merger': ('param_init_ff_initial_state_merger', 'ff_initial_state_merger_layer'),
          'ff_attend_merger': ('param_init_ff_attend_merger','ff_attend_merger_layer')
          }


def get_layer_param(name):
    param_fn, constr_fn = layers[name]
    return eval(param_fn)

def get_layer_constr(name):
    param_fn, constr_fn = layers[name]
    return eval(constr_fn)

# dropout that will be re-used at different time steps
def shared_dropout_layer(shape, use_noise, trng, value, scaled=True):
    #re-scale dropout at training time, so we don't need to at test time
    if scaled:
        proj = tensor.switch(
            use_noise,
            trng.binomial(shape, p=value, n=1,
                                        dtype='float32')/value,
            theano.shared(numpy.float32(1.)))
    else:
        proj = tensor.switch(
            use_noise,
            trng.binomial(shape, p=value, n=1,
                                        dtype='float32'),
            theano.shared(numpy.float32(value)))
    return proj

# feedforward layer: affine transformation + point-wise nonlinearity
def param_init_fflayer(options, params, prefix=None, nin=None, nout=None,
                       ortho=True):
    if nin is None:
        nin = options['dim_proj']
    if nout is None:
        nout = options['dim_proj']
    params[pp(prefix, 'W')] = norm_weight(nin, nout, scale=0.01, ortho=ortho)
    params[pp(prefix, 'b')] = numpy.zeros((nout,)).astype('float32')

    return params


def fflayer(tparams, state_below, options, prefix=None,
            activ='lambda x: tensor.tanh(x)', **kwargs):
    return eval(activ)(
        tensor.dot(state_below, tparams[pp(prefix, 'W')]) +
        tparams[pp(prefix, 'b')])


# GRU layer
def param_init_gru(options, params, prefix='gru', nin=None, dim=None):
    if nin is None:
        nin = options['dim_proj']
    if dim is None:
        dim = options['dim_proj']

    # embedding to gates transformation weights, biases
    W = numpy.concatenate([norm_weight(nin, dim),
                           norm_weight(nin, dim)], axis=1)
    params[pp(prefix, 'W')] = W
    params[pp(prefix, 'b')] = numpy.zeros((2 * dim,)).astype('float32')

    # recurrent transformation weights for gates
    U = numpy.concatenate([ortho_weight(dim),
                           ortho_weight(dim)], axis=1)
    params[pp(prefix, 'U')] = U

    # embedding to hidden state proposal weights, biases
    Wx = norm_weight(nin, dim)
    params[pp(prefix, 'Wx')] = Wx
    params[pp(prefix, 'bx')] = numpy.zeros((dim,)).astype('float32')

    # recurrent transformation weights for hidden state proposal
    Ux = ortho_weight(dim)
    params[pp(prefix, 'Ux')] = Ux

    return params


def gru_layer(tparams, state_below, options, prefix='gru', mask=None,
              emb_dropout=None,
              rec_dropout=None,
              profile=False,
              **kwargs):
    nsteps = state_below.shape[0]
    if state_below.ndim == 3:
        n_samples = state_below.shape[1]
    else:
        n_samples = 1

    dim = tparams[pp(prefix, 'Ux')].shape[1]

    if mask is None:
        mask = tensor.alloc(1., state_below.shape[0], 1)

    # utility function to slice a tensor
    def _slice(_x, n, dim):
        if _x.ndim == 3:
            return _x[:, :, n*dim:(n+1)*dim]
        return _x[:, n*dim:(n+1)*dim]

    # state_below is the input word embeddings
    # input to the gates, concatenated
    state_below_ = tensor.dot(state_below*emb_dropout[0], tparams[pp(prefix, 'W')]) + \
        tparams[pp(prefix, 'b')]
    # input to compute the hidden state proposal
    state_belowx = tensor.dot(state_below*emb_dropout[1], tparams[pp(prefix, 'Wx')]) + \
        tparams[pp(prefix, 'bx')]

    # step function to be used by scan
    # arguments    | sequences |outputs-info| non-seqs
    def _step_slice(m_, x_, xx_, h_, U, Ux, rec_dropout):

        preact = tensor.dot(h_*rec_dropout[0], U)
        preact += x_

        # reset and update gates
        r = tensor.nnet.sigmoid(_slice(preact, 0, dim))
        u = tensor.nnet.sigmoid(_slice(preact, 1, dim))

        # compute the hidden state proposal
        preactx = tensor.dot(h_*rec_dropout[1], Ux)
        preactx = preactx * r
        preactx = preactx + xx_

        # hidden state proposal
        h = tensor.tanh(preactx)

        # leaky integrate and obtain next hidden state
        h = u * h_ + (1. - u) * h
        h = m_[:, None] * h + (1. - m_)[:, None] * h_

        return h

    # prepare scan arguments
    seqs = [mask, state_below_, state_belowx]
    init_states = [tensor.alloc(0., n_samples, dim)]
    _step = _step_slice
    shared_vars = [tparams[pp(prefix, 'U')],
                   tparams[pp(prefix, 'Ux')],
                   rec_dropout]

    rval, updates = theano.scan(_step,
                                sequences=seqs,
                                outputs_info=init_states,
                                non_sequences=shared_vars,
                                name=pp(prefix, '_layers'),
                                n_steps=nsteps,
                                profile=profile,
                                strict=True)
    rval = [rval]
    return rval


# Conditional GRU layer with Attention
def param_init_gru_cond(options, params, prefix='gru_cond',
                        nin=None, dim=None, dimctx=None,
                        nin_nonlin=None, dim_nonlin=None, attend_merge_op=None):
    if nin is None:
        nin = options['dim']
    if dim is None:
        dim = options['dim']
    if dimctx is None:
        dimctx = options['dim']
    if nin_nonlin is None:
        nin_nonlin = nin
    if dim_nonlin is None:
        dim_nonlin = dim
        
    num_encoders = len(options['datasets']) -1
    print >> sys.stderr,"Num. Encoders Param Init GRU Cond Layer:",num_encoders

    W = numpy.concatenate([norm_weight(nin, dim),
                           norm_weight(nin, dim)], axis=1)
    params[pp(prefix, 'W')] = W
    params[pp(prefix, 'b')] = numpy.zeros((2 * dim,)).astype('float32')
    U = numpy.concatenate([ortho_weight(dim_nonlin),
                           ortho_weight(dim_nonlin)], axis=1)
    params[pp(prefix, 'U')] = U

    Wx = norm_weight(nin_nonlin, dim_nonlin)
    params[pp(prefix, 'Wx')] = Wx
    Ux = ortho_weight(dim_nonlin)
    params[pp(prefix, 'Ux')] = Ux
    params[pp(prefix, 'bx')] = numpy.zeros((dim_nonlin,)).astype('float32')

    U_nl = numpy.concatenate([ortho_weight(dim_nonlin),
                              ortho_weight(dim_nonlin)], axis=1)
    params[pp(prefix, 'U_nl')] = U_nl
    params[pp(prefix, 'b_nl')] = numpy.zeros((2 * dim_nonlin,)).astype('float32')

    Ux_nl = ortho_weight(dim_nonlin)
    params[pp(prefix, 'Ux_nl')] = Ux_nl
    params[pp(prefix, 'bx_nl')] = numpy.zeros((dim_nonlin,)).astype('float32')

    # context to LSTM
    Wc = norm_weight(dimctx, dim*2)
    params[pp(prefix, 'Wc')] = Wc

    Wcx = norm_weight(dimctx, dim)
    params[pp(prefix, 'Wcx')] = Wcx

    # attention: combined -> hidden
    W_comb_att = norm_weight(dim, dimctx)
    params[pp(prefix, 'W_comb_att')] = W_comb_att

    # attention: context -> hidden
    Wc_att = norm_weight(dimctx)
    params[pp(prefix, 'Wc_att')] = Wc_att

    # attention: hidden bias
    b_att = numpy.zeros((dimctx,)).astype('float32')
    params[pp(prefix, 'b_att')] = b_att

    # attention:
    U_att = norm_weight(dimctx, 1)
    params[pp(prefix, 'U_att')] = U_att
    c_att = numpy.zeros((1,)).astype('float32')
    params[pp(prefix, 'c_tt')] = c_att

    if num_encoders == 2:
        U_att1 = norm_weight(dimctx, 1)
        params[pp(prefix, 'U_att1')] = U_att1
        c_att1 = numpy.zeros((1,)).astype('float32')
        params[pp(prefix, 'c_tt1')] = c_att1

        params = get_layer_param('ff_attend_merger')(
            options, params, dimctx, prefix=pp(prefix, 'ff_attend_merger'),
            attend_merge_op=attend_merge_op)

    return params


def gru_cond_layer(tparams, state_below, options, prefix='gru',
                   mask=None, context=None, one_step=False,
                   init_memory=None, init_state=None,
                   context_mask=None, emb_dropout=None,
                   rec_dropout=None, ctx_dropout=None,
                   profile=False,attend_merge_op=None,
                   **kwargs):

    assert context, 'Context must be provided'

    num_encoders = len(context)
    
    print >> sys.stderr,"Num. Encoders GRU Cond Layer:",num_encoders

    if one_step:
        assert init_state, 'previous state must be provided'

    nsteps = state_below.shape[0]
    if state_below.ndim == 3:
        n_samples = state_below.shape[1]
    else:
        n_samples = 1

    # mask
    if mask is None:
        mask = tensor.alloc(1., state_below.shape[0], 1)

    dim = tparams[pp(prefix, 'Wcx')].shape[1]

    # initial/previous state
    if init_state is None:
        init_state = tensor.alloc(0., n_samples, dim)

    # projected context
    #assert context.ndim == 3, 'Context must be 3-d: #annotation x #sample x dim'

    #pctx_ = tensor.dot(context*ctx_dropout[0], tparams[pp(prefix, 'Wc_att')]) + tparams[pp(prefix, 'b_att')]

    enc_lspaces = [eval('linear')((context[eid]*ctx_dropout[0]).dot(tparams[pp(prefix, 'Wc_att')]) + tparams[pp(prefix, 'b_att')]) for eid in context.keys()]

    def _slice(_x, n, dim):
        if _x.ndim == 3:
            return _x[:, :, n*dim:(n+1)*dim]
        return _x[:, n*dim:(n+1)*dim]

    # projected x
    state_belowx = tensor.dot(state_below*emb_dropout[0], tparams[pp(prefix, 'Wx')]) +\
        tparams[pp(prefix, 'bx')]
    state_below_ = tensor.dot(state_below*emb_dropout[1], tparams[pp(prefix, 'W')]) +\
        tparams[pp(prefix, 'b')]

    def _step_slice(m_, x_, xx_, h_,
                    U, Wc, W_comb_att, Ux, Wcx,
                    U_nl, Ux_nl, b_nl, bx_nl, rec_dropout, ctx_dropout, *args):

        preact1 = tensor.dot(h_*rec_dropout[0], U)
        preact1 += x_
        preact1 = tensor.nnet.sigmoid(preact1)

        r1 = _slice(preact1, 0, dim)
        u1 = _slice(preact1, 1, dim)

        preactx1 = tensor.dot(h_*rec_dropout[1], Ux)
        preactx1 *= r1
        preactx1 += xx_

        h1 = tensor.tanh(preactx1)

        h1 = u1 * h_ + (1. - u1) * h1
        h1 = m_[:, None] * h1 + (1. - m_)[:, None] * h_

        # attention
        pstate_ = tensor.dot(h1*rec_dropout[2], W_comb_att)

        alphas = []
        weighted_averages = []
        num_encs = len(context)

        for i in range(num_encs):

            # combine encoder and decoder latent spaces and compute alignments
            pctx_ = args[i]
            cc_ = args[i + num_encs]

            pctx__ = pctx_ + pstate_[None, :, :]
            pctx__ = tensor.tanh(pctx__)
            alpha = tensor.dot(pctx__ * ctx_dropout[1], args[num_encs+num_encs+i]) + args[num_encs+num_encs+num_encs+i]
            alpha = alpha.reshape([alpha.shape[0], alpha.shape[1]])
            alpha = tensor.exp(alpha - alpha.max(0, keepdims=True))
            if context_mask is not None:
                alpha = alpha * context_mask[i]
            alpha = alpha / alpha.sum(0, keepdims=True)

            ctx_ = (cc_ * alpha[:, :, None]).sum(0)  # current context

            # project current context using a shared post projection
            #ctx_ = eval(lctxproj_act)(ctx_.dot(Wp_att)) # later try this option too
            weighted_averages.append(ctx_)
            alphas.append(alpha.T)


        if num_encoders == 2:
            # our guy, the merger layer
            ctx_, merger_alpha = get_layer_constr('ff_attend_merger')(
                tparams, weighted_averages, prefix=pp(prefix, 'ff_attend_merger'), attend_merge_act='linear',
                attend_merge_op=attend_merge_op, **kwargs)
        elif num_encoders ==1:
            ctx_ =  weighted_averages[0]
            # merger_alpha = None #(if you get error here then set it to "None")
        else:
            print >>sys.stderr, 'Number of sources is too large! System stopped!'
            sys.exit()



        preact2 = tensor.dot(h1*rec_dropout[3], U_nl)+b_nl
        preact2 += tensor.dot(ctx_*ctx_dropout[2], Wc)
        preact2 = tensor.nnet.sigmoid(preact2)

        r2 = _slice(preact2, 0, dim)
        u2 = _slice(preact2, 1, dim)

        preactx2 = tensor.dot(h1*rec_dropout[4], Ux_nl)+bx_nl
        preactx2 *= r2
        preactx2 += tensor.dot(ctx_*ctx_dropout[3], Wcx)

        h2 = tensor.tanh(preactx2)

        h2 = u2 * h1 + (1. - u2) * h2
        h2 = m_[:, None] * h2 + (1. - m_)[:, None] * h1

        return (h2, ctx_) + tuple(alphas)  # pstate_, preact, preactx, r, u

    seqs = [mask, state_below_, state_belowx]
    #seqs = [mask, state_below_, state_belowx, state_belowc]
    _step = _step_slice

    shared_vars = [tparams[pp(prefix, 'U')],
                   tparams[pp(prefix, 'Wc')],
                   tparams[pp(prefix, 'W_comb_att')],
                   tparams[pp(prefix, 'Ux')],
                   tparams[pp(prefix, 'Wcx')],
                   tparams[pp(prefix, 'U_nl')],
                   tparams[pp(prefix, 'Ux_nl')],
                   tparams[pp(prefix, 'b_nl')],
                   tparams[pp(prefix, 'bx_nl')]]

    outputs_info = [init_state, None]
    outputs_info += [None for _ in context.values()]

    non_seqs = (shared_vars + [rec_dropout] + [ctx_dropout] + enc_lspaces + context.values())
    
    if num_encoders ==2: 
        non_seqs += [tparams[pp(prefix, 'U_att')],tparams[pp(prefix, 'U_att1')],tparams[pp(prefix, 'c_tt')],tparams[pp(prefix, 'c_tt1')]]
    elif num_encoders ==1:
        non_seqs += [tparams[pp(prefix, 'U_att')],tparams[pp(prefix, 'c_tt')]]
    else:
         print >>sys.stderr, 'Number of sources is too large! System stopped!'
         sys.exit()
    
    if attend_merge_op == 'att':
        non_seqs += [tparams[pp(prefix, 'ff_attend_merger_U')],
                     tparams[pp(prefix, 'ff_attend_merger_W')],
                     tparams[pp(prefix, 'ff_attend_merger_b')]]
    #elif (attend_merge_op == 'mean-concat'):    
    elif (attend_merge_op == 'mean-concat') & (num_encoders ==2):
        non_seqs += [tparams[pp(prefix, 'ff_attend_merger_W')],
                     tparams[pp(prefix, 'ff_attend_merger_b')]]


    if one_step:
        rval = _step(*(seqs + [init_state] + non_seqs))
    else:
        rval, updates = theano.scan(_step,
                                    sequences=seqs,
                                    outputs_info=outputs_info,
                                    non_sequences=non_seqs,
                                    name=pp(prefix, '_layers'),
                                    n_steps=nsteps,
                                    profile=profile,
                                    strict=True)
    return rval

def param_init_ff_initial_state_merger(
        params, prefix='ff_init_state_merger', nin=None, nout=None, ortho=True):
    params[pp(prefix, 'W')] = norm_weight(nin, nout, scale=0.01, ortho=ortho)
    params[pp(prefix, 'b')] = numpy.zeros((nout,)).astype('float32')

    return params


def ff_initial_state_merger_layer(
        tparams, states_below, prefix=None,
        init_merge_act='lambda x: x', add_bias=True, init_merge_op='mean',
        bundle_states=None, **kwargs):
    if init_merge_op == 'mean':
        preact = states_below[0]
        for i in range(1, len(states_below)):
            preact += states_below[i]
        preact /= tensor.cast(len(states_below), theano.config.floatX)
    elif init_merge_op == 'sum':
        preact = states_below[0]
        for i in range(1, len(states_below)):
            preact += states_below[i]
    elif init_merge_op == 'max':
        preact = states_below[0]
        for i in range(1, len(states_below)):
            preact = tensor.maximum(preact, states_below[i])
    elif init_merge_op == 'mean-concat':
        # concatenate each bundle
        concatenated = concatenate(states_below.values(), axis=states_below[0].ndim - 1)

        # project
        preact = get_layer_constr('ff')(tparams, concatenated, None, prefix=prefix, activ='linear')
    else:
        raise ValueError('Unrecognized merge op!')
    return eval(init_merge_act)(preact)


def param_init_ff_attend_merger(options, params, dimctx=None, prefix=None, attend_merge_op=None):
    if attend_merge_op == 'gru':
        pass
    elif attend_merge_op == 'att':
        params[pp(prefix, 'U')] = norm_weight(dimctx, 1)
        params[pp(prefix, 'W')] = norm_weight(dimctx, dimctx)
        params[pp(prefix, 'b')] = numpy.zeros((dimctx,)).astype('float32')
    elif attend_merge_op == 'mean-concat':
        numSrc = len(options['datasets']) - 1
        params[pp(prefix, 'W')] = norm_weight(dimctx * numSrc, dimctx)
        params[pp(prefix, 'b')] = numpy.zeros((dimctx,)).astype('float32')
    return params


def ff_attend_merger_layer(tparams, states_below, prefix=None,
                           attend_merge_act=None, attend_merge_op=None):
    alpha = None
    if attend_merge_op == 'mean':
        preact = states_below[0]
        for i in range(1, len(states_below)):
            preact += states_below[i]
        preact /= tensor.cast(len(states_below), theano.config.floatX)
    elif attend_merge_op == 'sum':
        preact = states_below[0]
        for i in range(1, len(states_below)):
            preact += states_below[i]
    elif attend_merge_op == 'att':
        tbd = tensor.stack(states_below)  # time x batch x dim
        ptbd = tensor.tanh(
            tensor.dot(tbd, tparams[pp(prefix, 'W')]) +
            tparams[pp(prefix, 'b')])
        alpha = tensor.dot(ptbd, tparams[pp(prefix, 'U')])
        alpha = alpha.reshape([alpha.shape[0], alpha.shape[1]])
        alpha = tensor.exp(alpha)
        alpha = alpha / alpha.sum(0, keepdims=True)
        preact = (tbd * alpha[:, :, None]).sum(0)  # weighted average
    elif attend_merge_op == 'max':
        preact = states_below[0]
        for i in range(1, len(states_below)):
            preact = tensor.maximum(preact, states_below[i])

    elif attend_merge_op == 'mean-concat':
        if not isinstance(states_below, dict):
            states_below_ = OrderedDict()
            for i in xrange(len(states_below)):
                states_below_[i] = states_below[i]
        else:
            states_below_ = states_below

        # concatenate each bundle
        concatenated = concatenate(states_below_.values(), axis=states_below[0].ndim - 1)

        # project
        preact = get_layer_constr('ff')(
            tparams, concatenated, None, prefix=prefix, activ='linear')

    else:
        raise ValueError('Unrecognized merge op!')
    return eval(attend_merge_act)(preact), alpha
