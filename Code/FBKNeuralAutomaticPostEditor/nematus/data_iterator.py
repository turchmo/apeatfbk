import numpy

import gzip
import sys

import shuffle
from util import load_dict

epoch_num = 0

def fopen(filename, mode='r'):
    if filename.endswith('.gz'):
        return gzip.open(filename, mode)
    return open(filename, mode)

class TextIterator:
    """Simple Bitext iterator."""
    def __init__(self, source, target,
                 source_dicts, target_dict,
                 batch_size=128,
                 maxlen=100,
                 n_words_source=-1,
                 n_words_target=-1,
                 shuffle_each_epoch=False,
                 sort_by_length=True,
                 maxibatch_size=20):
        global epoch_num
        if shuffle_each_epoch:
            shuffle.main(source + [target], epoch_num)
            self.source = [fopen(s + '.shuf', 'r') for s in source]
            self.target = fopen(target+'.shuf', 'r')
        else:
            self.source = [fopen(s, 'r') for s in source]
            self.target = fopen(target, 'r')
        self.source_dicts = []
        for source_dict in source_dicts:
            self.source_dicts.append(load_dict(source_dict))
        self.target_dict = load_dict(target_dict)

        self.batch_size = batch_size
        self.maxlen = maxlen

        self.n_words_source = n_words_source
        self.n_words_target = n_words_target

        if self.n_words_source > 0:
            for d in self.source_dicts:
                for key, idx in d.items():
                    if idx >= self.n_words_source:
                        del d[key]

        if self.n_words_target > 0:
                for key, idx in self.target_dict.items():
                    if idx >= self.n_words_target:
                        del self.target_dict[key]

        self.shuffle = shuffle_each_epoch
        self.sort_by_length = sort_by_length

        self.source_buffer = [[] for _ in self.source]
        self.target_buffer = []
        self.k = batch_size * maxibatch_size
        

        self.end_of_data = False

    def __iter__(self):
        return self

    def reset(self):
        global epoch_num
        epoch_num += 1
        if self.shuffle:
            shuffle.main([s.name.replace('.shuf','') for s in self.source] + [self.target.name.replace('.shuf','')], epoch_num)
            self.source = [fopen(s.name, 'r') for s in self.source]
            self.target = fopen(self.target.name, 'r')
        else:
            for s in self.source:
                s.seek(0)
            self.target.seek(0)

    def next(self):
        if self.end_of_data:
            self.end_of_data = False
            self.reset()
            raise StopIteration

        source = [[] for _ in self.source]
        source_words = [[] for _ in self.source]
        target = []
        target_words = []

        # fill buffer, if it's empty
        for i in xrange(len(self.source_buffer)):
            assert len(self.source_buffer[i]) == len(self.target_buffer), 'Buffer size mismatch!'

        if len(self.target_buffer) == 0:
            for k_ in xrange(self.k):
                source_brk = False
                target_brk = False

                tt = self.target.readline()

                if tt == "":
                    target_brk = True


                source_data = []
                for ii, s in enumerate(self.source):
                    ss = s.readline()
                    if ss == "":
                        source_brk = True
                        continue
                    source_data.append(ss)

                if source_brk or target_brk:
                    break
                else:
                    self.target_buffer.append(tt.strip().split())
                    for ii in xrange(len(source_data)):
                        self.source_buffer[ii].append(source_data[ii].strip().split())

            # sort by target buffer
            if self.sort_by_length:
                tlen = numpy.array([len(t) for t in self.target_buffer])
                tidx = tlen.argsort()

                _sbuf = [[] for _ in self.source]
                for ii, s in enumerate(self.source):
                    _sbuf[ii] = [self.source_buffer[ii][i] for i in tidx]

                #_sbuf = [self.source_buffer[i] for i in tidx]
                _tbuf = [self.target_buffer[i] for i in tidx]

                self.source_buffer = _sbuf
                self.target_buffer = _tbuf

            else:
                for s in self.source_buffer:
                    s.reverse()
                self.target_buffer.reverse()

            for i in xrange(len(self.source_buffer)):
                assert len(self.source_buffer[i]) == len(self.target_buffer), 'Buffer size mismatch!'
        
        #print >>sys.stderr, 'Number of Encoders in data iterator: ',len(self.source_buffer)
        
        if len(self.source_buffer) == 2: 
            if len(self.source_buffer[0]) == 0 or len(self.source_buffer[1]) == 0 or len(self.target_buffer) == 0:
                self.end_of_data = False
                self.reset()
                raise StopIteration
        elif len(self.source_buffer) == 1:
            if len(self.source_buffer[0]) == 0  or len(self.target_buffer) == 0:
                self.end_of_data = False
                self.reset()
                raise StopIteration
        else:   
            print >>sys.stderr, 'Number of sources is too large! System stopped!'
            sys.exit()
             
        try:

            # actual work here
            while True:

                # read from file and map to word index
                try:
                    tt = self.target_buffer.pop()
                except IndexError:
                    break

                source_ = []
                source_words_ = []
                for ii, s in enumerate(self.source_buffer):
                    ss = s.pop()
                    tmp_id = []
                    tmp_words = []
                    for w in ss:
                        w_words = [f for (i, f) in enumerate(w.split('|'))]
                        tmp_words.append(w_words)

                        w = [self.source_dicts[ii][f] if f in self.source_dicts[ii] else 1 for (i, f) in
                             enumerate(w.split('|'))]
                        tmp_id.append(w)
                    #ss = tmp_id
                    source_.append(tmp_id)
                    source_words_.append(tmp_words)

                # read from file and map to word index
                tt_words = [w for w in tt]

                tt = [self.target_dict[w] if w in self.target_dict else 1
                      for w in tt]

                if self.n_words_target > 0:
                    tt = [w if w < self.n_words_target else 1 for w in tt]
                
                if len(self.source_buffer) == 2: 
                    if len(source_[0]) > self.maxlen or len(source_[1]) > self.maxlen or len(tt) > self.maxlen:
                        continue
                    if not source_[0] or not source_[1] or not tt:
                        continue
                elif len(self.source_buffer) == 1: 
                    if len(source_[0]) > self.maxlen or len(tt) > self.maxlen:
                        continue
                    if not source_[0]  or not tt:
                        continue
                else:   
                    print >>sys.stderr, 'Number of sources is too large! System stopped!'
                    sys.exit()   
                    
                for ii, ss in enumerate(source_):
                    source[ii].append(ss)

                for ii, ss in enumerate(source_words_):
                    source_words[ii].append(ss)

                target.append(tt)
                target_words.append(tt_words)
                
                if len(self.source_buffer) == 2: 
                    #multi-source
                    if len(source[0]) >= self.batch_size or len(source[1]) >= self.batch_size or len(target) >= self.batch_size:
                        break
                elif len(self.source_buffer) == 1:
                    #single source 
                    if len(source[0]) >= self.batch_size  or len(target) >= self.batch_size:
                        break
                else:   
                    print >>sys.stderr, 'Number of sources is too large! System stopped!'
                    sys.exit()     
                    
        except IOError:
            self.end_of_data = True

        # all sentence pairs in maxibatch filtered out because of length
        if len(source[0]) == 0 or len(target) == 0:
            #print >>sys.stderr,source[0]
            #print >>sys.stderr,len(source[0])
            #print >>sys.stderr,source[1]
            #print >>sys.stderr,len(source[1])
            #print >>sys.stderr,target
            #print >>sys.stderr,len(target)
            #sys.stderr.flush()
            

            source, target, source_words, target_words = self.next()

#         print>> sys.stderr, "source size:", len(source)
#         print>> sys.stderr, "target size:", len(target)
#         print >> sys.stderr, len(source_words), len(target_words)
#         sys.stderr.flush()
        return source, target, source_words, target_words
