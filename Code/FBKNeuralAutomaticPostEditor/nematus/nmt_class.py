'''
Build a neural machine translation model with soft attention
'''
import theano
import theano.tensor as tensor
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

import cPickle as pkl
import json
import ipdb
import numpy
import copy
import argparse

import os
import warnings
import sys
import time

import lucene
import distutils
import re


from subprocess import Popen

from collections import OrderedDict
import nltk
from PyLucene import Indexer, Searcher
from itertools import izip
import itertools
from math import floor
from collections import defaultdict

profile = False
numpy.random.seed(1234)

from data_converter import dataConverter
from dictionary_manager import dictionaryManager
from data_iterator import TextIterator
from util import *
from theano_util import *
from alignment_util import *
from apply_bpe import BPE
import pyter

from layers import *
from initializers import *
from optimizers import *

from apply_bpe import BPE



class Nematus(object):
    """Main class of the class-oriented Nematus"""

    def __init__(self, model_json, debug, otherParam):
        """
         Initialization of the class
         Args:
            model_jason: the path to the configuration of the model file: model.npz.json.
            debug: flag to enable/disable the print of the messages on std.errt.
            otherParam: map containing the parameters read from the server configuration file.
        """
        
        
        
        self.debug = debug
        
        
        #load the parameters
        self.model_options = load_config(model_json)
        
        if self.debug:
            print >> sys.stderr, self.model_options
            print >> sys.stderr, "others"
            print >> sys.stderr, otherParam
            sys.stderr.flush()
            
        # update the parameters with the values contained in the server configuration file.
        self.model_options.update(otherParam)
        
        if self.debug:
            print >> sys.stderr, self.model_options
            sys.stderr.flush()

        
        
        self.model_options['model_version'] = '0.11'
        
        time_start = time.time()
    
        if self.debug:
            print >> sys.stderr, self.model_options['use_bpe']
        
       

        # set the dimension of the factors (NOTE: multi-source APE tested with one factor)
        if self.model_options['dim_per_factor'] == None:
            if factors == 1:
                self.model_options['dim_per_factor'] = [self.model_options['dim_word']]
            else:
                sys.stderr.write('Error: if using factored input, you must specify \'dim_per_factor\'\n')
                sys.exit(1)
    
        # set the number of encoders
        self.num_encoder = len(self.model_options['datasets']) - 1
    
    
        self.dictionaryManagers = []
        
        # build dictionary manager
        dictionaries = self.model_options['dictionaries']

        if self.debug:
                print >> sys.stderr, "loading dictionaries..."
                sys.stderr.flush()

        for i in xrange(self.num_encoder):
            if self.debug:
                print >> sys.stderr, "i: " + str(i)
                print >> sys.stderr, dictionaries[i]
                sys.stderr.flush()
            self.dictionaryManagers.append(dictionaryManager(self.model_options['n_words_src'], dictionaries[i], self.model_options, self.debug))
            
        self.dictionaryManagers.append(dictionaryManager(self.model_options['n_words'], dictionaries[self.num_encoder], self.model_options, self.debug))
    
        # if use_bpe == True, load the codes files
        if(self.model_options['use_bpe']):
            if self.debug:
                print >> sys.stderr, "loading bpe codes..."
                sys.stderr.flush()
            self.bpeManager = []
            bpecodes = self.model_options['bpecodes']
            for i in xrange(self.num_encoder+1):
                if self.debug:
                    print >> sys.stderr, "i: " + str(i)
                    print >> sys.stderr, bpecodes[i]
                    sys.stderr.flush()
                self.bpeManager.append(BPE(bpecodes[i]))
    
    
        if self.debug:
            print >> sys.stderr, len(self.dictionaryManagers)
            sys.stderr.flush()
    
        # set the number of source and target words (NOTE: all the sources have the same dimensionality)
        if self.model_options['n_words_src'] is None:
            self.model_options['n_words_src'] = self.dcSource.getNumWords()
        if self.model_options['n_words'] is None:
            self.model_options['n_words'] =  self.dcPE.getNumWords()


    
        if self.debug:
            print >> sys.stderr,'Building model'
            sys.stderr.flush()
        # Initialized the parameters of the model   
        self.params = self.init_params()
    

        self.uidx = 0
        self.history_errs = []
        self.grad_history = None
        
        # reload history
        reload_ = bool(self.model_options['reload_'])
        saveto = self.model_options['saveto']
        
        if reload_ and os.path.exists(saveto):
            if self.debug:
                print >> sys.stderr, 'save to is ', saveto
                sys.stderr.flush()
            self.params = load_params(saveto, self.params)
            if self.debug:
                print >> sys.stderr, 'Reloading the model'
                sys.stderr.flush()
            self.rmodel = numpy.load(saveto)

            self.history_errs = list(self.rmodel['history_errs'])
            if 'ghist' in self.rmodel:
                self.grad_history = self.rmodel['ghist']
                print >> sys.stderr, self.grad_history
                print >> sys.stderr, type(self.grad_history)
                
#                 mappingParam ={'Wemb' : 'Wemb_0',
#                'encoder_U':  'encoder_0_U',
#                 'encoder_Ux': 'encoder_0_Ux',
#                 'encoder_W': 'encoder_0_W',
#                 'encoder_Wx': 'encoder_0_Wx',
#                 'encoder_b': 'encoder_0_b',
#                 'encoder_bx': 'encoder_0_bx',
#                 'encoder_r_U': 'encoder_r_0_U',
#                 'encoder_r_Ux': 'encoder_r_0_Ux',
#                 'encoder_r_W':  'encoder_r_0_W',
#                 'encoder_r_Wx': 'encoder_r_0_Wx',
#                 'encoder_r_b': 'encoder_r_0_b',
#                 'encoder_r_bx': 'encoder_r_0_bx',
#                 }
#                 
#                 
#                 for idxVal in xrange(len(self.grad_history)):
#                     item = self.grad_history[idxVal]
#                     if item in mappingParam:
#                         self.grad_history[idxVal] = mappingParam[item]
#                 
#                 print >> sys.stderr, "After:",self.grad_history
                
            if 'uidx' in self.rmodel:
                self.uidx = self.rmodel['uidx']
                
        # load the tparams        
        print >> sys.stderr,'Load tparams'
        self.tparams = init_theano_params(self.params)
        
        # load the tparams again in a different variable to allow the software to reset the parameters when requested.
        print >> sys.stderr,'Load tparams background'
        self.tparams_background = init_theano_params(self.params)
    
        # build the model
        self.trng, self.use_noise, \
            self.xs, self.x_masks, self.y, self.y_mask, \
            self.opt_ret, \
            self.cost = \
            self.build_model(self.tparams)
    
        self.inps = self.xs[:self.num_encoder] + self.x_masks[:self.num_encoder] + [self.y, self.y_mask]
    
   
        
    
        if self.debug:
            print >> sys.stderr, 'Building sampler'
            sys.stderr.flush()
        #build the sampler
        self.f_init, self.f_next = self.build_sampler(self.tparams,  self.use_noise, self.trng)
    
    
    
        self.cost = self.cost.mean()
        
        self.decay_c= float(self.model_options['decay_c'])
    
        # apply L2 regularization on weights
        if self.decay_c > 0.:
            self.decay_c = theano.shared(numpy.float32(self.decay_c), name='decay_c')
            self.weight_decay = 0.
            for kk, vv in self.tparams.iteritems():
                self.weight_decay += (vv ** 2).sum()
            self.weight_decay *= self.decay_c
            self.cost += self.weight_decay
    
        # regularize the alpha weights
        self.alpha_c= float(self.model_options['alpha_c'])
        if self.alpha_c > 0. and not self.model_options['decoder'].endswith('simple'):
            self.alpha_c = theano.shared(numpy.float32(self.alpha_c), name='alpha_c')
            self.alpha_reg = self.alpha_c * (
                (tensor.cast(self.y_mask.sum(0)//self.x_mask.sum(0), 'float32')[:, None] -
                 self.opt_ret['dec_alphas'].sum(0))**2).sum(1).mean()
            self.cost += self.alpha_reg
    
        self.map_decay_c= float(self.model_options['map_decay_c'])
        
        # apply L2 regularisation to loaded model (map training)
        if self.map_decay_c > 0:
            self.map_decay_c = theano.shared(numpy.float32(self.map_decay_c), name="map_decay_c")
            self.weight_map_decay = 0.
            for kk, vv in self.tparams.iteritems():
                init_value = theano.shared(vv.get_value(), name= kk + "_init")
                self.weight_map_decay += ((vv -init_value) ** 2).sum()
            self.weight_map_decay *= self.map_decay_c
            self.cost += self.weight_map_decay
    
    
        # allow finetuning with fixed embeddings
        finetune= bool(self.model_options['finetune'])
        if finetune:
            self.updated_params = OrderedDict([(key,value) for (key,value) in self.tparams.iteritems() if not key.startswith('Wemb')])
        else:
            self.updated_params = self.tparams
    
        # allow finetuning of only last layer (becomes a linear model training problem)
        finetune_only_last = bool(self.model_options['finetune_only_last'])
        if finetune_only_last:
            self.updated_params = OrderedDict([(key,value) for (key,value) in self.tparams.iteritems() if key in ['ff_logit_W', 'ff_logit_b']])
        else:
            self.updated_params = self.tparams
    
        if self.debug:
            print >> sys.stderr, 'Computing gradient...',
            sys.stderr.flush()
        self.grads = tensor.grad(self.cost, wrt=itemlist(self.updated_params))
        if self.debug:
            print >> sys.stderr, 'Done'
            sys.stderr.flush()
    
        # apply gradient clipping here
        self.clip_c= float(self.model_options['clip_c'])
        if self.clip_c > 0.:
            g2 = 0.
            for g in self.grads:
                g2 += (g**2).sum()
            new_grads = []
            for g in self.grads:
                new_grads.append(tensor.switch(g2 > (self.clip_c**2),
                                               g / tensor.sqrt(g2) * self.clip_c,
                                               g))
            self.grads = new_grads
    
        # compile the optimizer, the actual computational graph is compiled here
        self.lr = tensor.scalar(name='lr')
    
        if self.debug:
            print >> sys.stderr, 'Building optimizers...',
            sys.stderr.flush()
        self.f_grad_shared, self.f_update, self.grad_history = eval(self.model_options['optimizer'])(self.lr, self.updated_params, self.grads, self.inps, self.cost, profile=profile, ghist=self.grad_history)
        if self.debug:
            print >> sys.stderr, 'Done'
            sys.stderr.flush()
        if self.debug:
            print >> sys.stderr, 'Optimization'
            sys.stderr.flush()
        lucene.initVM(vmargs=['-Djava.awt.headless=true'], maxheap='2500m')
        self.vm_env = lucene.getVMEnv()
        
        
        use_instance_selection = bool(self.model_options['use_instance_selection'])
        # create the index and the searcher. Fill the indexer with the training data if empty
        if use_instance_selection:
            # General variables for instance selection
            
            if self.debug:
                print >> sys.stderr, 'lucene', lucene.VERSION
                sys.stderr.flush()
           
            
            indexer_path = self.model_options['indexer_path']
            self.initVM = False
    
            if not os.path.exists(os.path.join(indexer_path)):
                # there is not an already filled index in indexer_path, so it creates and fills it.
                
                print >> sys.stderr, "WARNING: I COULD NOT FIND THE INDEX DIR. SO, I AM GOING TO INDEX THE BG CORPUS..."
                if self.debug:
                    print >> sys.stderr, "Start creating Index"
                    sys.stderr.flush()
                sys.stderr.flush()
                self.indexer = Indexer(os.path.join(indexer_path), initVM=self.initVM)
    
                if self.debug:
                    print >> sys.stderr, "Finish creating Index"
                    sys.stderr.flush()
    
                # Feed PyLucene with the generic data
                if self.debug:
                    print >> sys.stderr, "start feeding PyLucene"
                    sys.stderr.flush()
    
    
    
                if self.debug:
                    print >> sys.stderr, "Initializing train iterator"
                    sys.stderr.flush()    
                    
                train = TextIterator(self.model_options['datasets'][:-1], self.model_options['datasets'][-1],
                                     dictionaries[:-1], dictionaries[-1],
                                     n_words_source=self.model_options['n_words_src'], n_words_target=self.model_options['n_words'],
                                     batch_size=1,
                                     maxlen=self.model_options['maxlen'],
                                     shuffle_each_epoch=self.model_options['shuffle_each_epoch'],
                                     sort_by_length=self.model_options['sort_by_length'],
                                     maxibatch_size=1
                                     )
    
                id = 0
                start_lucene = time.time()
                for x, y, x_words, y_words in train: # WARNING: Batch should contain only one sentence, Not tested on factored input
                    # flat the sentences x, y, x_words and y_words
                    x_doc = [' '.join(map(str, numpy.array(x_).flatten())) for x_ in x]
                    y_doc = ' '.join(map(str, numpy.array(y).flatten()))
                    
                    if(self.model_options['query_repres']  != 'id'):
                        # the index stores both the training sentences containing the bpe and the sentences without bpe.
                        bpe = [' '.join(numpy.array(x_words_).flatten()) for x_words_ in x_words]
                        x_doc_words = [bpe_.replace('@@ ', '') for bpe_ in bpe]
                        bpe = ' '.join(numpy.array(y_words).flatten())
                        y_doc_words = bpe.replace('@@ ', '')
                        
                        self.indexer.add(id, x_doc, y_doc, x_doc_words, y_doc_words)
                    else:  
                        # the index stores the training sentences e.

                        self.indexer.add(id, x_doc, y_doc, "", "") 
  
                    
    
                    id += 1
                    

                    
                    if (id % 100000) == 0:
                        if self.debug:
                            print >> sys.stderr, "c: " + str(id)
                            print >> sys.stderr, x_doc, y_doc
                            sys.stderr.flush()
                        self.indexer.commit()
                        
                self.indexer.commit()
                self.indexer.close()
                if self.debug:
                    print >> sys.stderr, "finish feeding PyLucene adding: " + str(id)
                    print >> sys.stderr, "Lucene time: " + str(time.time() - start_lucene)
                    sys.stderr.flush()
            else:
                
                #self.indexer.close()
                print >> sys.stderr, "WARNING: I FOUND THE LUCENE INDEXER DIRECTORY...!!!"
                print >> sys.stderr, "SO, I AM GOING TO RE-USE IT. IF YOU NEED TO RE-INDEX YOUR CORPUS, REMOVE THE INDEX DIR AND RE-RUN NEMATUS."
    
            # Initialise the Search
            #print self.initVM
            self.searcher = Searcher(os.path.join(indexer_path), initVM=self.initVM)
            
        
            
    
        self.maxlen_test = float("inf")
        self.print_alignment = False
        self.epoch_list = [int(epc) for epc in self.model_options['epoch_list']]
        self.lrate_list = [float(lr) for lr in self.model_options['lrate_list']]
        self.num_epoch_bin = len(self.epoch_list)
        self.num_lrate_bin = len(self.lrate_list)
        # Translation output file name
        self.of = open(self.model_options['translation_fname'], 'w')
        
        print >>sys.stderr, 'Finished Initializing Nematus'
        
        sys.stderr.flush()
      
        
     

    # initialize all parameters
    def init_params(self):
        """
         Initialization of the parameters of the model
         
        """
    
        params = OrderedDict()
    
        for i in xrange(self.num_encoder):
            # embedding
            for factor in range(self.model_options['factors']):
                params[embedding_name(factor)+'_'+str(i)] = norm_weight(self.model_options['n_words_src'], self.model_options['dim_per_factor'][factor])
    
            # encoder: bidirectional RNN
            params = get_layer_param(self.model_options['encoder'])(self.model_options, params,
                                                      prefix='encoder'+'_'+str(i),
                                                      nin=self.model_options['dim_word'],
                                                      dim=self.model_options['dim'])
            params = get_layer_param(self.model_options['encoder'])(self.model_options, params,
                                                      prefix='encoder_r'+'_'+str(i),
                                                      nin=self.model_options['dim_word'],
                                                      dim=self.model_options['dim'])
    
        ctxdim = 2 * self.model_options['dim']
        
        if(self.num_encoder == 2):
            params = get_layer_param('ff_init_merger')(params, prefix='ff_init_merger',
                                    nin=ctxdim*self.num_encoder, nout=ctxdim)
    
        params['Wemb_dec'] = norm_weight(self.model_options['n_words'], self.model_options['dim_word'])
    
    
        # init_state, init_cell
        params = get_layer_param('ff')(self.model_options, params, prefix='ff_state',
                                    nin=ctxdim, nout=self.model_options['dim'])
        # decoder
        params = get_layer_param(self.model_options['decoder'])(self.model_options, params,
                                                  prefix='decoder',
                                                  nin=self.model_options['dim_word'],
                                                  dim=self.model_options['dim'],
                                                  dimctx=ctxdim, attend_merge_op='mean-concat')
        # readout
        params = get_layer_param('ff')(self.model_options, params, prefix='ff_logit_lstm',
                                    nin=self.model_options['dim'], nout=self.model_options['dim_word'],
                                    ortho=False)
        params = get_layer_param('ff')(self.model_options, params, prefix='ff_logit_prev',
                                    nin=self.model_options['dim_word'],
                                    nout=self.model_options['dim_word'], ortho=False)
        params = get_layer_param('ff')(self.model_options, params, prefix='ff_logit_ctx',
                                    nin=ctxdim, nout=self.model_options['dim_word'],
                                    ortho=False)
        params = get_layer_param('ff')(self.model_options, params, prefix='ff_logit',
                                    nin=self.model_options['dim_word'],
                                    nout=self.model_options['n_words'])
    
        return params
 
    
    # bidirectional RNN encoder: take input x (optionally with mask), and produce sequence of context vectors (ctx)
    def build_encoder(self, tparams,  trng, use_noise, x=None, x_mask=None, enc_id=None, sampling=False):
        """
        Build the bidirectional RNN encoder
        """
        
        #x = tensor.tensor3('x', dtype='int64')
        #x.tag.test_value = (numpy.random.rand(1, 5, 10) * 100).astype('int64')
    
        # for the backward rnn, we just need to invert x
        xr = x[:, ::-1]
        if x_mask is None:
            xr_mask = None
        else:
            xr_mask = x_mask[::-1]
    
        n_timesteps = x.shape[1]
        n_samples = x.shape[2]
    
        if self.model_options['use_dropout']:
            retain_probability_emb = 1 - self.model_options['dropout_embedding']
            retain_probability_hidden = 1 - self.model_options['dropout_hidden']
            retain_probability_source = 1 - self.model_options['dropout_source']
            if sampling:
                if self.model_options['model_version'] < 0.1:
                    rec_dropout = theano.shared(numpy.array([retain_probability_hidden] * 2, dtype='float32'))
                    rec_dropout_r = theano.shared(numpy.array([retain_probability_hidden] * 2, dtype='float32'))
                    emb_dropout = theano.shared(numpy.array([retain_probability_emb] * 2, dtype='float32'))
                    emb_dropout_r = theano.shared(numpy.array([retain_probability_emb] * 2, dtype='float32'))
                    source_dropout = theano.shared(numpy.float32(retain_probability_source))
                else:
                    rec_dropout = theano.shared(numpy.array([1.] * 2, dtype='float32'))
                    rec_dropout_r = theano.shared(numpy.array([1.] * 2, dtype='float32'))
                    emb_dropout = theano.shared(numpy.array([1.] * 2, dtype='float32'))
                    emb_dropout_r = theano.shared(numpy.array([1.] * 2, dtype='float32'))
                    source_dropout = theano.shared(numpy.float32(1.))
            else:
                if self.model_options['model_version'] < 0.1:
                    scaled = False
                else:
                    scaled = True
                rec_dropout = shared_dropout_layer((2, n_samples, self.model_options['dim']), use_noise, trng,
                                                   retain_probability_hidden, scaled)
                rec_dropout_r = shared_dropout_layer((2, n_samples, self.model_options['dim']), use_noise, trng,
                                                     retain_probability_hidden, scaled)
                emb_dropout = shared_dropout_layer((2, n_samples, self.model_options['dim_word']), use_noise, trng,
                                                   retain_probability_emb, scaled)
                emb_dropout_r = shared_dropout_layer((2, n_samples, self.model_options['dim_word']), use_noise, trng,
                                                     retain_probability_emb, scaled)
                source_dropout = shared_dropout_layer((n_timesteps, n_samples, 1), use_noise, trng,
                                                      retain_probability_source, scaled)
                source_dropout = tensor.tile(source_dropout, (1, 1, self.model_options['dim_word']))
        else:
            rec_dropout = theano.shared(numpy.array([1.] * 2, dtype='float32'))
            rec_dropout_r = theano.shared(numpy.array([1.] * 2, dtype='float32'))
            emb_dropout = theano.shared(numpy.array([1.] * 2, dtype='float32'))
            emb_dropout_r = theano.shared(numpy.array([1.] * 2, dtype='float32'))
    
        # word embedding for forward rnn (source)
        emb = []
        for factor in range(self.model_options['factors']):
            emb.append(tparams[embedding_name(factor)+'_'+str(enc_id)][x[factor].flatten()])
    
        emb = concatenate(emb, axis=1)
        emb = emb.reshape([n_timesteps, n_samples, self.model_options['dim_word']])
    
        if self.model_options['use_dropout']:
            emb *= source_dropout
    
        proj = get_layer_constr(self.model_options['encoder'])(tparams, emb, self.model_options,
                                                    prefix='encoder'+'_'+str(enc_id),
                                                    mask=x_mask,
                                                    emb_dropout=emb_dropout,
                                                    rec_dropout=rec_dropout,
                                                    profile=profile)
    
        # word embedding for backward rnn (source)
        embr = []
        for factor in range(self.model_options['factors']):
            embr.append(tparams[embedding_name(factor)+'_'+str(enc_id)][xr[factor].flatten()])
        embr = concatenate(embr, axis=1)
        embr = embr.reshape([n_timesteps, n_samples, self.model_options['dim_word']])
        if self.model_options['use_dropout']:
            if sampling:
                embr *= source_dropout
            else:
                # we drop out the same words in both directions
                embr *= source_dropout[::-1]
    
        projr = get_layer_constr(self.model_options['encoder'])(tparams, embr, self.model_options,
                                                     prefix='encoder_r'+'_'+str(enc_id),
                                                     mask=xr_mask,
                                                     emb_dropout=emb_dropout_r,
                                                     rec_dropout=rec_dropout_r,
                                                     profile=profile)
    
        # context will be the concatenation of forward and backward rnns
        ctx = concatenate([proj[0], projr[0][::-1]], axis=proj[0].ndim - 1)
    
        return ctx
    
    
    
    def prepare_data(self, seqs_x, seqs_y, maxlen=None, n_words_src=30000,
                 n_words=30000):
        """
        Prepare the batch for single source.
        Args:
            seqs_x: the sequence of source sentences.
            seqs_y: the sequence of target sentences.
            maxlen: the maximum length of the batch.
            
        """
        
        if(self.debug):
            print >> sys.stderr, "Prepare data!"
        
        # x: a list of sentences
        lengths_x = [[len(x__) for x__ in x_] for x_ in seqs_x ]
        lengths_y = [len(s) for s in seqs_y]
     
     
        n_samples = len(seqs_x[0])
        n_factors = len(seqs_x[0][0][0])
        maxlen_x = [numpy.max(len_x) + 1 for len_x in lengths_x]
        maxlen_y = numpy.max(lengths_y) + 1
     
        x = [numpy.zeros((n_factors, maxlen_x_, n_samples)).astype('int64') for maxlen_x_ in maxlen_x]
        y = numpy.zeros((maxlen_y, n_samples)).astype('int64')
     
        x_mask = [numpy.zeros((maxlen_x_, n_samples)).astype('float32') for maxlen_x_ in maxlen_x]
        y_mask = numpy.zeros((maxlen_y, n_samples)).astype('float32')
        

        for idx, seq_ in enumerate(zip(*seqs_x + [seqs_y])):
 
            s_x_ = seq_[:-1]
            s_y = seq_[-1]
            
            if(self.debug):
                print >> sys.stderr, "Prepare s_x and s_y"
     
            for ii, s_x in enumerate(s_x_):
                x[ii][:, :lengths_x[ii][idx], idx] = zip(*s_x)
                x_mask[ii][:lengths_x[ii][idx]+1, idx] = 1.

            y[:lengths_y[idx], idx] = s_y
            y_mask[:lengths_y[idx]+1, idx] = 1.

        return x, x_mask, y, y_mask
     
 
       # batch preparation
    def prepare_data_sources(self, seqs_x, maxlen=None, n_words_src=30000):
        """
        Prepare the batch for multi-source.
        Args:
            seqs_x: the sequence of source sentences.
            seqs_y: the sequence of target sentences.
            maxlen: the maximum length of the batch.
            
        """
        #print seqs_x
        # x: a list of sentences
        lengths_x = [[len(x__) for x__ in x_] for x_ in seqs_x ]

        n_samples = len(seqs_x[0])
        n_factors = len(seqs_x[0][0][0])
        maxlen_x = [numpy.max(len_x) + 1 for len_x in lengths_x]
        
        if self.debug:
            print >> sys.stderr, maxlen_x
       
     
        x = [numpy.zeros((n_factors, maxlen_x_, n_samples)).astype('int64') for maxlen_x_ in maxlen_x]
     
        x_mask = [numpy.zeros((maxlen_x_, n_samples)).astype('float32') for maxlen_x_ in maxlen_x]

     
        for idx, seq_ in enumerate(zip(*seqs_x )):
            s_x_ = seq_[:]
            
     
            for ii, s_x in enumerate(s_x_):
                x[ii][:, :lengths_x[ii][idx], idx] = zip(*s_x)
                x_mask[ii][:lengths_x[ii][idx]+1, idx] = 1.

        
        return x, x_mask
       

    
    # build a training model
    def build_model(self, tparams):
        """
        Building training model
        Args:
            tparams: the parameters of the model.

            
        """
        
        
        opt_ret = dict()

    
        trng = RandomStreams(1234)
        use_noise = theano.shared(numpy.float32(0.))
    
        xs = [tensor.ltensor3('source%d' % i) for i in range(self.num_encoder)]
        for i in xrange(self.num_encoder):
            x_test = (numpy.random.rand(1, 5, 10) * 100).astype('int64')
            xs[i].tag.test_value = x_test
    
        x_masks = [tensor.matrix('source%d_mask' % i) for i in range(self.num_encoder)]
    
        y = tensor.matrix('y', dtype='int64')
        y.tag.test_value = (numpy.random.rand(8, 10) * 100).astype('int64')
        y_mask = tensor.matrix('y_mask', dtype='float32')
        y_mask.tag.test_value = numpy.ones(shape=(8, 10)).astype('float32')
        n_samples = y.shape[1]
        n_timesteps_trg = y.shape[0]
    
        if self.model_options['use_dropout']:
            retain_probability_emb = 1-self.model_options['dropout_embedding']
            retain_probability_hidden = 1-self.model_options['dropout_hidden']
            retain_probability_target = 1-self.model_options['dropout_target']
            if self.model_options['model_version'] < 0.1:
                scaled = False
            else:
                scaled = True
            rec_dropout_d = shared_dropout_layer((5, n_samples, self.model_options['dim']), use_noise, trng, retain_probability_hidden, scaled)
            emb_dropout_d = shared_dropout_layer((2, n_samples, self.model_options['dim_word']), use_noise, trng, retain_probability_emb, scaled)
            ctx_dropout_d = shared_dropout_layer((4, n_samples, 2*self.model_options['dim']), use_noise, trng, retain_probability_hidden, scaled)
            target_dropout = shared_dropout_layer((n_timesteps_trg, n_samples, 1), use_noise, trng, retain_probability_target, scaled)
            target_dropout = tensor.tile(target_dropout, (1,1,self.model_options['dim_word']))
        else:
            rec_dropout_d = theano.shared(numpy.array([1.]*5, dtype='float32'))
            emb_dropout_d = theano.shared(numpy.array([1.]*2, dtype='float32'))
            ctx_dropout_d = theano.shared(numpy.array([1.]*4, dtype='float32'))
    
        for i in range(self.num_encoder):
            xs[i].tag.test_value = (numpy.random.rand(1, 5, 10)*100).astype('int64')
            x_masks[i].tag.test_value = numpy.ones(shape=(5, 10)).astype('float32')
    
        ctxs = OrderedDict()
        for enc_id in xrange(self.num_encoder):
            ctxs[enc_id] = self.build_encoder(tparams,  trng, use_noise, xs[enc_id], x_masks[enc_id], enc_id, sampling=False)
    
        states_below = OrderedDict()
        for enc_id in xrange(self.num_encoder):
            states_below[enc_id] = (ctxs[enc_id] * x_masks[enc_id][:, :, None]).sum(0) / x_masks[enc_id].sum(0)[:, None]
    
    
        if(self.num_encoder ==2 ):
            ctx_mean = get_layer_constr('ff_init_merger')(
                tparams, states_below, prefix='ff_init_merger',
                init_merge_op='mean-concat',
                init_merge_act='linear')
        elif(self.num_encoder == 1 ):   
            ctx_mean = states_below[0]
        else:
            print >>sys.stderr, 'Number of sources is too large! System stopped!'
            sys.exit()

    
        if self.model_options['use_dropout']:
            ctx_mean *= shared_dropout_layer((n_samples, 2*self.model_options['dim']), use_noise, trng, retain_probability_hidden, scaled)
    
        # initial decoder state
        init_state = get_layer_constr('ff')(tparams, ctx_mean, self.model_options,
                                        prefix='ff_state', activ='tanh')
    
    
    
        # word embedding (target), we will shift the target sequence one time step
        # to the right. This is done because of the bi-gram connections in the
        # readout and decoder rnn. The first target will be all zeros and we will
        # not condition on the last output.
        emb = tparams['Wemb_dec'][y.flatten()]
        emb = emb.reshape([n_timesteps_trg, n_samples, self.model_options['dim_word']])
    
        emb_shifted = tensor.zeros_like(emb)
        emb_shifted = tensor.set_subtensor(emb_shifted[1:], emb[:-1])
        emb = emb_shifted
    
        if self.model_options['use_dropout']:
            emb *= target_dropout
    
        # decoder - pass through the decoder conditional gru with attention
        proj = get_layer_constr(self.model_options['decoder'])(tparams, emb, self.model_options,
                                                prefix='decoder',
                                                mask=y_mask, context=ctxs,
                                                context_mask=x_masks,
                                                one_step=False,
                                                init_state=init_state,
                                                emb_dropout=emb_dropout_d,
                                                ctx_dropout=ctx_dropout_d,
                                                rec_dropout=rec_dropout_d,
                                                profile=profile, attend_merge_op='mean-concat')
        # hidden states of the decoder gru
        proj_h = proj[0]
    
        # weighted averages of context, generated by attention module
        ctxs = proj[1]
    
        if self.model_options['use_dropout']:
            proj_h *= shared_dropout_layer((n_samples, self.model_options['dim']), use_noise, trng, retain_probability_hidden, scaled)
            emb *= shared_dropout_layer((n_samples, self.model_options['dim_word']), use_noise, trng, retain_probability_emb, scaled)
            ctxs *= shared_dropout_layer((n_samples, 2*self.model_options['dim']), use_noise, trng, retain_probability_hidden, scaled)
    
        # weights (alignment matrix) #####LIUCAN: this is where the attention vector is.
        opt_ret['dec_alphas'] = proj[2:2+self.num_encoder]
    
        # compute word probabilities
        logit_lstm = get_layer_constr('ff')(tparams, proj_h, self.model_options,
                                        prefix='ff_logit_lstm', activ='linear')
        logit_prev = get_layer_constr('ff')(tparams, emb, self.model_options,
                                        prefix='ff_logit_prev', activ='linear')
        logit_ctx = get_layer_constr('ff')(tparams, ctxs, self.model_options,
                                       prefix='ff_logit_ctx', activ='linear')
        logit = tensor.tanh(logit_lstm+logit_prev+logit_ctx)
    
        if self.model_options['use_dropout']:
            logit *= shared_dropout_layer((n_samples, self.model_options['dim_word']), use_noise, trng, retain_probability_hidden, scaled)
    
        logit = get_layer_constr('ff')(tparams, logit, self.model_options,
                                       prefix='ff_logit', activ='linear')
        logit_shp = logit.shape
        probs = tensor.nnet.softmax(logit.reshape([logit_shp[0]*logit_shp[1],
                                                   logit_shp[2]]))
    
        # cost
        y_flat = y.flatten()
        y_flat_idx = tensor.arange(y_flat.shape[0]) * self.model_options['n_words'] + y_flat
        cost = -tensor.log(probs.flatten()[y_flat_idx])
        cost = cost.reshape([y.shape[0], y.shape[1]])
        cost = (cost * y_mask).sum(0)
        
   
        return trng, use_noise, xs, x_masks, y, y_mask, opt_ret, cost


    # build a sampler
    def build_sampler(self, tparams,  use_noise, trng, return_alignment=False):
        """
        Build a sampler
            
        """
            
        opt_ret = dict()

    
        trng = RandomStreams(1234)
        use_noise = theano.shared(numpy.float32(0.))
    
        xs = [tensor.ltensor3('source%d' % i) for i in range(self.num_encoder)]
        #x_masks = [tensor.matrix('source%d_mask' % i) for i in range(num_encoder)]
    
        ctx = OrderedDict()
        for enc_id in xrange(self.num_encoder):
            ctx[enc_id] = self.build_encoder(tparams,  trng, use_noise, xs[enc_id], None, enc_id, sampling=True)
    
        states_below = OrderedDict()
        for enc_id in xrange(self.num_encoder):
            states_below[enc_id] = ctx[enc_id].mean(0) #(ctxs[enc_id] * x_masks[enc_id][:, :, None]).sum(0) / x_masks[enc_id].sum(0)[:, None]
    
        if(self.num_encoder ==2 ):
            ctx_mean = get_layer_constr('ff_init_merger')(
                tparams, states_below, prefix='ff_init_merger',
                init_merge_op='mean-concat',
                init_merge_act='linear')
        elif(self.num_encoder == 1 ):   
            ctx_mean = states_below[0]
        else:
            print >>sys.stderr, 'Number of sources is too large! System stopped!'
            sys.exit()
    
    
        
    
        if self.model_options['use_dropout'] and self.model_options['model_version'] < 0.1:
            retain_probability_emb = 1-self.model_options['dropout_embedding']
            retain_probability_hidden = 1-self.model_options['dropout_hidden']
            retain_probability_source = 1-self.model_options['dropout_source']
            retain_probability_target = 1-self.model_options['dropout_target']
            rec_dropout_d = theano.shared(numpy.array([retain_probability_hidden]*5, dtype='float32'))
            emb_dropout_d = theano.shared(numpy.array([retain_probability_emb]*2, dtype='float32'))
            ctx_dropout_d = theano.shared(numpy.array([retain_probability_hidden]*4, dtype='float32'))
            target_dropout = theano.shared(numpy.float32(retain_probability_target))
        else:
            rec_dropout_d = theano.shared(numpy.array([1.]*5, dtype='float32'))
            emb_dropout_d = theano.shared(numpy.array([1.]*2, dtype='float32'))
            ctx_dropout_d = theano.shared(numpy.array([1.]*4, dtype='float32'))
    

    
        n_samples = xs[0].shape[2]
    
    
        if self.model_options['use_dropout'] and self.model_options['model_version'] < 0.1:
            ctx_mean *= retain_probability_hidden
    
        init_state = get_layer_constr('ff')(tparams, ctx_mean, self.model_options,
                                        prefix='ff_state', activ='tanh')
    
    
        outs = [init_state] + ctx.values() + xs
    
        print >> sys.stderr, "Building f_init...",
        f_init = theano.function(xs, outs, name='f_init', profile=profile)
        print >>sys.stderr, "Done"
    
        # x: 1 x 1
        y = tensor.vector('y_sampler', dtype='int64')
        init_state = tensor.matrix('init_state', dtype='float32')
    
        # if it's the first word, emb should be all zero and it is indicated by -1
        emb = tensor.switch(y[:, None] < 0,
                            tensor.alloc(0., 1, tparams['Wemb_dec'].shape[1]),
                            tparams['Wemb_dec'][y])
    
        if self.model_options['use_dropout'] and self.model_options['model_version'] < 0.1:
            emb *= target_dropout
    
        # apply one step of conditional gru with attention
        proj = get_layer_constr(self.model_options['decoder'])(tparams, emb, self.model_options,
                                                prefix='decoder',
                                                mask=None, context=ctx,
                                                one_step=True,
                                                init_state=init_state,
                                                emb_dropout=emb_dropout_d,
                                                ctx_dropout=ctx_dropout_d,
                                                rec_dropout=rec_dropout_d,
                                                profile=profile, attend_merge_op='mean-concat')
    
        # get the next hidden state
        next_state = proj[0]
    
        # get the weighted averages of context for this target word y
        ctxs = proj[1]
    
        # alignment matrix (attention model)
        dec_alphas = proj[2:2+self.num_encoder]
    
        if self.model_options['use_dropout'] and self.model_options['model_version'] < 0.1:
            next_state_up = next_state * retain_probability_hidden
            emb *= retain_probability_emb
            ctxs *= retain_probability_hidden
        else:
            next_state_up = next_state
    
        logit_lstm = get_layer_constr('ff')(tparams, next_state_up, self.model_options,
                                        prefix='ff_logit_lstm', activ='linear')
        logit_prev = get_layer_constr('ff')(tparams, emb, self.model_options,
                                        prefix='ff_logit_prev', activ='linear')
        logit_ctx = get_layer_constr('ff')(tparams, ctxs, self.model_options,
                                       prefix='ff_logit_ctx', activ='linear')
        logit = tensor.tanh(logit_lstm+logit_prev+logit_ctx)
    
        if self.model_options['use_dropout'] and self.model_options['model_version'] < 0.1:
            logit *= retain_probability_hidden
    
        logit = get_layer_constr('ff')(tparams, logit, self.model_options,
                                  prefix='ff_logit', activ='linear')
    
        # compute the softmax probability
        next_probs = tensor.nnet.softmax(logit)
    
        # sample from softmax distribution to get the sample
        next_sample = trng.multinomial(pvals=next_probs).argmax(1)
    
        # compile a function to do the whole thing above, next word probability,
        # sampled word for the next target, next hidden state to be used
        if self.debug:    
            print >>sys.stderr, "Building f_next..",
        inps = [y] + ctx.values() + [init_state]
        outs = [next_probs, next_sample, next_state]
    
        if return_alignment:
            outs += dec_alphas
            #outs.append(dec_alphas)
    
        f_next = theano.function(inps, outs, name='f_next', profile=profile)
        if self.debug:    
            print >>sys.stderr, "Done"
    
        return f_init, f_next

    
    def post_edit(self, id, sources):
        """
        Prepare the source segments for being post-edited and call the decoder.
        Args:
            id: the id of the client request.
            sources: list of source segments (single source = 1 segment, multi-source = 2 segments)
            
        """
        
        if self.debug:    
            print >>sys.stderr,"post-editing!"
        
        
        
        
        if self.debug:
                
            print >>sys.stderr,sources[0]
            whatisthis(sources[0])
            if (self.num_encoder ==2):
                print >>sys.stderr,sources[1]
                whatisthis(sources[1])
        
        xs = []
        
        
        #bpe-fy the sources
        self.sources = []
        if(self.model_options['use_bpe']):
            if self.debug:    
                print >>sys.stderr,"Apply BPE"
                sys.stderr.flush()
            
            for i in xrange(self.num_encoder):
                self.sources.append(self.bpeManager[i].segment(sources[i], i).strip())
                if self.debug:  
                    print >>sys.stderr,i
                    print >>sys.stderr,sources[i]
                    sys.stderr.flush()
                
        else:
            self.sources = sources
            
        #Convert Pipe symbol (| -> <PIPE>)
        if self.debug:    
            print >>sys.stderr,"Convert Pipe Symbol"
            sys.stderr.flush()
        for i in xrange(self.num_encoder):
            if self.debug:    
                print >>sys.stderr,i
                print >>sys.stderr,self.sources[i]
                sys.stderr.flush()
            self.sources[i] = self.sources[i].replace("|", "<PIPE>")
            if self.debug:    
                print >>sys.stderr,"AFTER"
                print >>sys.stderr,self.sources[i]
                sys.stderr.flush()
        
        if self.debug:    
            print >>sys.stderr,"Pipe Converted"
            sys.stderr.flush()
            
        # convert words into ids    
        for i in xrange(self.num_encoder):
            if self.debug:    
                print >>sys.stderr,i
                sys.stderr.flush()
            xs.append(self.dictionaryManagers[i].fromSourcetoIds(self.sources[i]))
            
  
        
       
        if self.debug:    
            print >>sys.stderr, xs

        
            print >>sys.stderr, "Before post-editing"
        
            print >>sys.stderr,self.model_options['use_instance_selection']
            
        sys.stderr.flush()
        
        # post-edit
        APEs = self.translate(id, xs)
        
        if self.debug:  
            print >>sys.stderr,"Size of APEs: ", len(APEs)
            
        # strip bpe symbol and restore normal words
        if(self.model_options['use_bpe']):
            for i in xrange(len(APEs)):
                if self.debug:  
                    print >>sys.stderr,i
                    print >>sys.stderr, "Before: ", APEs[i]
                APEs[i] = re.sub(self.bpeManager[self.num_encoder].getSeparator()+" ", "", APEs[i]).replace("<PIPE>", "|")
                if self.debug:    
                    print >>sys.stderr, "After: ",APEs[i]
       
        return APEs


        
    def translate(self, id, xs):
        """
        Post-edit the source segments.
        Args:
            id: the id of the client request.
            xs: the list of source.
        """
        
        
        time_total_start = time.time()

 
        
        
 
        #if xs[0] is None:
         #   print >>sys.stderr, 'Minibatch with zero sample under length ', self.model_options['maxlen']
            #continue
 
        time_retrieval = 0
        time_adaptation = 0
        dscore = 0.0
        time_retrieval_start = time.time()
 
        if self.model_options['use_instance_selection'] :
            # perform instance selection
            
            if self.debug:
                print >> sys.stderr, "inside instance selection"
                sys.stderr.flush()
            d_query = {}
 
            if self.model_options['query_repres'] == 'id':
                # query input format
                for i in xrange(self.num_encoder):
                    d_query['source'+str(i)] = ' '.join(map(str, numpy.array(xs[i]).flatten()))
            else:
                # query input format and words
                for i in xrange(self.num_encoder):
                    d_query['source_words' + str(i)] = self.sources[i].replace('@@ ', '')
 

 
            maxDoc = self.model_options['max_instances_for_adapter'] if self.model_options['rescore_metric'] is None else self.model_options['max_instances_for_reranker']
            

            
            # query the index
            retrieved_docs = self.searcher.query_multi_source(d_query, maxDoc, self.num_encoder)
            

 
            def rerank(d_query, docs, rescore_metric, query_repres, num_encoder, metric_source_index, rescore_metric_ref='query'):
                """
                Rerank the retrieved sentence pairs according to a certain criteria. This is needed because the Lucene score is not bounded and 
                it does not allow a fair analysis of the retrieved pairs.
                Args:
                    dos: the list of retrieved docs.
                    rescore_metric: the metric used to rescore the docs.
                    query_repres: the type of representation to be used for querying the index.
                    num_encoder: the number of encoders.
                    metric_source_index: in case of multi-source, this allow a user to decide what source to use to rerank the docs (-1 applies the average of the two sources).
                    rescore_metric_ref: set what is the reference when computing the sentence-level BLEU.
                """
                
                # to store all the retrieved training instances key:score, value:(training pair)
                
                chencherry = nltk.translate.bleu_score.SmoothingFunction()
                

                
                all_retrieved = defaultdict(list) #OrderedDict()
 
                if query_repres == 'id':
                    x_query = [d_query['source'+str(i)].split() for i in xrange(num_encoder)]
                else:
                    x_query = [d_query['source_words' + str(i)].split() for i in xrange(num_encoder)]
 

 
                for ids in range(len(docs)):
                    metric_scores = []
                    if query_repres == 'id':
                        retrieved_source_flat = [source.split() for source in docs[ids][2]]
                    else:
                        retrieved_source_flat = [source.split() for source in docs[ids][4]]
                    # compute the scores for each retrieved doc
                    if rescore_metric == 'BLEU':
                        if rescore_metric_ref == 'query':
                            for i in xrange(len(x_query)):
                                metric_scores.append(float(nltk.translate.bleu_score.sentence_bleu([x_query[i]], retrieved_source_flat[i],smoothing_function=chencherry.method1)))
                        else:
                            for i in xrange(len(x_query)):
                                metric_scores.append(float(nltk.translate.bleu_score.sentence_bleu([retrieved_source_flat[i]], x_query[i],smoothing_function=chencherry.method1)))
 

                        retrieved_score =  numpy.array(metric_scores).mean() if metric_source_index == -1 else metric_scores[metric_source_index]


                    elif rescore_metric == 'LD':
                        if rescore_metric_ref == 'query':
                            for i in xrange(len(x_query)):
                                metric_scores.append(pyter.levenshtein(x_query[i], retrieved_source_flat[i], normalize=True))
                        else:
                            for i in xrange(len(x_query)):
                                metric_scores.append(
                                    pyter.levenshtein(retrieved_source_flat[i], x_query[i], normalize=True))
 
                        retrieved_score = 1-(numpy.array(metric_scores).mean() if metric_source_index == -1 else metric_scores[metric_source_index])
                    else:
                        retrieved_score = docs[ids][1]
                        metric_scores.append(docs[ids][1])
 
                    all_retrieved[retrieved_score].append([docs[ids], metric_scores])
                    # return the ordered list of docs according to the selected metric
                return OrderedDict(sorted(all_retrieved.items(), reverse=True))
 
            # rerank the retrieved docs
            retrieved_docs = rerank(d_query, retrieved_docs, self.model_options['rescore_metric'], self.model_options['query_repres'], self.num_encoder, self.model_options['metric_source_index'], self.model_options['rescore_metric_ref'])
 
            for rd in retrieved_docs:
                print >> sys.stderr,rd
                for r in retrieved_docs[rd]:
                    print >> sys.stderr,r
                break
 
            def format_string(string, dimentions=1):
                string = numpy.array(map(int, string.split()))
 
                if dimentions == 2:
                    return string[:,None].tolist()
                return string.tolist()
 
            # sort the retrieved documents by key and take the top n
            
            selected_source = [[]] * self.num_encoder
            selected_target = []
            selected_scores = []
            j = 0
            
            print >> sys.stderr,"Retrieved: ",len(retrieved_docs)
            
            for kscore in retrieved_docs:
                print >> sys.stderr,"Sim. score: ",kscore
                
                # select the top k or those that have a similarity above a threshold
                if kscore < self.model_options['sim_thresh'] or j >= self.model_options['max_instances_for_adapter']:
                    break
 
                for rd in retrieved_docs[kscore]:
                    metric_value = rd[1]
                    if self.debug:
                        print >> sys.stderr, 'SampleID:', j, 'Sim_Multisrc:', metric_value
                    for i in xrange(self.num_encoder):
                        selected_source[i] = selected_source[i] + [format_string(rd[0][2][i], 2)]
                        if self.debug:
                            print >> sys.stderr, "Sample_Source[", i, "]", selected_source[i][-1]
 
                    selected_target.append(format_string(rd[0][3], 1))
                    if self.debug:
                        print >> sys.stderr, "Sample_Target", selected_target[-1]
 
                    selected_scores.append(kscore)
                    j += 1
                    if j >= self.model_options['max_instances_for_adapter']:
                        break
                    
            if self.debug:
                print >> sys.stderr, 'Batch length:', len(selected_target)
 
            time_retrieval = time.time() - time_retrieval_start
            time_adaptation_start = time.time()
 
 
 
            if selected_source[0]:
                if self.model_options['score_type_for_dmodel'] == 'best':
                    dscore = numpy.max(selected_scores)
                elif self.model_options['score_type_for_dmodel'] == 'average':
                    dscore = numpy.mean(selected_scores)
                    
               
                # selects the epochs and learning rate to use during adaptation.
                max_epochs = self.epoch_list[self.bin_(dscore, self.num_epoch_bin)]
                self.lrate = self.lrate_list[self.bin_(dscore, self.num_lrate_bin)]
                
                if self.debug:
                    print >> sys.stderr, 'Max Epoch:', max_epochs,"lrate:",self.lrate
                
                self.use_noise.set_value(1.0)
                # preprocess the retrieved docs
                similar_x, similar_x_mask, similar_y, similar_y_mask = self.prepare_data(selected_source,
                                                                                    selected_target, maxlen=self.model_options['maxlen'],
                                                                                    n_words_src=self.model_options['n_words_src'],
                                                                                    n_words=self.model_options['n_words'])
 
                if self.debug:
                    print >> sys.stderr, "x[0]:",similar_x[0]
                    print >> sys.stderr, "x[1]:",similar_x[1]
                    print >> sys.stderr, "y:",similar_y
                # update the model
                for epc in xrange(max_epochs):
                    self.cost = self.f_grad_shared(similar_x[0], similar_x[1], similar_x_mask[0], similar_x_mask[1], similar_y, similar_y_mask)  ## CHECK this
                    self.f_update(self.lrate)
                    if self.debug:
                        print >> sys.stderr, "Epoch:",epc,"Cost:", self.cost
                        sys.stderr.flush()
                    
            else:
                print >> sys.stderr, "Training instances not found for sample ID:", id
                sys.stderr.flush()
            time_adaptation = time.time() - time_adaptation_start
            

        
        errors = []
        words = []
        
        # prepare the sources to be post-edited
        xs, x_masks= self.prepare_data_sources(xs, maxlen=None, n_words_src=self.model_options['n_words_src'])
 

 
        time_translation_start = time.time()
 
        output = []
        
        # run the decoder
        for jj in xrange(xs[0].shape[2]):
            stochastic = False
 
            
            x_current = [[]] * self.num_encoder
 
            
            for i in xrange(self.num_encoder):
                x_current[i] = xs[i][:, :, jj][:, :, None]
                source_len = int(sum(x_masks[i][:, jj]))
                x_current[i] = x_current[i][:, :source_len, :]
            

            
            if self.debug:
                print >> sys.stderr, "start Decoding"
            self.use_noise.set_value(0.)
            # decoding
            sample, score, sample_word_probs, alignment = self.gen_sample([self.f_init], [self.f_next],
                                                                      x_current,
                                                                      trng=self.trng, k=12,
                                                                      maxlen=self.model_options['maxlen'],
                                                                      stochastic=stochastic,
                                                                      argmax=False,
                                                                      return_alignment=self.model_options['print_alignment'],
                                                                      suppress_unk=False)
            self.use_noise.set_value(1.)
            
            
            if self.debug:
                print >> sys.stderr, "Test documentID:", jj
            for i in xrange(self.num_encoder):
                if self.debug:
                    print >> sys.stderr, 'Source[', i, ']:',
                
                # convert the sources into words
                sentenceSource = self.dictionaryManagers[i].fromIdstoSource(xs[i])

                if self.debug:
                    print >> sys.stderr, sentenceSource
            
            
            
            if self.debug:
                print >> sys.stderr, 'Sample:',
                
            sentenceTarget = ""
            if stochastic:
                ss = sample
            else:
                lengths = numpy.array([len(s) for s in sample])
                
                if self.debug:
                    print >> sys.stderr, 'lenghts:'+ str(lengths)
                
                score = score / lengths
                
                if self.debug:
                    print >> sys.stderr, 'Score:'+str(score)
                    
                ss = sample[score.argmin()]
                
                if self.debug:
                    print >> sys.stderr, 'ss:'+str(ss)
                
            # convert the post-edit into words
            sentenceTarget = self.dictionaryManagers[self.num_encoder].fromIdstoTarget(ss)
            


            if self.debug:
                print >> sys.stderr, sentenceTarget#.decode("utf8")
                
 
            self.of.write(sentenceTarget.strip().encode("utf8") + "\n")
            
            
            output.append( sentenceTarget.strip()) 
 
        time_translation = time.time() - time_translation_start
        time_total = time.time() - time_total_start
        if self.debug:
            print >> sys.stderr,  'Sim:', dscore, 'Cost:', self.cost, 'total_time:', time_total, 'retrieval_time:', time_retrieval, 'adaptation_time:', time_adaptation, 'translation_time:', time_translation
            print >> sys.stderr, "=========="
            
        # reset the model if required    
        #if reset IST model: tparams == backgroundModel 
        if self.model_options['use_instance_selection'] and self.model_options['reset_ist_model_always']:
            copy_tparams(self.tparams_background, self.tparams)   
            
         
        return output



    # generate sample, either with stochastic sampling or beam search. Note that,
    # this function iteratively calls f_init and f_next functions.
    def gen_sample(self, f_init, f_next, xs, trng=None, k=1, maxlen=30,
                   stochastic=True, argmax=False, return_alignment=False, suppress_unk=False, idx=None):
        """
        Generate sample, either with stochastic sampling or beam search. Note that, this function iteratively calls f_init and f_next functions.
        This is the decoder.
        """
        
        if self.debug:
                print >> sys.stderr, "start gen Sample"
                print >> sys.stderr, xs
        
        # k is the beam size we have
        if k > 1:
            assert not stochastic, \
                'Beam search does not support stochastic sampling'
     
        sample = []
        sample_score = []
        sample_word_probs = []
        alignment = []
        if stochastic:
            sample_score = 0
     
        live_k = 1
        dead_k = 0
     
        hyp_samples = [[]] * live_k
        word_probs = [[]] * live_k
        hyp_scores = numpy.zeros(live_k).astype('float32')
        hyp_states = []
        if return_alignment:
            hyp_alignment = [[] for _ in xrange(live_k)]
     
        # for ensemble decoding, we keep track of states and probability distribution
        # for each model in the ensemble
        num_models = len(f_init)
        next_state = [None]*num_models
        ctx0 = [None]*num_models
        next_p = [None]*num_models
        dec_alphas = [None]*num_models
        # get initial state of decoder rnn and encoder context
        for i in xrange(num_models):
            if(len(xs) == 2):
                ret = f_init[i](xs[0], xs[1])
                next_state[i] = ret[0]
                ctx0[i] = [ret[1 + ii] for ii in xrange(2)]
            elif (len(xs) == 1):
                ret = f_init[i](xs[0])
                next_state[i] = ret[0]
                ctx0[i] = [ret[1]]
            else:
                print >>sys.stderr, 'Number of sources is too large! System stopped!'
                sys.exit()  
        

     
        next_w = -1 * numpy.ones((1,)).astype('int64')  # bos indicator
     
        # x is a sequence of word ids followed by 0, eos id
        for ii in xrange(maxlen):
            for i in xrange(num_models):
                ctx = [numpy.tile(ctx0_, [live_k, 1]) for ctx0_ in ctx0[i]]
                inps = [next_w] + ctx + [next_state[i]]
                ret = f_next[i](*inps)
                # dimension of dec_alpha (k-beam-size, number-of-input-hidden-units)
                next_p[i], next_w_tmp, next_state[i] = ret[0], ret[1], ret[2]
                if return_alignment:
                    dec_alphas[i] = ret[3:]
     
                if suppress_unk:
                    next_p[i][:,1] = -numpy.inf
            if stochastic:
                if argmax:
                    nw = sum(next_p)[0].argmax()
                else:
                    nw = next_w_tmp[0]
                sample.append(nw)
                sample_score += numpy.log(next_p[0][0, nw])
                if nw == 0:
                    break
            else:
                cand_scores = hyp_scores[:, None] - sum(numpy.log(next_p))
                probs = sum(next_p)/num_models
                cand_flat = cand_scores.flatten()
                probs_flat = probs.flatten()
                ranks_flat = cand_flat.argpartition(k-dead_k-1)[:(k-dead_k)]
     
                #averaging the attention weights across models
                if return_alignment:
                    num_enc = 2
                    mean_alignment = [numpy.asarray([dec_alphas[ii_][enc_id] for ii_ in xrange(len(dec_alphas))]).mean(axis=0) for enc_id in xrange(num_enc)]
                    #print 'mean align', mean_alignment
     
                voc_size = next_p[0].shape[1]
                # index of each k-best hypothesis
                trans_indices = ranks_flat / voc_size
                word_indices = ranks_flat % voc_size
                costs = cand_flat[ranks_flat]
     
                new_hyp_samples = []
                new_hyp_scores = numpy.zeros(k-dead_k).astype('float32')
                new_word_probs = []
                new_hyp_states = []
                if return_alignment:
                    # holds the history of attention weights for each time step for each of the surviving hypothesis
                    # dimensions (live_k * target_words * source_hidden_units]
                    # at each time step we append the attention weights corresponding to the current target word
                    new_hyp_alignment = [[] for _ in xrange(k-dead_k)]
     
                # ti -> index of k-best hypothesis
                for idx, [ti, wi] in enumerate(zip(trans_indices, word_indices)):
                    new_hyp_samples.append(hyp_samples[ti]+[wi])
                    new_word_probs.append(word_probs[ti] + [probs_flat[ranks_flat[idx]].tolist()])
                    new_hyp_scores[idx] = copy.copy(costs[idx])
                    new_hyp_states.append([copy.copy(next_state[i][ti]) for i in xrange(num_models)])
                    if return_alignment:
                        # get history of attention weights for the current hypothesis
                        new_hyp_alignment[idx] = copy.copy(hyp_alignment[ti])
                        # extend the history with current attention weights
                        new_hyp_alignment[idx].append(mean_alignment[:][ti])
     
                # check the finished samples
                new_live_k = 0
                hyp_samples = []
                hyp_scores = []
                hyp_states = []
                word_probs = []
                if return_alignment:
                    hyp_alignment = []
     
                # sample and sample_score hold the k-best translations and their scores
                for idx in xrange(len(new_hyp_samples)):
                    if new_hyp_samples[idx][-1] == 0:
                        sample.append(new_hyp_samples[idx])
                        sample_score.append(new_hyp_scores[idx])
                        sample_word_probs.append(new_word_probs[idx])
                        if return_alignment:
                            alignment.append(new_hyp_alignment[idx])
                        dead_k += 1
                    else:
                        new_live_k += 1
                        hyp_samples.append(new_hyp_samples[idx])
                        hyp_scores.append(new_hyp_scores[idx])
                        hyp_states.append(new_hyp_states[idx])
                        word_probs.append(new_word_probs[idx])
                        if return_alignment:
                            hyp_alignment.append(new_hyp_alignment[idx])
                hyp_scores = numpy.array(hyp_scores)
     
                live_k = new_live_k
     
                if new_live_k < 1:
                    break
                if dead_k >= k:
                    break
     
                next_w = numpy.array([w[-1] for w in hyp_samples])
                next_state = [numpy.array(state) for state in zip(*hyp_states)]
     
        if not stochastic:
            # dump every remaining one
            if live_k > 0:
                for idx in xrange(live_k):
                    sample.append(hyp_samples[idx])
                    sample_score.append(hyp_scores[idx])
                    sample_word_probs.append(word_probs[idx])
                    if return_alignment:
                        alignment.append(hyp_alignment[idx])
     
        if not return_alignment:
            alignment = [None for i in range(len(sample))]
     
        return sample, sample_score, sample_word_probs, alignment

     
    def bin_(self, value, scale):
        value *= scale
        if value >= scale:
            return scale - 1
        return int(floor(value))
     
     
     
    def updateModel (self, sources, pe):
        """
        Update the model using the sources and the post-edit
        Args:
            sources: the source segments (one if single source, two for multi-source).
            pe: the post-edited segment.
        """
        
        if(self.debug):
            print >> sys.stderr, "Starting Updating the model"
        try:
            
            self.sources = []
            self.pe = ""
            
            # apply bpe to source and pe
            if(self.model_options['use_bpe']):
                for i in xrange(self.num_encoder):
                    self.sources.append(self.bpeManager[i].segment(sources[i],i).strip())
                self.pe = self.bpeManager[self.num_encoder].segment(pe,i+1).strip()
            else:
                self.sources = sources
                self.pe = pe
                
            #Convert Pipe symbol
            for i in xrange(self.num_encoder):
                self.sources[i] = self.sources[i].replace("|", "<PIPE>")
                
            self.pe = self.pe.replace("|", "<PIPE>")
            
            if self.debug:
                print >> sys.stderr,"After bpe"
                print >> sys.stderr,"Sources:"
                print >>sys.stderr, self.sources
                print >> sys.stderr,"PE:"
                print >>sys.stderr, self.pe
                sys.stderr.flush
            
            self.use_noise.set_value(1.)
            
            if self.debug:
                print >> sys.stderr,"X before converting to ids"
                print >>sys.stderr, self.sources
             
            x =[]
            
            # convert source words into ids 
            for i in range(self.num_encoder):
                x.append(self.dictionaryManagers[i].fromSourcetoIds(self.sources[i]))
                
            if self.debug:
                print >> sys.stderr,"X after converting"
                print >>sys.stderr, x
             
             
             
            if self.debug:
                print >> sys.stderr,"Y before converting to ids"
                print >>sys.stderr, self.pe
                
            
            # convert target words into ids
            y = self.dictionaryManagers[self.num_encoder].fromTargettoIds(self.pe)
             
             
            if self.debug:
                print >> sys.stderr,"Y after converting to ids"
                print >>sys.stderr, y
            
            # prepare the source and pe for training
            xs, x_masks, ys, y_mask = self.prepare_data(x, y, maxlen=self.model_options['maxlen'],
                                                     n_words_src=self.model_options['n_words_src'],
                                                     n_words=self.model_options['n_words'])
             
             
            if self.debug:
                print >> sys.stderr,"Xs and Y after prepare data"
                print >>sys.stderr, xs
                print >>sys.stderr, ys
             
            if x is None:
                print >>sys.stderr, 'Source empty of with sentence too long', maxlen
                return -1
            
            ud_start = time.time()
            
            for i in xrange(self.model_options['max_epochs_pe']):
                # iterate the training step
                
         
                # compute cost, grads and copy grads to shared variables
                if self.num_encoder == 2:
                    self.cost = self.f_grad_shared(xs[0], xs[1], x_masks[0], x_masks[1], ys, y_mask)
                elif self.num_encoder == 1:
                    self.cost = self.f_grad_shared(xs[0], x_masks[0], ys, y_mask)
                else:
                    print >>sys.stderr, 'Number of sources is too large! System stopped!'
                    return -1
                
                # do the update on parameters
                self.lrate = self.model_options["lrate"]
                
                self.f_update(self.lrate)
         

                # check for bad numbers, usually we remove non-finite elements
                # and continue training - but not done here
                if numpy.isnan(self.cost) or numpy.isinf(self.cost):
                    print >>sys.stderr, 'NaN detected'
                    return 1., 1., 1.
            
            ud = time.time() - ud_start
            if self.debug:
                print>> sys.stderr,"Epoch:",i,"Cost:",self.cost
                sys.stderr.flush
            
            
            ak=0.0
            if(self.model_options['AddToKnowledgeBase']) & (self.model_options['use_instance_selection']):
                # Add sources and pe  to the Index
                
                self.indexer = Indexer(os.path.join(self.model_options['indexer_path']), initVM=self.initVM)
                if self.debug:
                    print >> sys.stderr,  'Starting adding the src-mt-pe to the base of knowledge '  
                    sys.stderr.flush()
                    
                ak_start = time.time()
                
                x_doc = [' '.join(map(str, numpy.array(x_).flatten())) for x_ in x]
                y_doc = ' '.join(map(str, numpy.array(y).flatten()))
                  
                if(self.model_options['query_repres']  != 'id'):
      
                      
                    self.indexer.add(self.searcher.maxDoc(), x_doc, y_doc, sources, pe)
                else:  
                    self.indexer.add(self.searcher.maxDoc(), x_doc, y_doc, "", "") 
              
                if self.debug:
                    print >> sys.stderr,"Committing the new src-mt-pe"              
                self.indexer.commit()
                
                if self.debug:
                    print >> sys.stderr,"Closing index"      
                self.indexer.close()
                
                if self.debug:
                    print >> sys.stderr,"Deleting the searcher" 
                #once the Index changes, the searcher should be deleted and restarted.
                self.searcher.delete()
                if self.debug:
                    print >> sys.stderr,"Starting the searcher" 
                
                self.searcher = Searcher(os.path.join(self.model_options['indexer_path']), initVM=self.initVM)
                
                ak = time.time() - ak_start
            
            # The model can be reset to the background model or the updated model becomes the background model.
            #copy_tparams(self.tparams, self.tparams_background)     
            if self.model_options['reset_pe_model_always']:
                copy_tparams(self.tparams_background, self.tparams)   
            else:
                copy_tparams(self.tparams, self.tparams_background) 
            
            if self.debug:
                
                totalT = ud+ak
                print >> sys.stderr,  'Model Updated: Updating time: ', ud,"Insertion to KB time:",  ak,"Total time:",totalT 
                print >> sys.stderr, "=========="
        
                sys.stderr.flush()
                
            
            self.uidx += 1
            
            return 0
        except Exception as e:
            print >> sys.stderr,e.__doc__
            print >> sys.stderr,e.message
            print >> sys.stderr, "Error in updating the model"
            return -1
 
 
 
    def dumpModel (self):
        """
        Dump the model into the saveto file.
        """
        
        if(self.debug):
            print >> sys.stderr, "Starting dumping the model"
        try:
            params = unzip_from_theano(self.tparams)
            if(self.debug):
                print >> sys.stderr, "Done unfolding"
                
            if self.grad_history == None:
                print >> sys.stderr, "Gradients are none"
            else:
                print >> sys.stderr, "Gradients are NOT none"
                
            numpy.savez(self.model_options['saveto'] +'.online.'+s, history_errs=self.history_errs, uidx=self.uidx, ghist=self.grad_history, **params)
            
            if(self.debug):
                print >> sys.stderr, "Done saving model.npz"
                
            json.dump(self.model_options, open('%s.online.npz.json' % self.model_options['saveto'], 'wb'), indent=2)
            
            if(self.debug):
                print >> sys.stderr, "Done saving model.npz.json"
                
            return 0
        except Exception as e:
            print >> sys.stderr,e.__doc__
            print >> sys.stderr,e.message
            print >> sys.stderr, "Error in updating the model"
            return -1
        

        


