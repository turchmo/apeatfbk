#!/usr/bin/python
# -*- coding: utf-8 -*-
# Author: Rico Sennrich

"""Use operations learned with learn_bpe.py to encode a new text.
The text will not be smaller, but use only a fixed vocabulary, with rare words
encoded as variable-length sequences of subword units.

Reference:
Rico Sennrich, Barry Haddow and Alexandra Birch (2015). Neural Machine Translation of Rare Words with Subword Units.
"""

from __future__ import unicode_literals, division

from util import *
import sys
import os
import codecs
import argparse
from collections import defaultdict

# hack for python2/3 compatibility
from io import open
argparse.open = open
#from pathlib import Path

# python 2/3 compatibility
if sys.version_info < (3, 0):
  sys.stderr = codecs.getwriter('UTF-8')(sys.stderr)
  sys.stdout = codecs.getwriter('UTF-8')(sys.stdout)
  sys.stdin = codecs.getreader('UTF-8')(sys.stdin)

class BPE(object):

    def __init__(self, codesFile,  separator='@@'):
        
       

        #myfile = Path(codesFile)
        
        if os.path.isfile(codesFile):
        
            inF = codecs.open(codesFile, encoding='UTF-8')
            
            #for item in codes:
            #    print >> sys.stderr, item.split()
            
            #self.bpe_codes = [tuple(item.split()) for item in codes]
            self.bpe_codes = [tuple(item.split()) for item in inF]
            #print >> sys.stderr,self.bpe_codes
            # some hacking to deal with duplicates (only consider first instance)
            self.bpe_codes = dict([(code,i) for (i,code) in reversed(list(enumerate(self.bpe_codes)))])
            print >> sys.stderr,len(self.bpe_codes) 
        else:
            print >> sys.stderr, "File",codesFiles,"does not exist!"
            sys.exit(1)
    
        
        #print >> sys.stderr,self.bpe_codes
        
        self.separator = separator

    def segment(self, sentence, i):
        """segment single sentence (whitespace-tokenized string) with BPE encoding"""
        
        self.cache = {}
        
        print >> sys.stderr, "Seg ", str(i), "::",sentence
        whatisthis(sentence)
        
        if(isinstance(sentence, str)):
            sentence = sentence.decode('UTF-8')
        whatisthis(sentence)
        output = []
        for word in sentence.split():
            #print >> sys.stderr, "Word ",word
            new_word = self.encode(word)
            #print >> sys.stderr, "New-Word: ",new_word
            for item in new_word[:-1]:
                output.append(item + self.separator)
            output.append(new_word[-1])
        print >> sys.stderr, "Seg  bpe ", str(i), "::", (' '.join(output))
        return (' '.join(output))#.decode("utf-8")


    def getSeparator(self):
        return self.separator

    def get_pairs(self, word):
        """Return set of symbol pairs in a word.
    
        word is represented as tuple of symbols (symbols being variable-length strings)
        """
        pairs = set()
        prev_char = word[0]
        for char in word[1:]:
            pairs.add((prev_char, char))
            prev_char = char
        return pairs
    
    def encode(self, orig):
        """Encode word based on list of BPE merge operations, which are applied consecutively
        """
    
        #print >> sys.stderr, "Inside Encoder"
        
        if orig in self.cache:
            return self.cache[orig]
        
        #print >> sys.stderr, self.cache
    
        word = tuple(orig) + ('</w>',)
        
        #print >> sys.stderr, word
        
        pairs = self.get_pairs(word)
        
        #print >> sys.stderr,pairs
    
        while True:
            bigram = min(pairs, key = lambda pair: self.bpe_codes.get(pair, float('inf')))
            #print >> sys.stderr,bigram
            
            if bigram not in self.bpe_codes:
                break
            first, second = bigram
            new_word = []
            i = 0
            while i < len(word):
                try:
                    j = word.index(first, i)
                    #print >> sys.stderr,"j: " + str(j)
                    new_word.extend(word[i:j])
                    #print >> sys.stderr,new_word
                    i = j
                except:
                    new_word.extend(word[i:])
                    break
    
                if word[i] == first and i < len(word)-1 and word[i+1] == second:
                    new_word.append(first+second)
                    i += 2
                else:
                    new_word.append(word[i])
                    i += 1
            new_word = tuple(new_word)
            word = new_word
            if len(word) == 1:
                break
            else:
                pairs = self.get_pairs(word)
    
        # don't print end-of-word symbols
        if word[-1] == '</w>':
            word = word[:-1]
        elif word[-1].endswith('</w>'):
            word = word[:-1] + (word[-1].replace('</w>',''),)
    
        self.cache[orig] = word
        return word



def create_parser():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="learn BPE-based word segmentation")

    parser.add_argument(
        '--input', '-i', type=argparse.FileType('r'), default=sys.stdin,
        metavar='PATH',
        help="Input file (default: standard input).")
    parser.add_argument(
        '--codes', '-c', type=argparse.FileType('r'), metavar='PATH',
        required=True,
        help="File with BPE codes (created by learn_bpe.py).")
    parser.add_argument(
        '--output', '-o', type=argparse.FileType('w'), default=sys.stdout,
        metavar='PATH',
        help="Output file (default: standard output)")
    parser.add_argument(
        '--separator', '-s', type=str, default='@@', metavar='STR',
        help="Separator between non-final subword units (default: '%(default)s'))")

    return parser



if __name__ == '__main__':
    parser = create_parser()
    args = parser.parse_args()

    bpe = BPE(args.codes, args.separator)
    for line in args.input:
        args.output.write(bpe.segment(line).strip())
        args.output.write('\n')
