import socket
#import threading
import sys
import distutils
import ConfigParser
from nematus.nmt_class import Nematus
from nematus.util import *

import xml.etree.ElementTree as ET



class ThreadedServer(object):
    def __init__(self, confFile):
        """
         Initialize the Server
         Args:
            confFile: the configuration file.
        """
        
        # Read the config file
        self.Config = ConfigParser.ConfigParser()
    
        self.Config.read(confFile)
        self.host = self.ConfigSectionMap("serverconfig")['host'] #"localhost" #raw_input("Server hostname? ")
        self.port = int(self.ConfigSectionMap("serverconfig")['port']) #input("Server port? ")
        self.debug  = distutils.util.strtobool(self.ConfigSectionMap("serverconfig")['debug'])
        conf = self.ConfigSectionMap("serverconfig")['conffile']
        
        otherParam ={}
        
        # Set the parameters of the model present in the server config file 
        otherParam["use_bpe".decode("utf-8")] = distutils.util.strtobool(self.ConfigSectionMap("serverconfig")['use_bpe'])
        otherParam["bpecodes".decode("utf-8")] = (self.ConfigSectionMap("serverconfig")['bpecodes']).split('\n')
        otherParam["translation_fname".decode("utf-8")] = self.ConfigSectionMap("serverconfig")['translation_fname']
        otherParam["use_instance_selection".decode("utf-8")] = distutils.util.strtobool(self.ConfigSectionMap("serverconfig")['use_instance_selection'])
        otherParam["reset_ist_model_always".decode("utf-8")] = distutils.util.strtobool(self.ConfigSectionMap("serverconfig")['reset_ist_model_always'])
        otherParam["max_instances_for_reranker".decode("utf-8")] = int(self.ConfigSectionMap("serverconfig")['max_instances_for_reranker'])
        otherParam["max_instances_for_adapter".decode("utf-8")] = int(self.ConfigSectionMap("serverconfig")['max_instances_for_adapter'])
        otherParam["indexer_path".decode("utf-8")] = self.ConfigSectionMap("serverconfig")['indexer_path']
        otherParam["rescore_metric".decode("utf-8")] = self.ConfigSectionMap("serverconfig")['rescore_metric']
        nl = (self.ConfigSectionMap("serverconfig")['epoch_list']).split('\n')
        otherParam["epoch_list".decode("utf-8")]= [int(x) for x in nl]
        nlr = (self.ConfigSectionMap("serverconfig")['lrate_list']).split('\n')
        otherParam["lrate_list".decode("utf-8")]= [float(x) for x in nlr]
        otherParam["rescore_metric_ref".decode("utf-8")] = self.ConfigSectionMap("serverconfig")['rescore_metric_ref']
        otherParam["score_type_for_dmodel".decode("utf-8")] = self.ConfigSectionMap("serverconfig")['score_type_for_dmodel']
        otherParam["sim_thresh".decode("utf-8")] = float(self.ConfigSectionMap("serverconfig")['sim_thresh'])
        otherParam["metric_source_index".decode("utf-8")] = int(self.ConfigSectionMap("serverconfig")['metric_source_index'])
        otherParam["query_repres".decode("utf-8")] = self.ConfigSectionMap("serverconfig")['query_repres']
        otherParam["print_alignment".decode("utf-8")] = distutils.util.strtobool(self.ConfigSectionMap("serverconfig")['print_alignment'])
        otherParam["debug".decode("utf-8")] = distutils.util.strtobool(self.ConfigSectionMap("serverconfig")['debug'])
        otherParam["reset_pe_model_always".decode("utf-8")] = distutils.util.strtobool(self.ConfigSectionMap("serverconfig")['reset_pe_model_always'])
        otherParam["AddToKnowledgeBase".decode("utf-8")] = distutils.util.strtobool(self.ConfigSectionMap("serverconfig")['addtoknowledgebase'])
        otherParam["max_epochs_pe".decode("utf-8")] = int(self.ConfigSectionMap("serverconfig")['max_epochs_pe'])


        if self.debug:
            print >> sys.stderr, self.host, self.port, self.debug, conf
            sys.stderr.flush()

        # init the server socket and Nematus
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))
        self.nematus = Nematus(conf, self.debug, otherParam)

    def listen(self):
        # start the server
        self.sock.listen(5)
        while True:
            client, address = self.sock.accept()
            client.settimeout(60)
            self.listenToClient(client, address)
            #threading.Thread(target = self.listenToClient,args = (client,address)).start()
            
            


            
            
    def listenToClient(self, client, address):
        # method listening on the port
        
        #self.nematus.vm_env.attachCurrentThread()
        #if self.debug:
        #    print >> sys.stderr, threading.current_thread().name
        size = 10000
        while True:
             c_close = False
             try:
                 # receive the input connection and data
                 data = client.recv(size).decode('utf-8')
                 print >> sys.stderr,"Server data received ",data
                 if data:
                     # parse the xml input
                     root = ET.fromstring(data) 

                     # extract the main information from the xml input
                     action = None
                     id = None
                     source = None
                     mt = None
                     pe = None
                     
                     for child in root:
                         
                         if child.tag == 'action':
                             action = child.text 
                             print >> sys.stderr, "action: " + action
                         elif child.tag == 'id':
                             id = child.text 
                         elif child.tag == 'source':
                             source = child.text 
                             print  >> sys.stderr, "src: " + source
                         elif child.tag == 'mt':
                             mt = child.text  
                             print  >> sys.stderr, "mt: " + mt 
                         elif child.tag == 'pe':
                             pe = child.text  
                             
                             
                     
                    

                     
                     self.back = ET.Element('root')
                     
                     if (action == None):
                         # no action
                         print >> sys.stderr," NO ACTION DEFINED"

                         txtBack = ET.SubElement(self.back, "error")
                         
                         txtBack.text = " NO ACTION DEFINED"
                         
                     elif (action == ""):
                         # action is empty
                         
                         print >> sys.stderr," NO ACTION DEFINED"
                         

                         txtBack = ET.SubElement(self.back, "error")
                         
                         txtBack.text = " NO ACTION DEFINED"
                         
                     elif action == "postedit":
                         # post-edit
                         
                         print >> sys.stderr,"I am post-editing"
                      
                                               
                         if((source is None) & (mt is None)): 
                         
                            print >> sys.stderr, "ERROR cannot post-edit! Problem with the input sentences (src and mt are null)!"
                            sys.stderr.flush()
                            txtBack = ET.SubElement(self.back, "error")
                         
                            txtBack.text = "ERROR cannot post-edit! Problem with the input sentences (src and mt are null)!"                        
                         else:
                            if ((source == "") & (mt == "")):
                                                    
                                print >> sys.stderr, "ERROR cannot post-edit! Problem with the input sentences (src and mt are empty)!"
                                 
                                txtBack = ET.SubElement(self.back, "error")
                              
                                txtBack.text = "ERROR cannot post-edit! Problem with the input sentences (src and mt are empty)!"
                            else:     

                                print >> sys.stderr,"Source or mt are not empty"
                                sys.stderr.flush()
                                self.postedit(id, source, mt)
                                
                                
                                
                     elif action == "update":
                         # update the model
                         
                         print >> sys.stderr,"I am updating"
                         
                         
                         if ((source is None) & (mt is None) & (pe is None)): 
                         
                            print >> sys.stderr, "ERROR cannot update the model! Problem with the input sentences (src, mt and pe are null)!"
                            
                            txtBack = ET.SubElement(self.back, "error")
                         
                            txtBack.text = "ERROR cannot update the model! Problem with the input sentences (src, mt and pe are  null)!"
                                             
                         elif  (pe is None):
                            print >> sys.stderr, "ERROR cannot update the model! Pe is Null!"
                            
                            txtBack = ET.SubElement(self.back, "error")
                         
                            txtBack.text = "ERROR cannot update the model! Pe is Null!"
                         elif (pe == ""):
                            print >> sys.stderr, "ERROR cannot update the model! Pe is empty!"
                            
                            txtBack = ET.SubElement(self.back, "error")
                         
                            txtBack.text = "ERROR cannot update the model! Pe is empty!"
                         else:
                            self.update(id, source, mt, pe)
                    
                     elif action == "modelDump":
                         # dump the current model on file
                         print >> sys.stderr,"I am dumping the models"
                         
                         self.dumpModel(id)
                         
                     else:
                            print >> sys.stderr, "ERROR UNKNOWN action!"
                            
                            txtBack = ET.SubElement(self.back, "error")
                         
                            txtBack.text = "ERROR UNKNOWNE action! nothing to do!"  
                            
                    
                     
                     # prepare the response to the client
                     
                     response = ET.tostring(self.back)
                         

                     print >> sys.stderr,"Sent: " + response
                     client.send(response.encode("UTF-8"))
                 else:
                     c_close = True
                     raise Exception('Client disconnected')
                    
             except Exception as e:
                print e.__doc__
                if(c_close == False):
                    print >> sys.stderr,"Problem in Server - Thread closed"
                else:
                    print "Thread closed"
                client.close()
                return False            


    def postedit(self, id, source, mt):
         """
         Post-edit the source and or the mt segment
         Args:
            id: id of the client request.
            source: the source segment.
            mt: the machine translated segment
         """
         sources = []


         if (mt is not None):
            if (mt.strip() != ""):
                sources.append(mt)
                print >> sys.stderr,mt
            else:
                print >> sys.stderr,"The mt sentence is not present!"

         if (source is not None):
             if (source.strip() != ""):
                 sources.append(source)
                 print >> sys.stderr,source

         # prepare the xml answer to the client         
         idT = ET.SubElement(self.back, "id")
         idT.text = id
         
         if(len(sources) != 0):
             # post-edit
             pes = self.nematus.post_edit(id, sources)
             
             if (pes is None) :
                            
                 print >> sys.stderr, "ERROR the post-edited sentence is NULL"
                                
                 txtBack = ET.SubElement(self.back, "error")
                             
                 txtBack.text = "ERROR the post-edited sentence is NULL"
             elif (len(pes) == 0):
                 
                
                 print >> sys.stderr, "ERROR the post-edited sentence is empty"
                                
                 txtBack = ET.SubElement(self.back, "error")
                             
                 txtBack.text = "ERROR the post-edited sentence is empty"
             else:
                 
                 for ape in pes:
                     print >> sys.stderr, "ape: " + ape
                 
                     peBack = ET.SubElement(self.back, "ape")
                     #peBack.text = ape.decode("UTF-8")
                     peBack.text = ape

        
         else:
            print >> sys.stderr, "ERROR source sentences are empty, returning empty ape!"
                                
            peBack = ET.SubElement(self.back, "ape")
                     
            peBack.text = " "
         
         return ET
     
     
     
    def update(self, id, source, mt, pe):
         """
         Update the model
         Args:
            id: id of the client request.
            source: the source segment.
            mt: the machine translated segment.
            pe: post-edited segment.
         """
         sources = []


         if (mt is not None):
             if (mt.strip() != ""):
                 sources.append(mt)
                 print >> sys.stderr,mt

         if (source is not None):
             if (source.strip() != ""):
                 sources.append(source)

                 print >> sys.stderr,source

         # prepare the xml response to the client         
         idT = ET.SubElement(self.back, "id")
         idT.text = id
         
         if len(sources) != 0:
             # update the model
             res = self.nematus.updateModel(sources, pe)
             
             
             
             if (res is None):
                print >> sys.stderr, "ERROR Model not updated: pe is NULL"
                                
                txtBack = ET.SubElement(self.back, "error")
                             
                txtBack.text = "ERROR model not updated: pe is NULL"
                  
             elif (res == -1) :
                print >> sys.stderr, "ERROR Model not updated: pe is empty"
                                
                txtBack = ET.SubElement(self.back, "error")
                             
                txtBack.text = "ERROR model not updated: pe is empty"
             else:
                updBack = ET.SubElement(self.back, "update")
                    
                updBack.text = "Model correctly updated"
        
                
         else:
            print >> sys.stderr, "ERROR Model not updated: sources are empty!"
                                
            txtBack = ET.SubElement(self.back, "error")
                             
            txtBack.text = "ERROR model not updated: sources are empty" 
         return ET
 
 
    def ConfigSectionMap(self, section):
         """
         Map the sections in the configuration file into a map
         Args:
            section: the section of the configuration file to map.
         """
        
         dict1 = {}
         options = self.Config.options(section)
         for option in options:
             try:
                 dict1[option] = self.Config.get(section, option)
                 if dict1[option] == -1:
                     DebugPrint("skip: %s" % option)
             except:
                 print("exception on %s!" % option)
                 dict1[option] = None
         return dict1


    def dumpModel(self, id ):
         """
         Dump the current model on file
         Args:
            id: id of the client request.
         """         
         
         res = self.nematus.dumpModel()
         
         
         idT = ET.SubElement(self.back, "id")
         idT.text = id
         if (res == None) :
             print >> sys.stderr, "ERROR in dumping the modes"
                            
             txtBack = ET.SubElement(self.back, "error")
                         
             txtBack.text = "ERROR in dumping the modes"
         elif (res == 0) :
             print >> sys.stderr, "ERROR in dumping the modes"
                            
             txtBack = ET.SubElement(self.back, "error")
                         
             txtBack.text = "ERROR in dumping the modes"
         
         else:   
             print >> sys.stderr, "Model dumped correctly"
         
             peBack = ET.SubElement(self.back, "dump")
             #peBack.text = ape.decode("UTF-8")
             peBack.text = "Model dumped correctly"
         
            
         
         return ET
  





if __name__ == "__main__":
    
    argv=None
    if argv is None:
        argv = sys.argv
        print >> sys.stderr, argv
        
        if len(argv) != 2:
                print "Wrong Number of Parameters "
                print "Usage: "
                print "python ....py configurationFile"

                sys.exit()

    
    confFile  = argv[1] 


    ThreadedServer(confFile).listen()