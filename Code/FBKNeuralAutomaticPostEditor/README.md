FBK-NAPE
-------


Attention-based encoder-decoder model for neural automatic post-editing

This package is based on the dl4mt-tutorial by Kyunghyun Cho et al. ( https://github.com/nyu-dl/dl4mt-tutorial ) and it is a branch of the Open-Source Neural Machine Translation Nematus (https://github.com/EdinburghNLP/nematus) commit n. XXXX developed by the Edinburgh nlp group (http://edinburghnlp.inf.ed.ac.uk).
It was used to produce top-scoring systems at the APE shared tast at WMT 17.

In addition to the functionalities proposed by Nematus, the FBK-NAPE system has been enhanced by adding:

 - multi-source input (source and mt segments) (Chatterjje et al http://www.aclweb.org/anthology/W17-4773)
 - client-server architecture
 - batch and online settings
 - instance selection mechanism for on-the-fly domain adaptation (Farajian et al http://www.aclweb.org/anthology/W17-4713 )
 - unified configuration script


INSTALLATION
------------

Nematus requires the following packages:

 - Python >= 2.7
 - numpy
 - nltk
 - ipdb
 - Theano <= 0.8.X (and its dependencies)
 - PYLucene

we recommend executing the following command in a Python virtual environment:
   `pip install numpy numexpr cython tables theano ipdb`

PyLucene should be installed following the instruction on http://lucene.apache.org/pylucene/

the following packages are optional, but *highly* recommended

 - CUDA >= 7  (only GPU training is sufficiently fast)
 - cuDNN >= 4 (speeds up training substantially)


you can run Nematus locally. To install it, execute `python setup.py install`



USAGE INSTRUCTIONS
------------------

## BATCH 

The usage of the FBK-NAPE software is similar to the original Nematus, but a different interface with the code is proposed. To train the APE system (single or double source),  

To train the APE system (single or double source), execute: 

`train.sh config.cfg workingDirPath`

This script automatically gets the available gpu from the machine where it is run using the environmental variable $SGE_GPU. 

The config.cfg contains the following parameters:

#### data (*)
| parameter            | description |
|---                   |--- |
| data_dir PATH |  path to the folder containing the training and dev files |
| work_dir PATH | path to the folder where the output files are generated |
| src        |  list of source corpus extensions (e.g. ['src','mt'] ). The code supports one or two sources. |
| trg       |  source corpus extensions |
| apply_bpe  INT | enable the computation of bpe NOTE: Supported only for single source, for multi-source the BPE should be computed offline | 
| bpe_operation_src   INT     |  number of bpe codes to learn from the source corpus |
| bpe_operation_strg  INT        |  number of bpe codes to learn from the target corpus |

#### architecture (*)
| parameter            | description |
|---                   |--- |
| dim_word INT       |  embedding layer size (default: 512) |
| dim INT            |  hidden layer size (default: 1000) |
| n_words_src INT    |  source vocabulary size (default: None) |
| n_words INT        |  target vocabulary size (default: None) |
| factors INT        |  number of input factors (default: 1) |
| dim_per_factor INT [INT ...] | list of word vector dimensionalities (one per factor): '  dim_per_factor 250 200 50' for total dimensionality of 500 (default: None) |
| use_dropout        |  use dropout layer (default: False) |
| dropout_embedding FLOAT | dropout for input embeddings (0: no dropout) (default: 0.2) |
| dropout_hidden FLOAT | dropout for hidden layer (0: no dropout) (default: 0.2) |
| dropout_source FLOAT | dropout source words (0: no dropout) (default: 0) |
| dropout_target FLOAT | dropout target words (0: no dropout) (default: 0) |
| encoder 			| gru |
| decoder			| gru_cond |
| maxlen INT         |  maximum sequence length (default: 100) |
| optimizer {adam,adadelta,rmsprop,sgd} | optimizer (default: adam) |
| batch_size INT     | minibatch size (default: 80) |
| max_epochs INT     | maximum number of epochs (default: 5000) |
| finish_after INT   | maximum number of updates (minibatches) (default: 10000000) |
| decay_c FLOAT      |  L2 regularization penalty (default: 0) |
| map_decay_c FLOAT  |  L2 regularization penalty towards original weights (default: 0) |
| alpha_c FLOAT      |  alignment regularization (default: 0) |
| clip_c FLOAT       |  gradient clipping threshold (default: 1) |
| lrate FLOAT        |  learning rate (default: 0.0001) |
| shuffle_each_epoch         |  enable shuffling of training data (for each epoch) |
| sort_by_length  |  enable sort sentences in maxibatch by length |
| maxibatch_size INT |  size of maxibatch (number of minibatches that are sorted by length) (default: 20) |
| finetune           |  train with fixed embedding layer |
| finetune_only_last |  train with all layers except output layer fixed |

### additional (*)
| parameter            | description |
|---                   |--- |
| valid_datasets PATH PATH | parallel validation corpus (source and target)| (default: None) |
| valid_batch_size INT | validation minibatch size (default: 80) |
| validFreq INT       | validation frequency (default: 10000) |
| patience INT        | early stopping patience (default: 10) |
| external_validation_script PATH | location of validation script (to run your favorite metric for validation) (default: None) |
| dispFreq INT       | display loss after INT updates (default: 1000) |
| sampleFreq INT     | display some samples after INT updates (default: 10000) |
| saveFreq INT       |  save frequency (default: 30000) |
| reload_            |  load existing model (if '--model' points to existing model) |
| overwrite          |  write all models to same file |

An example of the configuration file can be found in the root folder of FBK-NAPE

For testing, see the online scenario.

## ONLINE
The use of the FBK-NAPE code in the online setting requires a pre-trained model obtained by running the batch training. This working scenario is modelled by a client-server architecture.

### SERVER
The server is developed to perform actions provided in input by the client. The available actions are:

 - post-edit: a target segment or a source and target segments are post-edited.
 - update the model 
 - post-edit and then update
 - dump the updated model
 
The exchange of information between the server and a client is done using xml strings that vary according to the action:

 - post-edit: <root><id>id</id><action>postedit</action><source>TokenizedSource</source><mt>TokenizedMT</mt></root>
 - update: <root><id>1</id><action>update</action><source>TokenizedSource</source><mt>TokenizedMT</mt><pe>TokenizedPE</pe></root>
 - dump: <root><id>1</id><action>modelDump</action></root>
 
The server returns to the client a xml strings containing the outcome of its processing. 

To run the server, execute:

`server.sh server_config.ini` 

add 

`\> log.out 2> log.err &`

to run it in background and to save the debug messages in two log files.

The server_config.ini contains the following parameters:

| parameter            | description |
|---                   |--- |
| host | host name where the server runs | 
| port INT | port number where the server connects |
| conffile PATH | path to the pre-trained model file model.npz |
| use_bpe | enable application of bpe on the sentences |
| bpecodes | list of bpe codes files |
| use_instance_selection | enable instance selection adaptation during testing |
| reset_ist_model_always | enable resetting of the adapted model after  translation |
| max_instances_for_reranker INT | maximum number of instances to retrieve by Lucence| 
| max_instances_for_adapter INT |  maximum number of instances to keep for the adaptation (among the "max_instances_for_reranker" instances)|
| indexer_path PATH | path to the folder where to save the indexer |
| translation_fname PATH | path of the output file containing the post-edited segments | 
| rescore_metric {BLEU, LD} | the method for rescoring and reranking the "max_instances_for_reranker" retrieved instances. Currently we support sentence-level BLEU, and Levenshtein distance|
| epoch_list |  list of integers, indicating the number of epochs for each similarity bin (e.g. by receiving the list of [1,3,5,7], the system divides the similarity range into 4 bins of size 0.25, and performs 1 epoch if sim <= 0.25, or 3 epochs if 0.25 <sim<=0.5, and so on.|
| lrate_list |  list of real numbers, indicating the learning rate for each similarity bin|
| rescore_metric_ref {query, candidate} | determines which of the "query" or "candidate" should be used as reference by the "rescore_metric" |
| score_type_for_dmodel {best, average}| if "max_instances_for_adapter" is larger than 1 we need to obtain one single score for that batch. "best" returns the similarity of the top1, "average" returns the mean of the similarities in the batch|
| sim_thresh FLOAT | threshold on the similarity value. Adaptation is not applied using retrieved sentence with a similarity smaller than this threshold |
| metric_source_index {-1, 0, 1, ..., N} | use the similarity of the nth source. If set to -1, use the average similarity of all the sources |
| query_repres {"id", "words"} | query Lucene with the "word" representation of the test sentence or the segmented version ("id")|
| print_alignment | enable printing of word alignments |
| debug | enable debug modality that results in large numbers of comment in the log file |
| reset_pe_model_always | enable resetting of the model after updating using the post-edited segment |
| max_epochs_pe INT | number of epochs during the adaptation to the post-edited segments |
| AddToKnowledgeBase | enable addition of post-edited segments to the knowledge base |

An example of the configuration file can be found in the root folder of FBK-NAPE

### CLIENT

The code is provided with a client software that sends instruction to the server. 

To run the client, execute:

`python client.py client_config.ini`

The client_config.ini contains the following parameters:

| parameter            | description |
|---                   |--- |
| host | host name where the server runs | 
| port INT | port number where the server connects |
| action INT | the id of the action to be performed by the server (0:post-edit 1: update the model 2: post-edit + update 3: dump model) |
| srcFile PATH | path to the file containing the source segments. It is up to the user to decide if sending the tokenised  or the tokenised plus bpe-fied segments. This choices has to synchronise with the server parameter use_bpe. |
| mtFile PATH | path to the file containing the target segments. It is up to the user to decide if sending the tokenised or the tokenised plus bpe-fied segments. This choices has to synchronise with the server parameter use_bpe |
| peFile PATH | path to the file containing the human post-edited segments. It is up to the user to decide if sending the tokenised  or the tokenised plus bpe-fied segments. This choices has to synchronise with the server parameter use_bpe |
| outputFile PATH | path to the output file where the automatic post-edited segments are saved |

In case, the user wants to pass the tokenised segments and let the server to apply the bpe, set use_bpe = True and bpecodes in the server_config.ini and pass the tokenised segments to the client.

In case, the user does not want to use the bpe, set use_bpe = False in the server_config.ini and pass the tokenised segments to the client.
 

#### TEST A PRE-TRAINED MODEL

To test a pre-trained model, adjust the paths in model.npz.json, then start the server:

`server.sh server_config.ini > log.out 2> log.err &`

start the client setting to 0 the action parameter:

`python client.py client_config.ini`

the post-edited segments are then saved in the outputFile.


PUBLICATIONS
------------

the code is based on the following models:

Dzmitry Bahdanau, Kyunghyun Cho, Yoshua Bengio (2015): Neural Machine Translation by Jointly Learning to Align and Translate, Proceedings of the International Conference on Learning Representations (ICLR).

Sennrich, Rico  and  Firat, Orhan  and  Cho, Kyunghyun  and  Birch, Alexandra  and  Haddow, Barry  and  Hitschler, Julian  and  Junczys-Dowmunt, Marcin  and  Laubli, Samuel  and  Miceli Barone, Antonio Valerio  and  Mokry, Jozef  and  Nadejde, Maria, 2017. Nematus: a toolkit for neural machine translation. Proceedings of the Software Demonstrations of the 15th Conference of the European Chapter of the Association for Computational Linguistics. Valencia, Spain.

for the changes specific to FBK-NAPE, please consider the following papers:

Chatterjee, Rajen, M. Amin Farajian, Matteo Negri, Marco Turchi, Ankit Srivastava, and Santanu Pal. "Multi-source Neural Automatic Post-Editing: FBK’s participation in the WMT 2017 APE shared task." In Proceedings of the Second Conference on Machine Translation, pp. 630-638. 2017.

(*) These instructions are identical to the ones reported in the Nematus implementation.
