#!/bin/bash

ANACONDA="/hltsrv1/software/anaconda"

THEANO="/hltsrv1/software/Theano/Theano.0.8.2-dev/"
#THEANO="/hltsrv1/software"

CUDA_ROOT="/usr/local/cuda-7.5"

LIBGPU="/hltsrv1/software/libgpu"

GRAPHVIZ="/hltsrv1/software/graphviz"

export PATH="$GRAPHVIZ/bin/:$ANACONDA/bin:$CUDA_ROOT/bin:$PATH"

#JCC="/hltsrv1/software/pylucene/pylucene-6.2.0/jcc/build/lib.linux-x86_64-2.7"
export PYTHONPATH="$THEANO:/hltsrv1/software/pylucene/pylucene-6.2.0/build/test:$PYTHONPATH"


#export LIBRARY_PATH="$JCC:$LIBRARY_PATH"

#export LD_LIBRARY_PATH="$JCC:$LD_LIBRARY_PATH"


#export PYTHONPATH="$THEANO:/hltsrv0/turchi/Projects/QT21/WorkingFolder/NMT/software/pylucene-6.2.0/build/test:$PYTHONPATH"

export LIBRARY_PATH="$CUDA_ROOT/lib64:$LIBGPU/lib:$LIBRARY_PATH"

export LD_LIBRARY_PATH="$CUDA_ROOT/lib64:$LIBGPU/lib:$LD_LIBRARY_PATH"

export CPATH="$LIBGPU/include:$CPATH"

export CPPPATH="$LIBGPU/include:$CPPPATH"
#export PYTHONUNBUFFERED=true
